import React, { Component } from 'react';
import { ListView, Text, TouchableOpacity, View } from 'react-native';
import {observable,computed,action} from 'mobx'
class OtherShowCountryStore {

  @observable stories = []
  @observable liked = []


  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  @computed get dataSourceStories() {
    return this.ds.cloneWithRows(this.stories.slice());
  }

  @action resetLikeds(){
    this.liked = []
  }

}
const otherShowCountryStore = new OtherShowCountryStore()
export default otherShowCountryStore
