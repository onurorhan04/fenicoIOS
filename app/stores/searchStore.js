import React, { Component } from 'react';
import { ListView, Text, TouchableOpacity, View } from 'react-native';
import {observable,computed} from 'mobx';

class SearchStore {

  @observable searchedData = []
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  @computed get dataSource() {
    return this.ds.cloneWithRows(this.searchedData.slice());
  }
}
const searchStore = new SearchStore()
export default searchStore
