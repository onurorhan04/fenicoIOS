import React,{Component} from 'react'
import {ListView} from 'react-native'
import {observable,computed} from 'mobx'

class ProfilOtherStore {
  @observable data:""

  @observable countryData = []
  @observable storyData = []
  @observable limitedStoryData = []
  @observable userProfileData = ""
  @observable location = ""
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  @computed get dataSourceCountries(){
    return this.ds.cloneWithRows(this.countryData.slice())
  }

  @computed get dataSourceStories(){
    return this.ds.cloneWithRows(this.storyData.slice())
  }

  @observable wishListData = []

}
const profilOtherStore = new ProfilOtherStore()
export default profilOtherStore
