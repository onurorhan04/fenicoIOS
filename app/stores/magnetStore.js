import {observable,computed} from 'mobx'
class MagnetStore {
   @observable myKey = ""
   //for comment id ( sorting )
   @observable ai_key = ""
   //
   @observable currentRegion = ""
   @observable searchedRegionName = ""
   @observable searchPlaceZIndex = 9999
   @observable magnetPinBG = '#FFF'
   @observable magnetLocationBG = '#FFF'
   @observable magnetSubscribeBG = '#FFF'

   @observable searchModal = false

   //try to solve for crash after creating new pin 
   @observable allMagnets = []

   @observable serverTime = ''

   

}
const magnetStore = new MagnetStore()
export default magnetStore
