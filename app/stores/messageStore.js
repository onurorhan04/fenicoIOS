import React, { Component } from 'react';
import { ListView, Text, TouchableOpacity, View } from 'react-native';
import {observable,computed} from 'mobx';

class MessageStore {
  @observable chatData = []
  @observable messageCreaterController = []
  @observable checkOutFireBaseServer:""
  @observable contactList = []
  @observable gotNewContact:false

  @observable userGotInContactList:false
  @observable oppositeUserIsGot = ""

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  @computed get dataSource() {
    return this.ds.cloneWithRows(this.contactList.slice());
  }
}
const messageStore = new MessageStore()
export default messageStore
