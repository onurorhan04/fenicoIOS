import React, { Component } from 'react';
import { ListView, Text, TouchableOpacity, View } from 'react-native';
import {observable,computed} from 'mobx'
class ProfilStories {

  @observable stories: []
  @observable showMessage = false
  @observable photo = ""

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  @computed get dataSourceStories() {
    return this.ds.cloneWithRows(this.stories.slice());
  }

}
const profilStories = new ProfilStories()
export default profilStories
