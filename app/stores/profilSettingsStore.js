import React, { Component } from 'react';
import { ListView, Text, TouchableOpacity, View } from 'react-native';
import {observable,computed} from 'mobx';

class ProfilSettingsStore {

  @observable blockedUsers = []

  
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  @computed get blockedUsersDataSource() {
    return this.ds.cloneWithRows(this.blockedUsers.slice());
  }
}
const profilSettingsStore = new ProfilSettingsStore()
export default profilSettingsStore
