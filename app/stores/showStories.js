import {observable,computed} from 'mobx';

class ShowStories {
  @observable storyID = ""
  @observable storyData = ""
  @observable heartStatus = false
  @observable cityDetails = ""
  @observable likeCount = ""
}
const showStories = new ShowStories()
export default showStories
