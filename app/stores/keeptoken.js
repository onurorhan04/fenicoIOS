import {observable,computed} from 'mobx'
class KeepToken {
  @observable userID:""
  @observable username:""
  @observable password:""
  @observable token:""
  @observable fbToken:""
  @observable howToLogin:""


}
const keepToken = new KeepToken()
export default keepToken
