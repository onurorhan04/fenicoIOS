import React, { Component } from 'react';
import { ListView, Text, TouchableOpacity, View } from 'react-native';
import {observable,computed} from 'mobx';

class AddStoryStore {

  @observable searchedLocation = []

  @observable selectedLocationID = ""
  @observable selectedLocationName = ""
  @observable selectedDate = ""

  @observable photoNum1 = ""
  @observable photoNum2 = ""
  @observable photoNum3 = ""
  @observable photoNum4 = ""
  @observable photoNum5 = ""
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  @computed get dataSource() {
    return this.ds.cloneWithRows(this.searchedLocation.slice());
  }
}
const addStoryStore = new AddStoryStore()
export default addStoryStore
