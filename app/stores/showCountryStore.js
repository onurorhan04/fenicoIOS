import React, { Component } from 'react';
import { ListView, Text, TouchableOpacity, View } from 'react-native';
import {observable,computed,action} from 'mobx'
import KeepToken from './keeptoken'
class ShowCountryStore {

  @observable stories = []
  @observable liked = []
  @observable triggerToFetch = false
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  @action getStoryDataAfterCreatedNoLocStory(){
    setTimeout(() => { 
      var fetch_url = ""
      fetch_url = "https://fenicoapp.com/api/v1/user-story/index?userid="+KeepToken.userID+"expand=userStoryImages"
      fetch(fetch_url, {
        method: "GET",
        headers: {
          'Authorization': 'Bearer ' + KeepToken.token
        }
      })
      .then((response) => response.json())
      .then((responseData) => {
        let responseDataStories = responseData.items
        let newLiked = []
        
        responseDataStories.forEach(function(story) {
          if(story.userStoryLikes.length == 0){
            newLiked.push({'story_id':story.id,'liked':false,'likeCount':0})
          }
          story.userStoryLikes.forEach(function(like) {
            console.log('each story', story)
            newLiked.push({'story_id':story.id,'liked':false,'likeCount':story.like_count})
          });
          story.userStoryLikes.forEach(function(like) {
            if(like.olusturan == KeepToken.userID){
              var index = newLiked.findIndex(item =>item.story_id === story.id)
              newLiked[index].liked = true
            }
          });
        });
        this.liked = newLiked
        this.stories = responseData.items
        
      }).catch((error) => {
        console.log(error)
      });
    }, 1200)
  }
  
  @computed get dataSourceStories() {
    return this.ds.cloneWithRows(this.stories.slice());
  }

  @action resetLikeds(){
    this.liked = []
  }

}
const showCountryStore = new ShowCountryStore()
export default showCountryStore
