import React, { Component } from 'react';
import { ListView, Text, TouchableOpacity, View } from 'react-native';
import {observable,computed} from 'mobx';

class NotificationStore {


  @observable notificationData = ""
  @observable commentNotificationKey = []

  @observable commentNotifData = []
  @observable messageNotifData = []
  @observable preparedNotificationData = []
  @observable notificationCount = 0

  /*
  @observable test1 = [{ name: 'onur', surname: 'orhan' }]
  @observable test2 = [{ name: 'serpil', surname: 'orhan' }]

  @observable testCombinatedData = this.test1.concat(this.test2)*/

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  @computed get dataSource() {
    return this.ds.cloneWithRows(this.preparedNotificationData.slice());
  }
}
const notificationStore = new NotificationStore()
export default notificationStore
