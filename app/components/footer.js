import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,View,Dimensions,TouchableOpacity,TouchableHighlight,Image} from 'react-native';
import Images from '../images/images'
const windowSize = Dimensions.get('window');
import NotificationStore from '../stores/notificationStore';
import {withNavigation} from 'react-navigation'
import { observer } from 'mobx-react/native';
@withNavigation
@observer
export default class NotificationMain extends Component {
  routing(routeName){
    if(this.props.navigation.state.routeName != routeName){
      this.props.navigation.navigate(routeName)
    }
  }
  render() {
    return (
      <View style={styles.container}>
       <TouchableOpacity activeOpacity={1} underlayColor={'transparent'} onPress={() => this.routing('searchMain')} style={{flex:1,backgroundColor:'#fff',justifyContent:'center',alignItems:'center'}}>
          <Image source={this.props.navigation.state.routeName === "searchMain" ? Images.activeFooterSearch : Images.footerSearch } style={{height:27,width:27,resizeMode:'stretch'}} />
       </TouchableOpacity>
       <TouchableOpacity activeOpacity={1} underlayColor={'transparent'} onPress={() => this.routing('notificationMain')} style={{flex:1,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
        <Image source={this.props.navigation.state.routeName === "notificationMain" ? Images.activeFooterNotification : Images.footerNotification} style={{height:27,width:27,resizeMode:'stretch'}} >
          {NotificationStore.notificationCount > 0 && NotificationStore.notificationCount < 10 ?
            <View style={{position:'absolute',right:0,top:0,justifyContent:'center',alignItems:'center',height:16,width:13,borderRadius:3,backgroundColor:'#e74c3c'}}>
              <Text style={{color:'#fff',fontWeight:'bold',fontSize:10}}>{NotificationStore.notificationCount}</Text>
            </View>
            :
            null
          }
          {NotificationStore.notificationCount > 9  && NotificationStore.notificationCount < 100 ?
            <View style={{position:'absolute',right:0,top:0,justifyContent:'center',alignItems:'center',height:16,width:17,borderRadius:3,backgroundColor:'#e74c3c'}}>
              <Text style={{color:'#fff',fontWeight:'bold',fontSize:9}}>{NotificationStore.notificationCount}</Text>
            </View>
            :
            null
          }
          {NotificationStore.notificationCount > 99  && NotificationStore.notificationCount < 1000 ?
            <View style={{position:'absolute',right:0,top:0,justifyContent:'center',alignItems:'center',height:16,width:19,borderRadius:3,backgroundColor:'#e74c3c'}}>
              <Text style={{color:'#fff',fontWeight:'bold',fontSize:9}}>{NotificationStore.notificationCount}</Text>
            </View>
            :
            null
          }
          
        </Image>
       </TouchableOpacity>
       <TouchableOpacity activeOpacity={1} onPress={() => this.routing('magnetMain')} style={{flex:1,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
          <Image source={this.props.navigation.state.routeName === "magnetMain" ? Images.activeFooterMagnet : Images.footerMagnet } style={{height:40,width:40}} />
       </TouchableOpacity>
       <TouchableOpacity activeOpacity={1} underlayColor={'transparent'} onPress={() => this.routing('messageMain')} style={{flex:1,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
        <Image source={this.props.navigation.state.routeName === "messageMain" ?  Images.activeFooterMessage : Images.footerMessage } style={{height:27,width:27,resizeMode:'stretch'}}>
          {NotificationStore.messageNotifData.length > 0 ?
            <View style={{position:'absolute',right:0,top:0,justifyContent:'center',alignItems:'center',height:16,width:13,borderRadius:3,backgroundColor:'#e74c3c'}}>
              <Text style={{color:'#fff',fontWeight:'bold',fontSize:10}}>{NotificationStore.messageNotifData.length}</Text>
            </View>
            :
            null
          }
        </Image>
       </TouchableOpacity>
       <TouchableOpacity activeOpacity={1} underlayColor={'transparent'} onPress={() => this.routing('profilMain')} style={{flex:1,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
        <Image source={this.props.navigation.state.routeName === "profilMain" ? Images.activeFooterProfile : Images.footerProfile} style={{height:27,width:27,resizeMode:'stretch'}} />
       </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    position:'absolute',
    left:0,
    bottom:0,
    height:50,
    width:windowSize.width,
    backgroundColor: '#fff',
    flexDirection:'row',
    borderTopWidth:1.5,
    borderColor:'#A0A0A0'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

