
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Animated
} from 'react-native';
const windowSize = Dimensions.get('window');
import Images from '../../images/images'
export default class SplashScreen extends Component {
    state = {
        opacity:new Animated.Value(0.1),
    }
    componentDidMount(){
        Animated.timing(                            // Animate value over time
        this.state.opacity,                      // The value to drive
            {
                toValue: 1,    
                duration:3000                     // Animate to final value of 1
            }
        ).start(); 
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{zIndex:9999,height:200,width:200,backgroundColor:'transparent',position:'absolute',top:140,left:(windowSize.width-200)/2,justifyContent:'center',alignItems:'center'}}>
                    <Animated.Image style={{opacity:this.state.opacity,height:50,width:180,resizeMode:'contain'}} source={Images.logo} />
                </View>
                <Animated.Image style={{opacity:this.state.opacity,height:windowSize.height,width:windowSize.width}} source={Images.ss1} /> 
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});


/*

LoginManager.logInWithReadPermissions(['public_profile', 'email', 'user_friends']).then(
    //...
    //..

    //..

).bind(this)

*/