'use strict';
import Realm from 'realm';


class CurrentPlaceSchema extends Realm.Object {}
  CurrentPlaceSchema.schema = {
    name: 'CurrentPlaceSchema',
    properties: {
      latitude: 'string',
      longitude: 'string',
    }
  };
class SPLoginSchema extends Realm.Object {}
  SPLoginSchema.schema = {
    name:'SPLoginSchema',
    properties: {
      username: 'string',
      password: 'string',
    }
  }
class FaceBookLoginSchema extends Realm.Object {}
  FaceBookLoginSchema.schema = {
    name:'FaceBookLoginSchema',
    properties: {
      username: 'string',
      password: 'string',
      fbtoken: 'string',
      email: 'string',
    }
  }
class AppIntro extends Realm.Object {}
  AppIntro.schema = {
    name:'AppIntro',
    properties: {
      username: 'string',
    }
  }

class CommentedData extends Realm.Object {}
  CommentedData.schema = {
    name: 'CommentedData',
    properties: {
      userID: 'string',
      magnetKey: 'string',
    }
  };
class FirebaseSubscriber extends Realm.Object {}
  FirebaseSubscriber.schema = {
    name: 'FirebaseSubscriber',
    properties: {
      userID: 'string',
    }
  };


export default new Realm({schema: [ CurrentPlaceSchema, SPLoginSchema, FaceBookLoginSchema, AppIntro, CommentedData, FirebaseSubscriber ],schemaVersion:1})
