import AppIntro from './pages/appIntro'
import Contracts from './pages/contracts'
import LoginPage from './pages/loginPage'
import SignupPage from './pages/signupPage'
import MagnetMain from './pages/magnet/magnetMain'
import ProfilMain from './pages/profil/profilMain'
import WishLists from './pages/profil/wishLists'


import ShowStory from './pages/profil/showStory'
import ShowCountryStories from './pages/profil/showCountryStories'
import ShowOtherCountryStories from './pages/profil/showOtherCountryStories'
import ProfileSettings from './pages/profil/profilSettings'
import ProfileAddStory from './pages/profil/profileAddStory'
import AddStoryWithoutLoc from './pages/profil/addStoryWithoutLoc'


import ProfilOther from './pages/profil/profilOther'
import AddLocation from './pages/profil/addLocation'
import MessageMain from './pages/message/messageMain'
import Room from './pages/message/room'
import SearchMain from './pages/search/searchMain'
import SearchPeople from './pages/search/searchPeople'
import SearchStory from './pages/search/searchStory'
import SearchTitle from './pages/search/searchTitle'

import NotificationMain from './pages/notification/notificationMain'


import ForgotPasswordPage from './pages/forgotPasswordPage'

import FollowInfo from './pages/follow/followInfo'

//TEST//
import TestMarkers from './pages/testPart/testMarkers'
import ImageEditor from './pages/testPart/imageEditor'


//TEST//

export const Routes = {
  appIntro: {screen: AppIntro},
  contracts: {screen: Contracts},
  signupPage: {screen: SignupPage},
  loginPage: {screen: LoginPage},
  magnetMain: {screen: MagnetMain},
  profilMain: {screen: ProfilMain},
  wishLists: {screen: WishLists},
  showCountryStories: {screen:ShowCountryStories},
  showOtherCountryStories:{screen:ShowOtherCountryStories},
  showStory: {screen: ShowStory},
  profileSettings: {screen: ProfileSettings},
  profileAddStory: {screen: ProfileAddStory},
  addStoryWithoutLoc: {screen: AddStoryWithoutLoc},
  profilOther:{screen:ProfilOther},
  addLocation: {screen: AddLocation},
  messageMain: {screen: MessageMain},
  room: {screen: Room},
  searchMain: {screen: SearchMain},
  searchPeople: {screen: SearchPeople},
  searchStory: {screen: SearchStory},
  notificationMain: {screen: NotificationMain},
  forgotPasswordPage: {screen: ForgotPasswordPage},
  followInfo: {screen: FollowInfo},
  


  testMarkers: {screen: TestMarkers},
  imageEditor: {screen: ImageEditor},
  
}
