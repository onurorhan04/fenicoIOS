import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,View,TextInput,Alert,Dimensions,Image,KeyboardAvoidingView,TouchableOpacity } from 'react-native';
var windowSize = Dimensions.get('window');
import Button from 'apsl-react-native-button';
import Images from '../images/images';
import KeepToken from '../stores/keeptoken';
import TextField from 'react-native-md-textinput';
import Colours from '../const/colours';

export default class ForgotPasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email:"",
      status:"",
      statusColor:"white",
    }
  }
  sendMail(email){
    var formData = new FormData();
    formData.append('email',email);
    fetch("https://fenicoapp.com/api/v1/user/forgot", {
      method: "POST",
      headers: {
        'Authorization' : "Bearer " + KeepToken.token,
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    })
    .then((response) => response.json())
    .then((responseData) => {
      if(responseData.status === true){
        this.setState({status:'Password reset message sent',statusColor:'#2ecc71'})
      }else{
        this.setState({status:responseData.message,statusColor:'#e74c3c'})
      }
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView>
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={{height:100,width:100,justifyContent:'center',alignItems:'center'}}>
                <Image style={{height:30,width:30}} source={Images.back}/>
              </TouchableOpacity>
            </View>
            <View style={{flex:8}}></View>
            
          </View>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontWeight:'bold',fontSize:17}}>Verification for password resetting</Text>
          </View>
        </View>
        <View style={{flex:1}}>
          <View style={{flex:5,justifyContent:'center',alignItems:'center'}}>
            <TextField
              onChangeText={(email) => this.setState({email:email})}
              label={"Username"}
              style={{height:50,width:windowSize.width-50}}/>
          </View>
          <View style={{justifyContent:'center',alignItems:'center'}}>
            <Button
              style={{color:'#FFFFFF',backgroundColor: Colours.lightSalmon,borderRadius:30,borderWidth:0,width:300}}
              textStyle={{fontSize: 18}}
              onPress={() => this.sendMail(this.state.email)}>
              <Text style={{color:'#FFFFFF'}}>Reset Password</Text>
            </Button>
          </View>
        </View>
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <View style={{alignItems:'center',height:100,width:250}}>
            <Text style={{color:this.state.statusColor}}>{this.state.status}</Text>
          </View>
          
        </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#fff'},
});