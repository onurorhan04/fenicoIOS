import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions
} from 'react-native';
import MapView ,{ Marker } from 'react-native-maps';
//import Images from '../../images/images';
import styles from './marker.styles';


const marker = ({ latitude, longitude, title, icon}) => {
    return (
        <Marker
          coordinate={{latitude:latitude,longitude:longitude}}
          title={title}
        >
          <Image style={{height:40,width:40,resizeMode:'contain'}} source={icon} />
        </Marker>
    )
}
export default marker