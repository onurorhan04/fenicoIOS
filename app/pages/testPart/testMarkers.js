import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions
} from 'react-native';
import MapView,{Marker,Callout} from 'react-native-maps';
import Images from '../../images/images';
import MarkerC from './view/marker.component'
var { width, height } = Dimensions.get('window')

var Firebase  = require('firebase');
import mockMarkers from './mockMarkers'
export default class testMarkers extends Component {
  constructor(props) {
    super(props);
    this.state={
      allMagnets:[],
      instantMagnet:[]
    }
  }

  
  handlePress(e){
    //this.pushNewMagnet(e.nativeEvent.coordinate.latitude,e.nativeEvent.coordinate.longitude)
    
    let newMagnet = {latitude:e.nativeEvent.coordinate.latitude,longitude:e.nativeEvent.coordinate.longitude,title:'asdasd'}
    this.setState({instantMagnet: newMagnet})
  }

  componentWillMount(){
    this.setState({allMagnets: mockMarkers})

  }
  render() {
    const { allMagnets, instantMagnet } = this.state
    return (
      <MapView
        onPress={this.handlePress.bind(this)}
        style={styles.mapContainer}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 100,
          longitudeDelta: 100,
        }}
      >
      <MarkerC latitude = {'41.054709'} longitude = {'28.916015'} title = {'Eyüp'} icon = {Images.pinGreen} />
        {allMagnets.map(marker => (
          <MarkerC latitude = {marker.latitude} longitude = {marker.longitude} title = {marker.title} icon = {Images.pinRed} />
        ))}
        <MarkerC latitude = {instantMagnet.latitude} longitude = {instantMagnet.longitude} title = {instantMagnet.title} icon = {Images.pinOrange} />
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  mapContainer:{position:'relative',flex: 1,height:height,width:width,opacity:1},
});
