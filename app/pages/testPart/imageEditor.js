
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
  ImageEditor,
  Alert,
  ScrollView,
  TextInput,
  Dimensions
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import {RichTextEditor, RichTextToolbar} from 'react-native-zss-rich-text-editor';
const windowSize = Dimensions.get('window');

export default class ImageEditoring extends Component {
    
    
    componentWillMount(){
       
    }
    componentDidMount(){
      
    }
    
   
  render() {
    return (
         <View style={styles.container}>
           <KeyboardAwareScrollView behavior="padding" style={{flex:1}}>
           <ScrollView style={{flex:1}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <View style={{height:300,width:300,backgroundColor:'#A0A0A0'}}></View>
              <View>
                <TextInput style={{height:40,width:150,backgroundColor:'red'}} placeHolder={'TITLE'} />
              </View>
              <View>
                <TextInput style={{height:40,width:150}} placeHolder={'DESCRIPTION'} />
              </View>
              <View style={{height:400,width:300}}></View>
            </View>
            
            <RichTextEditor
                ref={(r)=>this.richtext = r}
                style={styles.richText}
                customCSS = {{backgroundColor:'red'}}
                titlePlaceholder = {'Title'}
                contentPlaceholder = {'Description'}
                initialContentHTML={''}
                editorInitializedCallback={() => this.onEditorInitialized()}
              />
            
                
           <RichTextToolbar getEditor={() => this.richtext} />
           </ScrollView>
           </KeyboardAwareScrollView>
            
         </View> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
    backgroundColor: '#F5FCFF',
  },
  richText:{
    height:300,
    width:windowSize.width-30,
    margin:30
  }
});

