
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  FlatList,
  View,
  Alert,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native';
var { width, height } = Dimensions.get('window')
import Colours from '../../const/colours'
import Images from '../../images/images';
import KeepToken from '../../stores/keeptoken';
import { observer } from 'mobx-react/native';
import HTML from 'react-native-render-html';

export default class fenicoMac extends Component {
    constructor(props){
        super(props)
        this.state = {
            userID:this.props.navigation.state.params.userID,
            allData:[],
            filteredData:[],
            filterIndex:10,
            noStoryMoreData: false,
            isLoaded: false
        }
    }
    getWishLists(){
        fetch("https://fenicoapp.com/api/v1/user/view?id="+ this.state.userID +"&expand=profile,userWishlists", {
            method: "GET",
            headers: {
                'Authorization': 'Bearer ' + KeepToken.token
            }
            })
            .then((response) => response.json())
            .then((responseData) => {
            //console.log(responseData)
            var data = responseData.userWishlists.reverse()
            console.log('data', data)
            this.setState({
                 allData: data, 
                 filteredData:data.slice(0,this.state.filterIndex),
                 filterIndex:this.state.filterIndex + 10,
                 isLoaded: true
            }) 
                //Alert.alert('',JSON.stringify( this.state.filteredData))
           
        })
    }
    loadExtraWishListsData(){
        if(this.state.allData.length < this.state.filterIndex){
            this.setState({noStoryMoreData:true})
        }
        this.setState({
            filteredData: this.state.allData.slice(0,this.state.filterIndex),
            filterIndex:this.state.filterIndex + 10
        })
    }
    componentWillMount(){
        //this.getWishLists()
    }
    componentDidMount(){
        this.getWishLists()
    }
    routingToShowStory(storyID){
      this.props.navigation.navigate('showStory',{storyID:storyID})
  }
    renderRow(item){
        return(
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.routingToShowStory(item.item.story_id)} style={[{flexDirection:'row',height:'auto',minHeight:45,width:width,borderBottomWidth:1,borderColor:'#ddd'}, item.index % 2 === 0 ? styles.white : styles.gray  ]}>
                <View style={{flex:1}}>
                    <Image style={{height:null,width:null,flex:1,resizeMode:'contain'}} source={{uri: item.item.image_link}} />
                </View>
                <View style={{flex:3,borderLeftWidth:1,borderColor:'#e0e0e0'}}>
                    <View style={{padding:5}}>

                        <HTML  html={item.item.plan} /> 
                    </View> 

                </View>
                
            </TouchableOpacity>
        )

    }
    _keyExtractor = (item, index) => item.id;
    renderStoryListFooter(){
        if(this.state.noStoryMoreData){
            return(
                <View style={{height:40,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontWeight:'700',color:'#a0a0a0'}}>No More Stories</Text>
                </View>
            )
        }else{
            return(
                <TouchableOpacity onPress={() => this.loadExtraWishListsData()} style={{height:40,justifyContent:'center',alignItems:'center'}}>
                    <View style={{height:26,width:120,borderRadius:13,backgroundColor:'#e0e0e0',justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontWeight:'700',color:'#a0a0a0'}}>Load More</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }
    render() {
        return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerBlank}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.headerLeft}>
                    <Image style={styles.headerLeftImage} source={Images.leftArrow} />
                    </TouchableOpacity>
                    <View style={styles.headerMiddle}>
                    <Text style={styles.headerMiddleText}>Wish List</Text>
                    </View>
                    <View onPress={() => this.save()} style={styles.headerRight}>
                    <Text style={styles.headerRightText}></Text>
                    </View>
                </View>
            </View>
            {this.state.isLoaded ? 
            <FlatList
                style={{flex:1,marginTop:5}}
                data={this.state.filteredData}
                renderItem={this.renderRow.bind(this)}
                keyExtractor={this._keyExtractor}
                ListFooterComponent={this.renderStoryListFooter.bind(this)}
            />
            :
            <View style={{flex:1}}></View> 
            }
            
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
headerBlank:{marginTop:20,height:50,width:width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  header:{height:70,width:width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerLeft:{flex:1,justifyContent:'center',alignItems:'center'},
  headerMiddle:{flex:4,justifyContent:'center',alignItems:'center'},
  headerRight:{flex:1,justifyContent:'center',alignItems:'center'},
  headerRightText:{color:'#FFF',fontWeight:'bold'},
  headerMiddleText:{color:'#FFF',fontWeight:'bold',fontSize:18},
  headerLeftImage:{height:25,width:25},
  gray:{backgroundColor:'#fff'},
  white:{backgroundColor:'#ecf0f1'}

});
