import React, { Component } from 'react';
import {Keyboard,AppRegistry,StyleSheet,Alert,Text,View,Dimensions,Platform,Image,TextInput,TouchableOpacity,ScrollView} from 'react-native';
const windowSize = Dimensions.get('window');
import Images from '../../images/images';
import Swiper from 'react-native-swiper';
import { observer } from 'mobx-react/native';
import KeepToken from '../../stores/keeptoken';
import UserData from '../../stores/userData';
import Colours from '../../const/colours'
import AddStoryStore from '../../stores/addStoryStore';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import DateTimePicker from 'react-native-modal-datetime-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Toast from '../../components/toast';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import {RichTextEditor, RichTextToolbar} from '../../components/react-native-zss-rich-text-editor';

@observer
export default class ProfileAddStory extends Component {
  constructor(props) {
    super(props);
    this.state={
      location: "",
      date: "",
      title: "",
      description: "",
      isDateTimePickerVisible: false,
      changeOrAdd: "",
      currentIndex: "",
      loadingOverlay: false,
      visibleToast: false,
      toastMessage: "",
      richTextFocused : false,
      phase1: true,
      phase2: false,
      showToolbar: false
      //keyboardStatus: false
    }
    this.setFocusHandlers = this.setFocusHandlers.bind(this);
  }
  fetchUserCountriesData(){
    fetch("https://fenicoapp.com/api/v1/user/countries?id="+KeepToken.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      UserData.countryData = responseData.data

      this.setState({loadingOverlay: false, visibleToast: 1, toastMessage: 'Story successfully created..'})
      this.props.navigation.goBack(null)
      //setTimeout(() => this.setState({visibleToast:false,toastMessage:false}), 2000);
    })
  }
  fetchUserAllData(){
    fetch("https://fenicoapp.com/api/v1/user/view?id="+ KeepToken.userID +"&expand=profile,userWishlists,userStories", {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      UserData.data = responseData
      UserData.storyData = responseData.userStories
      reversedStoryData = UserData.storyData.reverse()
      UserData.storyData = reversedStoryData
      UserData.wishListData = responseData.userWishlists
      UserData.location = responseData.profile.location
      this.limitStoryData()
      this.fetchUserCountriesData()
    })
  }
  limitStoryData(){
    //if(UserData.storyData.length == 0 ){ this.setState({ noStoryMoreData: true }) }
    //eger lenght 5 ten fazla ise  önceden yapılmıs bi daha yapma ..
    if(UserData.limitedStoryData.length > 5){
      if(UserData.limitedStoryData.length == UserData.storyData.length){
        //this.setState({noStoryMoreData:true})
      }
    }else{
      UserData.limitedStoryData = UserData.storyData.slice(0,5)
    }
  }
  save(){
    this.getHTML().then((data) => {
      if(AddStoryStore.selectedLocationName != "" && this.state.title != "" && this.state.description != "" && this.state.date != ""){
        if(AddStoryStore.photoNum1 != "" || AddStoryStore.photoNum2 != "" || AddStoryStore.photoNum3 != "" || AddStoryStore.photoNum4 != "" || AddStoryStore.photoNum5 != ""){
          this.setState({loadingOverlay: true})
          RNFetchBlob.fetch('POST', 'https://fenicoapp.com/api/v1/user-story/create', {
              'Authorization' : "Bearer " + KeepToken.token,
              otherHeader : "foo",
              'Content-Type' : 'multipart/form-data',
            }, [
              { name : 'story', data : this.state.description},
              { name : 'title', data : this.state.title},
              { name : 'olusturuldu', data:this.state.date.toString()},
              { name : 'city', data : AddStoryStore.selectedLocationID.toString()},
              { name : 'image1', data: RNFetchBlob.wrap(AddStoryStore.photoNum1.uri)},
              { name : 'image2', data: RNFetchBlob.wrap(AddStoryStore.photoNum2.uri)},
              { name : 'image3', data: RNFetchBlob.wrap(AddStoryStore.photoNum3.uri)},
              { name : 'image4', data: RNFetchBlob.wrap(AddStoryStore.photoNum4.uri)},
              { name : 'image5', data: RNFetchBlob.wrap(AddStoryStore.photoNum5.uri)},
            ]).then((resp) => {
              this.fetchUserAllData();
            }).catch((err) => {
              //Alert.alert('Error','Something went wrong..')
            })
        }else{
          Alert.alert('Error','Fill in the blank spaces..')
        }
      }else{
        Alert.alert('Error','Fill in the blank spaces..')
      }
    })
  }
  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
  _handleDatePicked = (date) => {
    jsonDate = JSON.stringify(date);
    var year = jsonDate.slice(1,5);
    var month = jsonDate.slice(6,8);
    var day = jsonDate.slice(9,11);
    //ProfileStories.selectedDate = ;
    this.setState({date:day + "-" + month + "-" + year})

    this._hideDateTimePicker();
  };
  resetStore(){
    AddStoryStore.selectedLocationID = ""
    AddStoryStore.selectedLocationName = ""
    AddStoryStore.selectedDate = ""
    AddStoryStore.photoNum1 = ""
    AddStoryStore.photoNum2 = ""
    AddStoryStore.photoNum3 = ""
    AddStoryStore.photoNum4 = ""
    AddStoryStore.photoNum5 = ""
  }
  componentDidMount(){
    
  }
  componentWillMount(){
    this.resetStore()
    if(AddStoryStore.photoNum1 == ""){
      this.setState({changeOrAdd:'Add'})
    }else{
      this.setState({changeOrAdd:'Change'})
    }
    //this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
    //this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
  }
  componentWillUnmount () {
    //this.keyboardDidShowListener.remove();
    //this.keyboardDidHideListener.remove();
  }
  setFocusHandlers() {
    this.richtext.setTitleFocusHandler(() => {
      this.setState({ richTextFocused: true })
    });
    this.richtext.setContentFocusHandler(() => {
      this.setState({ richTextFocused: true })
    });
  }
  onEditorInitialized() {
    //this.setState({ showToolbar: true })
    this.setFocusHandlers();
  }
  /*_keyboardDidShow () {
    this.setState({ keyboardStatus: true })
  }
  _keyboardDidHide () {
    this.setState({ keyboardStatus: false })
  }*/
  clearAllPhotos(){
    AddStoryStore.photoNum1 = ""
    AddStoryStore.photoNum2 = ""
    AddStoryStore.photoNum3 = ""
    AddStoryStore.photoNum4 = ""
    AddStoryStore.photoNum5 = ""
  }
  openLibrary(){
    var options = {
      title: 'Select Avatar',
      quality:1,
      maxWidth:400,
      noData:false,
      customButtons: [
        {name: 'fb', title: 'Choose Photo from Facebook'}
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {}
      else if (response.error) {}
      else if (response.customButton) {}
      else {
        const source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};
        if (Platform.OS === 'ios') {
          const source = {uri: response.uri.replace('file://', ''), isStatic: true};
        } else {
          const source = {uri: response.uri, isStatic: true};
        }
        if(source){
          if(this.state.currentIndex == 0 || this.state.currentIndex == ""){AddStoryStore.photoNum1 = source}
          if(this.state.currentIndex == 1){AddStoryStore.photoNum2 = source}
          if(this.state.currentIndex == 2){AddStoryStore.photoNum3 = source}
          if(this.state.currentIndex == 3){AddStoryStore.photoNum4 = source}
          if(this.state.currentIndex == 4){AddStoryStore.photoNum5 = source}
        }
      }
    });
  }
  async getHTML() {
    const titleHtml = await this.richtext.getTitleHtml();
    const contentHtml = await this.richtext.getContentHtml();
    this.setState({ title: titleHtml, description: contentHtml })
    const data = {titleHtml: titleHtml, contentHtml: contentHtml}
    return data
  }
  indexControl(state){
    this.setState({currentIndex:state.index})
    if(state.index == 0 && AddStoryStore.photoNum1 == ""){this.setState({changeOrAdd:'Add'})}
    if(state.index == 0 && AddStoryStore.photoNum1 != ""){this.setState({changeOrAdd:'Change'})}
    if(state.index == 1 && AddStoryStore.photoNum2 == ""){this.setState({changeOrAdd:'Add'})}
    if(state.index == 1 && AddStoryStore.photoNum2 != ""){this.setState({changeOrAdd:'Change'})}
    if(state.index == 2 && AddStoryStore.photoNum3 == ""){this.setState({changeOrAdd:'Add'})}
    if(state.index == 2 && AddStoryStore.photoNum3 != ""){this.setState({changeOrAdd:'Change'})}
    if(state.index == 3 && AddStoryStore.photoNum4 == ""){this.setState({changeOrAdd:'Add'})}
    if(state.index == 3 && AddStoryStore.photoNum4 != ""){this.setState({changeOrAdd:'Change'})}
    if(state.index == 4 && AddStoryStore.photoNum5 == ""){this.setState({changeOrAdd:'Add'})}
    if(state.index == 4 && AddStoryStore.photoNum5 != ""){this.setState({changeOrAdd:'Change'})}
  }
  routing(routeName){
    this.props.navigation.navigate(routeName)
  }
  render() {
    
    if(this.state.phase2){
      return(
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={styles.headerBlank}>
              <TouchableOpacity onPress={() => this.setState({ phase2:false, phase1: true })} style={styles.headerLeft}>
              <Image style={styles.headerLeftImage} source={Images.leftArrow} />
              </TouchableOpacity>
              <View style={styles.headerMiddle}>
                <Text style={styles.headerMiddleText}>Add a Story</Text>
              </View>
              <TouchableOpacity onPress={() => this.save()} style={styles.headerRight}>
                <Text style={styles.headerRightText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          
            <View style={{height:400,marginTop:10,width:windowSize.width,justifyContent:'center',alignItems:'center'}}>
                <RichTextEditor
                  ref={(r)=>this.richtext = r}
                  style={styles.richText}
                  titlePlaceholder = {'Title'}
                  contentPlaceholder = {'Description'}
                  initialContentHTML={''}
                  editorInitializedCallback={() => this.onEditorInitialized()}
                />
                
            </View>
            <View style={{height:50,position:'absolute',left:0,top:10,width:windowSize.width,justifyContent:'center',alignItems:'center'}}>
              <RichTextToolbar getEditor={() => this.richtext} />
            </View>

          </View>
          <OrientationLoadingOverlay
            visible={this.state.loadingOverlay}
            color="white"
            indicatorSize="large"
            messageFontSize={24}
            message="Story is being created.."
            />
        </View>
      ) 
    }
    if(this.state.phase1){
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={styles.headerBlank}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.headerLeft}>
              <Image style={styles.headerLeftImage} source={Images.leftArrow} />
              </TouchableOpacity>
              <View style={styles.headerMiddle}>
                <Text style={styles.headerMiddleText}>Add a Story</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({ phase1: false, phase2: true })} style={styles.headerRight}>
                <Text style={styles.headerRightText}>Next</Text>
              </TouchableOpacity>
            </View>
          </View>
          <ScrollView ref="scrollView" style={{flex:1}}>

          <View style={styles.content}>
            <TouchableOpacity onPress={() => this.routing('addLocation')} style={styles.locationPlace}>
              <View style={styles.locationView}>
                {AddStoryStore.selectedLocationName === "" ?
                  <Text style={styles.locationText}>Select a Location</Text>
                :
                  <Text style={styles.locationText}>{AddStoryStore.selectedLocationName}</Text>
                }
              </View>
              <Image style={styles.locationImage} source={Images.grayLocation2} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this._showDateTimePicker} style={styles.calenderPlace}>
              <View style={styles.calenderView}>
                {this.state.date === "" ?
                  <Text style={styles.calenderText}>Select a Date</Text>
                  :
                  <Text style={styles.calenderText}>{this.state.date}</Text>
                }

              </View>
              <Image style={styles.calenderImage} source={Images.calendar} />
            </TouchableOpacity>
            <View style={styles.swiperContent}>
              <Swiper onMomentumScrollEnd={(e, state, context) => this.indexControl(state)} showsPagination={false} height={250} width={windowSize.width-40} >

                <TouchableOpacity onPress={() => this.openLibrary()} style={styles.photoView}>
                  {AddStoryStore.photoNum1 == "" ?
                    <Text style={styles.addPhotoText}>Add Photo</Text>
                    :
                    <Image style={{height:250,width:windowSize.width-40,resizeMode:'contain'}} source={AddStoryStore.photoNum1} />
                  }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.openLibrary()} style={styles.photoView}>
                  {AddStoryStore.photoNum2 == "" ?
                    <Text style={styles.addPhotoText}>Add Photo</Text>
                    :
                    <Image style={{height:250,width:windowSize.width-40,resizeMode:'contain'}} source={AddStoryStore.photoNum2} />
                  }
                </TouchableOpacity >
                <TouchableOpacity onPress={() => this.openLibrary()} style={styles.photoView}>
                  {AddStoryStore.photoNum3 == "" ?
                    <Text style={styles.addPhotoText}>Add Photo</Text>
                    :
                    <Image style={{height:250,width:windowSize.width-40,resizeMode:'contain'}} source={AddStoryStore.photoNum3} />
                  }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.openLibrary()} style={styles.photoView}>
                  {AddStoryStore.photoNum4 == "" ?
                    <Text style={styles.addPhotoText}>Add Photo</Text>
                    :
                    <Image style={{height:250,width:windowSize.width-40,resizeMode:'contain'}} source={AddStoryStore.photoNum4} />
                  }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.openLibrary()} style={styles.photoView}>
                  {AddStoryStore.photoNum5 == "" ?
                    <Text style={styles.addPhotoText}>Add Photo</Text>
                    :
                    <Image style={{height:250,width:windowSize.width-40,resizeMode:'contain'}} source={AddStoryStore.photoNum5} />
                  }
                </TouchableOpacity>

              </Swiper>
              
            </View>
            <View style={styles.imageButtonPlace}>
                <TouchableOpacity onPress={() => this.openLibrary()} style={styles.imageButton}>
                  <Text>{this.state.changeOrAdd}</Text>
                </TouchableOpacity>
                <View style={styles.imageButtonSeperator}></View>
                <TouchableOpacity onPress={() => this.clearAllPhotos() } style={styles.imageButton}>
                  <Text>Clear All Photos</Text>
                </TouchableOpacity>
              </View>
              
              
          
          </View>
          </ScrollView>
          
          <View style={styles.blankSpace}>
            
          </View>
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />
          
            <Toast
              visible={this.state.visibleToast === 1}
              position={windowSize.height-150}
              shadow={true}
              animation={true}
              hideOnPress={true}
            >{this.state.toastMessage}</Toast>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container:{flex:1,backgroundColor:'#FFF'},
  header:{height:70,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerBlank:{marginTop:20,height:50,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerLeft:{flex:1,justifyContent:'center',alignItems:'center'},
  headerMiddle:{flex:4,justifyContent:'center',alignItems:'center'},
  headerRight:{flex:1,justifyContent:'center',alignItems:'center'},
  headerRightText:{color:'#FFF',fontWeight:'bold'},
  headerMiddleText:{color:'#FFF',fontWeight:'bold',fontSize:18},
  headerLeftImage:{height:25,width:25},
  content:{flex:1,backgroundColor:'white',alignItems:'center',marginTop:10},
  locationView:{flex:1,height:40,justifyContent:'center'},
  locationText:{marginLeft:10},
  locationPlace:{margin:10,height:50,width:windowSize.width-30,borderWidth:1,borderColor:'#E0E0E0',flexDirection:'row',justifyContent:'center',alignItems:'center'},
  locationImage:{height:25,width:25,marginRight:5},
  calenderView:{flex:1,height:40,justifyContent:'center'},
  calenderText:{marginLeft:10},
  calenderPlace:{margin:10,height:50,width:windowSize.width-30,borderWidth:1,borderColor:'#E0E0E0',flexDirection:'row',justifyContent:'center',alignItems:'center'},
  calenderImage:{height:25,width:25,marginRight:5},
  swiperContent:{flex:1,width:windowSize.width-40,justifyContent:'center',alignItems:'center'},
  swiper:{height:100,width:null},
  imageButtonPlace:{height:50,width:windowSize.width-10,flexDirection:'row'},
  imageButton:{flex:4,borderWidth:0.5,borderColor:'#E0E0E0',borderRadius:5,justifyContent:'center',alignItems:'center'},
  imageButtonSeperator:{flex:1},
  textInputTitlePlace:{height:80,width:windowSize.width-30,marginTop:10,borderWidth:1,borderColor:'#E0E0E0',alignItems:'center'},
  textInputTitle:{height:80,width:windowSize.width-30,fontSize:15},
  textInputDescriptionPlace:{height:80,width:windowSize.width-30,marginTop:10,borderWidth:1,borderColor:'#E0E0E0',alignItems:'center'},
  textInputDescription:{height:80,width:windowSize.width-30,fontSize:15},
  swiperImages:{resizeMode:'stretch',height:200,width:null},
  photoView:{height:200,width:windowSize.width-40,justifyContent:'center',alignItems:'center'},
  blankSpace:{height:50},
  addPhotoText:{fontWeight:'800',color:'#A0A0A0',fontSize:18},
  richText:{height:400,width:windowSize.width-30,marginTop:20}
});



/*
<View style={styles.textInputTitlePlace}>
            <TextInput multiline={true} onChangeText={(text) => this.setState({title:text}) }  placeholder={'Title'} placeholderTextColor={'gray'} style={styles.textInputTitle}  />

          </View>
          <View style={styles.textInputDescriptionPlace}>
            <TextInput multiline={true} onChangeText={(text) => this.setState({description:text}) } placeholder={'Description'} placeholderTextColor={'gray'} style={styles.textInputDescription} />
          </View>

*/
