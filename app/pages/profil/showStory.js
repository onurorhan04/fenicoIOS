import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Dimensions,
  TextInput
} from 'react-native';
var windowSize = Dimensions.get('window');
import Swiper from 'react-native-swiper';
import { observer } from 'mobx-react/native';
import Images from '../../images/images';
import Colours from '../../const/colours'
import Footer from '../../components/footer'
import ShowStories from '../../stores/showStories';
import UserData from '../../stores/userData';
import KeepToken from '../../stores/keeptoken';
import Toast from '../../components/toast';
import Modal from 'react-native-modal'
import Firebase  from 'firebase';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import HTML from 'react-native-render-html';
import moment from 'moment'

@observer
export default class ShowStory extends Component {
  constructor(props){
    super(props)
    this.state={
      isLoading: false,
      visibleToast: false,
      toastMessage: '',
      reportingModal: false,
      reportingText: '',
      loadingOverlay: false,
      loadingOverlayMessage: ''
    }
  }
  getCityDetails(cityID){
    if(cityID !== null){
      fetch('https://fenicoapp.com/api/v1/cities/view?id='+cityID, {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + KeepToken.token
        }
      }).then((resp) => resp.json())
        .then((responseData) => {
          ShowStories.cityDetails = responseData.city + "," + responseData.country
      })
    }else{
      ShowStories.cityDetails = null
    }
  }
  getToken(username,password) {
      var params = {
          username: base64.encode(username),
          password: base64.encode(password)
      };
      var formData = new FormData();
      for (var k in params) {
          formData.append(k, params[k]);
      }
      fetch("https://fenicoapp.com/api/v1/api/access-token", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data'
        },
        body: formData
      })
      .then((response) => response.json())
      .then((responseData) => {
        //Alert.alert(JSON.stringify(responseData))
        if(responseData.status == true){
          KeepToken.userID=responseData.user_id
          KeepToken.token=responseData.access_token
          KeepToken.username = username
          KeepToken.password = password

          fetch('https://fenicoapp.com/api/v1/user-story/view?id='+ShowStories.storyID, {
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + KeepToken.token
            }
          }).then((resp) => resp.json())
            .then((responseData) => {

              ShowStories.storyData = responseData.items[0]
              ShowStories.likeCount = ShowStories.storyData.like_count
              this.getCityDetails(ShowStories.storyData.city)
              //processing
              var likes = responseData.items[0].userStoryLikes

              for(var i = 0;i<likes.length;i++){
                if(likes[i].olusturan == KeepToken.userID){
                  ShowStories.heartStatus = true
                }
              }
              this.setState({
                isLoading:true
              })


          })
        }
      })
      .done();
  }
  getStoryData(){
    ShowStories.heartStatus = false
    ShowStories.storyID = this.props.navigation.state.params.storyID
    fetch('https://fenicoapp.com/api/v1/user-story/view?id='+ShowStories.storyID, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    }).then((resp) => resp.json())
      .then((responseData) => {
        //
        if(responseData.status != null){
          if(responseData.status == 401){
            this.getToken(KeepToken.username,KeepToken.password)
          }
        }else{
          //console.log('responseData: ',responseData.items[0])
          ShowStories.storyData = responseData.items[0]
          ShowStories.likeCount = ShowStories.storyData.like_count
          this.getCityDetails(ShowStories.storyData.city)

          //processing
          var likes = responseData.items[0].userStoryLikes

          for(var i = 0;i<likes.length;i++){

            if(likes[i].olusturan == KeepToken.userID){
              ShowStories.heartStatus = true

            }
          }
          this.setState({
            isLoading:true
          })
        }

      //Alert.alert('',JSON.stringify(resp))
    }).catch((error) => {
        //console.error(error)
        //this.getToken(KeepToken.username,KeepToken.password)
    });
  }
  componentWillMount(){
    this.getStoryData()
  }
  async clickedHeart(storyID){
    var currentHeartStatus = ShowStories.heartStatus
    ShowStories.heartStatus = !ShowStories.heartStatus


    if(currentHeartStatus){
      ShowStories.likeCount = ShowStories.likeCount - 1
      await fetch("https://fenicoapp.com/api/v1/user-story-like/delete?story_id=" + storyID, {
        method: "DELETE",
        headers: {
          'Authorization': 'Bearer ' + KeepToken.token
        }
      })
      .then((response) => response.json())
      .then((responseData) => {

      }).catch((error) => {
          //Alert.alert('Error',JSON.stringify(error))
      });
    }else{
      ShowStories.likeCount = ShowStories.likeCount + 1
      var formData = new FormData();
      formData.append('story',storyID);
      await fetch('https://fenicoapp.com/api/v1/user-story-like', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer ' + KeepToken.token
        },
        body: formData
      }).then((resp) => {
        if(resp.status == "success" || resp.status == 200){

        }
      }).catch((error) => {
        //Alert.alert('Error',JSON.stringify(error))
      })

    }
  }
  showToastMessage(text,timeout){
    this.setState({visibleToast:true,toastMessage:text.toString()})
    setTimeout(() => this.setState({visibleToast:false,toastMessage:""}), timeout);
  }
  addToWishlist( storyID, wishlistText, image ){
    var formData = new FormData()
    formData.append('story_id', storyID)
    formData.append('plan', wishlistText)
    formData.append('image_link', image)
    
    fetch('https://fenicoapp.com/api/v1/user-wishlist/create', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + KeepToken.token
      },
      body:formData
    })
    .then((response) => response.json())
    .then((responseData) => {
      if(responseData.status == "success"){
        UserData.wishListData.push({'plan':wishlistText.toString()})
        this.showToastMessage("WishList created succesfully..",2000)
      }
    })
  }
  routeToUser(){
    if(ShowStories.storyData.user.id == KeepToken.userID){
      this.props.navigation.navigate('profilMain')
    }else{
      this.props.navigation.navigate('profilOther',{userID:ShowStories.storyData.user.id,username:ShowStories.storyData.user.name})
    }
  }
  fetchUserCountriesData(){
    fetch("https://fenicoapp.com/api/v1/user/countries?id="+KeepToken.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      UserData.countryData = responseData.data
      this.setState({loadingOverlayMessage:'Story removed, You will be redirected to the previous page within few seconds..'})
      setTimeout(() => this.redirect(), 2000);
    })
  }
  fetchUserAllData(){
    fetch("https://fenicoapp.com/api/v1/user/view?id="+ KeepToken.userID +"&expand=profile,userWishlists,userStories", {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      UserData.data = responseData
      UserData.storyData = responseData.userStories
      UserData.limitedStoryData = UserData.storyData.slice(0,UserData.shownStoryDataLength)
      UserData.wishListData = responseData.userWishlists
      UserData.location = responseData.profile.location

      this.fetchUserCountriesData()

    })
  }
  deleteStory(){
    this.setState({loadingOverlay: true,loadingOverlayMessage:'Story is removing..'})
    fetch("https://fenicoapp.com/api/v1/user-story/delete?id=" + ShowStories.storyData.id, {
      method: "DELETE",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.fetchUserAllData()
    }).catch((err) => {
      this.fetchUserAllData()
    })
  }
  redirect(){
    this.setState({loadingOverlay: false})
    this.props.navigation.goBack(null)
  }
  reporting(){
    const { id, title, story, city } = ShowStories.storyData
    const userID = ShowStories.storyData.user.id
    const username = ShowStories.storyData.user.name
    const reportRef = Firebase.database().ref().child("Reports").child('Story').child(id);
    reportRef.push({id: id, username: username, title: title, story: story, city: city, reportingText: this.state.reportingText})
    this.setState({reportingModal: false, reportingText: ''})
  
    this.showToastMessage('Story reported.',2000)
  }
  render(){
    if(this.state.isLoading){
      if(ShowStories.storyData.user.image != undefined){
        if(ShowStories.storyData.user.image.length>60){
          var profilImagePlace = <Image style={{height:50,width:50,borderRadius:25}} source={{uri:ShowStories.storyData.user.image}} />
        }else{
          var profilImagePlace = <Image style={{height:50,width:50,borderRadius:25}} source={Images.noImage} />
        }

      }

    return (
      <View style={styles.container}>
        <Modal style={{justifyContent:'center',alignItems:'center'}} isVisible={this.state.reportingModal}>
            <View style={{height:220,width:windowSize.width-50,backgroundColor:'#fff',borderRadius:6}}>
              <View style={{flex:1.2,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                <View style={{flex:1}}></View>
                <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:'#e74c3c',fontSize:16}} >Reporting</Text>
                </View>
                <TouchableOpacity onPress={() => this.setState({reportingModal: false}) } style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Image style={{height:13,width:13}} source={Images.grayCancel} />
                </TouchableOpacity>
                
              </View>
              <View style={{flex:3}}>
                <TextInput multiline={true} onChangeText={(text) => this.setState({reportingText:text})} placeholder={"Please, describe the problem"} placeholderTextColor={'#A0A0A0'}  style={{marginLeft:10,borderWidth:1,borderColor:'#d0d0d0',height:95,width:windowSize.width-70,fontSize:15}} />
              </View>
              <TouchableOpacity onPress={() => this.reporting()} style={{flex:1.2,flexDirection:'row',backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center',borderBottomLeftRadius:6,borderBottomRightRadius:6}}>
                  <View style={{height:35,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:'#fff',fontSize:18}}>Report</Text>
                  </View>
              </TouchableOpacity>
            </View>
        </Modal>
        <View style={styles.header}>  
          <View style={styles.headerBlank}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.headerLeft}>
             <Image style={styles.headerLeftImage} source={Images.leftArrow} />
            </TouchableOpacity>
            <View style={styles.headerMiddle}>
              <Text style={styles.headerMiddleText}>Story</Text>
            </View>
            <View style={styles.headerRight}>
              <Text style={styles.headerRightText}></Text>
            </View>
          </View>
        </View>
        <ScrollView style={{margin:10,backgroundColor:'transparent'}}>
          <View style={styles.borderContent}>
            <View style={{height:60,flexDirection:'row'}}>
              <TouchableOpacity onPress={() => this.routeToUser()} style={{flex:5,flexDirection:'row'}}>
                <View style={{flex:1,marginLeft:10,justifyContent:'center',alignItems:'center'}}>
                  {profilImagePlace}
                </View>
                <View style={{flex:4,flexDirection:'column',justifyContent:'center'}}>
                  <Text style={{color:'#808080',marginLeft:10,fontSize:15,fontWeight:'bold'}}>{ShowStories.storyData.user.name}</Text>
                  <Text style={{color:'#A0A0A0',marginLeft:10,fontSize:12}}>{ShowStories.cityDetails} {moment(ShowStories.storyData.olusturuldu).format("MMM Do YY") == "Invalid date" ? "" : moment(ShowStories.storyData.olusturuldu).format("MMM Do YY") } </Text>
                </View>
              </TouchableOpacity>
              {ShowStories.storyData.user.id != KeepToken.userID ? 
                <TouchableOpacity onPress={() => this.setState({reportingModal: true})} style={{flex:1,margin:7,justifyContent:'center',alignItems:'flex-end'}}>
                
                 <Image style={{height:30,width:30}} source={Images.extraGray} />
                </TouchableOpacity> 
              : 
               <TouchableOpacity onPress={() => this.deleteStory() } style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
                  <Image style={{height:30,width:30}} source={Images.trash} /> 
                </TouchableOpacity>
              }
              
            </View>
            <View style={{height:300,width:windowSize.width-20,backgroundColor:'white'}}>
              <Swiper width={windowSize.width-20} horizontal={true} removeClippedSubviews={false} showsPagination={false} >
              {ShowStories.storyData.image.map((image) => {
                return (
                  <View style={{justifyContent:'center',alignItems:'center',width:windowSize.width-20}} >
                    <Image style={{height:300,width:windowSize.width-50,resizeMode:'contain'}} source={{uri:image}} />
                  </View>
                )
              })}
              </Swiper>
            </View>
            <View style={{marginLeft:15,marginRight:15,marginBottom:15}}>

              <HTML html={ShowStories.storyData.title} />
              <HTML html={ShowStories.storyData.story} />
  
            </View>

            <View style={{flex:1,height:50,width:null,borderTopWidth:1,borderColor:'#A0A0A0',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.addToWishlist(ShowStories.storyData.id,ShowStories.storyData.title,ShowStories.storyData.image[0])} style={{marginLeft:10}}>
                <Image style={{height:22,width:22}} source={Images.addToWishList}/>
              </TouchableOpacity>
            <View style={{flex:1}}></View>
              <TouchableOpacity onPress={() => this.clickedHeart(ShowStories.storyData.id)} style={{flex:1,flexDirection:'row',justifyContent:'flex-end',alignItems:'center'}}>
                <Text style={{color:'#ffa500'}}>{ShowStories.likeCount}</Text>
                <Image style={{marginRight:5,height:28,width:28}} source={ShowStories.heartStatus == false ? Images.heartbordered : Images.heartfilled} />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <View style={{height:50}}></View>
        <Toast
            visible={this.state.visibleToast}
            position={windowSize.height-150}
            shadow={true}
            animation={true}
            hideOnPress={true}
            >{this.state.toastMessage}</Toast>
        <OrientationLoadingOverlay
            visible={this.state.loadingOverlay}
            color="white"
            indicatorSize="large"
            messageFontSize={24}
            message={this.state.loadingOverlayMessage}/>

        <Footer />
      </View>
    );
  }else{
    return null
  }
  }
}

const styles = StyleSheet.create({
  container: {flex: 1,backgroundColor: '#e9edf1',},
  content:{flex:1,borderWidth:1,borderColor:'black'},
  borderContent:{flex:1,borderWidth: 1,borderColor: '#ddd',borderBottomWidth: 0,backgroundColor:'white'},
  headerBlank:{marginTop:windowSize.height < 500 ? 0 : 20 ,height:50,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  header:{height:windowSize.height < 500 ? 50 : 70 ,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerLeft:{flex:1,justifyContent:'center',alignItems:'center'},
  headerMiddle:{flex:4,justifyContent:'center',alignItems:'center'},
  headerRight:{flex:1,justifyContent:'center',alignItems:'center'},
  headerRightText:{color:'#FFF',fontWeight:'bold'},
  headerMiddleText:{color:'#FFF',fontWeight:'bold',fontSize:windowSize.height < 500  ? 15 : 18},
  headerLeftImage:{height:25,width:25},
});
