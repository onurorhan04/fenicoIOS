import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,View,TextInput,ListView,Alert,Dimensions,TouchableOpacity,TouchableWithoutFeedback,Image} from 'react-native';
const windowSize = Dimensions.get('window');
import KeepToken from '../../stores/keeptoken';
import AddStoryStore from '../../stores/addStoryStore';
import Images from '../../images/images';
import Colours from '../../const/colours'
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { observer } from 'mobx-react/native';
import {createResponder} from 'react-native-gesture-responder';
@observer
export default class AddLocation extends Component {
  constructor(props){
    super(props)
    //this._textInput = this._textinput.bind(this)
  }
  componentWillMount(){
    AddStoryStore.searchedLocation = []
  }
  componentDidMount(){
    this._textInput.isFocused();
  }
  setResponder(){
    this.gestureResponder = createResponder({
      onStartShouldSetResponder: (evt, gestureState) => true,
      onStartShouldSetResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetResponder: (evt, gestureState) => true,
      onMoveShouldSetResponderCapture: (evt, gestureState) => true,
      onResponderGrant: (evt, gestureState) => {
        console.log('1',evt,gestureState)
      },
      onResponderMove: (evt, gestureState) => {
        console.log('2',evt,gestureState)
      },
      onResponderTerminationRequest: (evt, gestureState) => true,
      onResponderRelease: (evt, gestureState) => {
        console.log('3',evt,gestureState)
        
      },
      onResponderTerminate: (evt, gestureState) => {
        console.log('4',evt,gestureState)
        
      },
      
      onResponderSingleTapConfirmed: (evt, gestureState) => {
        console.log('5',evt,gestureState)
        
      },
      
      moveThreshold: 2,
      debug: true
    });
  }
  searchLocation(text){
    if(text == ""){AddStoryStore.searchedLocation = []}
    if(text.length>2){
      
        fetch("https://fenicoapp.com/api/v1/cities/searchh?q=" + text, {
          method: "GET",
          headers: {
            'Authorization': 'Bearer ' + KeepToken.token
          }
        })
        .then((response) => response.json())
        .then((responseData) => {
           //console.log('search data',responseData)
           AddStoryStore.searchedLocation = responseData
        }).catch((error) => {
           //console.log(error);
        });
    }
  }
  backAddStory(ID,name){
    AddStoryStore.selectedLocationID = ID
    AddStoryStore.selectedLocationName = name
    this.props.navigation.goBack(null)
  }
  renderSearchedLocation(rowData){
    return(
      <TouchableOpacity onPress={() => this.backAddStory(rowData.id,rowData.merged)} style={{height:40,width:windowSize.width-30,justifyContent:'center',borderBottomWidth:0.5,borderColor:'#E0E0E0'}}>
        <Text style={{color:'gray'}}>{rowData.merged}</Text>
      </TouchableOpacity>
    )
  }
  render() {
    return (
      <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.headerLeft}>
          <Image style={styles.headerLeftImage} source={Images.leftArrow} />
        </TouchableOpacity>
        <View style={styles.headerMiddle}>
          <Text style={styles.title}>Search Location</Text>
        </View>
        <View style={styles.headerRight}>
        </View>
      </View>
        <View style={styles.searchBarPlace}>
          <TextInput ref={(r) => { this._textInput = r; }} autoFocus={true} onChangeText={(text) => this.searchLocation(text) } placeholder={"Search a Location"} style={styles.textinput} />
        </View>
        <View style={styles.resultsPlace}>
          <ListView
            keyboardShouldPersistTaps={false}
            keyboardDismissMode={'on-drag'}
            style={{flex:1,paddingLeft:20}}
            dataSource={AddStoryStore.dataSource}
            renderRow={this.renderSearchedLocation.bind(this)}
            enableEmptySections={true}
            initialListSize={1}
          />
        </View>

      </View>
    );
  }
}
/*

          <KeyboardSpacer />

*/
const styles = StyleSheet.create({
  container: {flex: 1,backgroundColor: '#fff'},
  header:{height:70,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  searchBarPlace:{flex:1,justifyContent:'center',alignItems:'center'},
  resultsPlace:{flex:6},
  textinput:{height:50,width:windowSize.width-40,borderWidth:1,borderColor:'#E0E0E0',marginLeft:20,paddingHorizontal:10},
  headerRight:{flex:1,marginTop:25,justifyContent:'center',alignItems:'center'},
  headerMiddle:{flex:4,marginTop:25,justifyContent:'center',alignItems:'center'},
  headerLeft:{flex:1,marginTop:25,justifyContent:'center',alignItems:'center'},
  title:{color:'white',fontWeight:'bold',fontSize:18},
  leftArrow:{height:40,width:40}

});
