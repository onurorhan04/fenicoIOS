import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
  ListView,
  ScrollView,
  TextInput,
  FlatList,
  KeyboardAvoidingView
} from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob'
const windowSize = Dimensions.get('window');
import Modal from 'react-native-animated-modal';
import Images from '../../images/images';
import KeepToken from '../../stores/keeptoken';
import UserData from '../../stores/userData';
import { observer } from 'mobx-react/native';
import Footer from '../../components/footer';
import Toast from '../../components/toast';
import HTML from 'react-native-render-html';
//667

//480
const SCALE_PROFILIMAGE = 0.2
const SCALE_COUNTRY = 0.075
const SCALE_BUTTON = 0.075
const SCALE_BUTTON_IMAGE = 0.060
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

@observer
export default class ProfilMain extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2', 'row 2', 'row 2', 'row 2', 'row 2', 'row 2']),
      dataSourceFellows: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      isLoaded: false,
      visibleModal: false,
      isOpenedFellowsModal: false,
      isOpenedWishListModal: false,
      wishListText: "",
      visibleToast: false,
      toastMessage: "",
      noStoryMoreData: false,
      followerLength:null,
      followingLength:null,
      fontSize:14

    };
  }
  showToastMessage(text,timeout){
    this.setState({visibleToast:true,toastMessage:text.toString()})
    setTimeout(() => this.setState({visibleToast:false,toastMessage:""}), timeout);
  }
  createWishList(){
    const { wishListText } = this.state
    if(wishListText !== ""){
      var formData = new FormData()
      formData.append('plan', wishListText)
      fetch('https://fenicoapp.com/api/v1/user-wishlist/create', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer ' + KeepToken.token
        },
        body:formData
      })
      .then((response) => response.json())
      .then((responseData) => {
        if(responseData.status == "success"){
          UserData.wishListData.push({'plan':wishListText.toString()})
          this.setState({isOpenedWishListModal:false})
          this.showToastMessage("WishList created succesfully..",2000)
        }
      })
    }else {
      Alert.alert('Error','Fill in empty spaces!')
      //this should be toast
    }

  }
  updateLocation(newLocation){
    var formDataForURLENCODED = [];
      formDataForURLENCODED.push(encodeURIComponent('location') + "=" + encodeURIComponent(newLocation));
      const body=formDataForURLENCODED.join("&");
      fetch('https://fenicoapp.com/api/v1/user-profile/update?id='+KeepToken.userID, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
          'Authorization': 'Bearer ' + KeepToken.token
        },
        body
      }).then((resp) => {
        //Alert.alert(resp.status.toString(),JSON.stringify(resp))
        if(resp.status == "success" || resp.status == 200){
          //ISLOADING OLAYINI YAP VE STOREDAKİ LOCATIONI DOLDUR
          UserData.location = newLocation
        }
      }).catch((err) => {
        //Alert.alert('yada burada',JSON.stringify(err))
      })
  }
  getGoogleMapsData(lat,lon){
    fetch("https://maps.googleapis.com/maps/api/geocode/json?latlng=" +lat+ "," +lon+ "&sensor=true&language=en", {
      method: "GET",
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.updateLocation(responseData.results[2].formatted_address)
    })
  }
  geoLocation(){
    UserData.location = 'updating'
    navigator.geolocation.getCurrentPosition(
      (position) => {
        //Alert.alert(position.coords.latitude.toString(),position.coords.longitude.toString())
        this.getGoogleMapsData(position.coords.latitude,position.coords.longitude)
        //Alert.alert(position.coords.latitude.toString(),position.coords.longitude.toString())
      },
      (error) =>{
        //console.log(error)
        //Alert.alert('',error.message.toString())
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }
  openWishListModal(){
    if(UserData.wishListData.length > 0 ){
      this.setState({visibleModal: 'wishListModal'})
    }
  }
  openFellowsModal(){
    this.setState({isOpenedFellowsModal:true})
  }
  getFellows(){
    fetch("https://fenicoapp.com/api/v1/user-follower/index?id=" + KeepToken.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
       this.setState({dataSourceFellows:this.state.dataSourceFellows.cloneWithRows(responseData.items)})
    })
  }
  
  setFontSize(){
    if(windowSize.height < 500){ this.setState({ fontSize: 12}) }
  }
  componentWillMount(){
    this.setFontSize()
    //Alert.alert('',windowSize.height.toString())
    this.getFellows()
    this.limitStoryData()
    //UserData.wishListData[UserData.wishListData.length - 1].plan
  }
  getFollowerLength(){
    fetch("https://fenicoapp.com/api/v1/user-follower/follower?id="+ KeepToken.userID, {
          method: "GET",
          headers: {
          'Authorization': 'Bearer ' + KeepToken.token
          }
      })
      .then((response) => response.json())
      .then((responseData) => {
          this.setState({followerLength: responseData.items.length})
      })
  }
  getFollowingLength(){
    fetch("https://fenicoapp.com/api/v1/user-follower/index?id=" + KeepToken.userID, {
          method: "GET",
          headers: {
              'Authorization': 'Bearer ' + KeepToken.token
          }
      })
      .then((response) => response.json())
      .then((responseData) => {
          this.setState({followingLength: responseData.items.length})
      })
  }
  cleanWishList(){
    var wishListPlan = UserData.wishListData[UserData.wishListData.length - 1].plan
    var splicedPlan = wishListPlan.slice(0,30)
    var inTag = false
    var cleanedPlan = ""
    for( let i = 0; i < splicedPlan.length; i++ ){
      if(!inTag){
        if(splicedPlan.substr(i,1) == "<" && !inTag){
          inTag = true
        }else{
          cleanedPlan += splicedPlan.substr(i,1)
          inTag = false
        }

      }else{
        if(splicedPlan.substr(i,1) == ">"){
          inTag = false
        }
      }
    }
    return cleanedPlan
  }
  componentDidMount(){
    this.getFollowerLength()
    this.getFollowingLength()
  }
  limitStoryData(){
    if(UserData.storyData.length == 0 ){ this.setState({ noStoryMoreData: true }) }
    //eger lenght 5 ten fazla ise  önceden yapılmıs bi daha yapma ..
    if(UserData.limitedStoryData.length > 5){
      if(UserData.limitedStoryData.length == UserData.storyData.length){
        this.setState({noStoryMoreData:true})
      }
    }else{
      UserData.limitedStoryData = UserData.storyData.slice(0,5)
    }
  }
  loadExtraStoryData(){
    UserData.shownStoryDataLength = UserData.limitedStoryData.length + 5
    UserData.limitedStoryData = UserData.storyData.slice(0,UserData.limitedStoryData.length + 5)
    if(UserData.limitedStoryData.length == UserData.storyData.length){
      this.setState({noStoryMoreData:true})
    }
  }
  renderCountries(rowData){
    return(
      <TouchableOpacity onPress={() => this.routingToShowCountryStories(KeepToken.userID,rowData.id,rowData.name)} style={{height:windowSize.height * SCALE_COUNTRY,width:windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) / 2,margin:10}}>
       <Image source={{uri: rowData.flag + "/128/" + rowData.name + ".png"}} style={{height:windowSize.height * SCALE_COUNTRY ,width: windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) / 2,resizeMode:'stretch'}} />
      </TouchableOpacity>
    )
  }
  routingToShowCountryStories(userID,countryID,countryName){
    this.props.navigation.navigate('showCountryStories',{userID:userID,countryID:countryID,countryName:countryName})
  }
  routingToShowStory(storyID){
      this.props.navigation.navigate('showStory',{storyID:storyID})
  }
  routing(routeName){
    this.props.navigation.navigate(routeName)
  }
  goToProfileWithModal(followerID,followerName,followerImage){
    this.setState({isOpenedFellowsModal:false})
    this.props.navigation.navigate('profilOther',{userID:followerID,username:followerName});
  }
  routeToFollow(filtre){
    this.props.navigation.navigate('followInfo',{userID:KeepToken.userID,filtre:filtre})
  }
  renderFellows(rowData){
    var imageView = ""
    if(rowData.image == null || rowData.image == "" || rowData.image.length < 40){
       imageView = <Image style={{height:40,width:40,borderRadius:20}} source={Images.noImage} />
    }else{
       imageView = <Image style={{height:40,width:40,borderRadius:20}} source={{uri: rowData.image}} />
    }
    return(
      <TouchableOpacity onPress={() => this.goToProfileWithModal(rowData.follower,rowData.name,rowData.image)} style={{flexDirection:'row',height:50,justifyContent:'center',alignItems:'center'}}>
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          {imageView}
        </View>
        <View style={{flex:5,justifyContent:'center'}}>
          <Text style={{marginBottom:5}}>{rowData.name} {rowData.userID} </Text>
          
        </View>
      </TouchableOpacity>
    )
  }
  render() {
    
    return (
      <View style={styles.container}>
        <Modal isVisible={this.state.visibleModal === 'wishListModal'}
             backdropColor = '#000000'
             backdropOpacity = {0.85}
             animationIn={'bounceIn'}
             animationOut={'bounceOut'}
             style={{justifyContent:'center',alignItems:'center'}}
             >
         <View style={{height:300,width:windowSize.width-60,backgroundColor:'white',borderRadius:10}}>
           <View style={{height:290,width:windowSize.width-60,borderRadius:10}}>
            <View style={{flex:2,borderColor:'#E0E0E0',borderTopLeftRadius:10,borderTopRightRadius:10,backgroundColor:'#f0a574',flexDirection:'row',justifyContent:'center',alignItems:'center',borderBottomWidth:1.5}}>
              <View style={{flex:1}}></View>
              <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#FFF',fontWeight:'bold',fontSize:17}}>Wish</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({visibleModal:false})} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image style={{height:17,width:17}} source={Images.cancel} />
              </TouchableOpacity>

            </View>
            <View style={{flex:9,flexDirection:'row',borderBottomLeftRadius:10,borderBottomRightRadius:10,backgroundColor:'#FFF',justifyContent:'center',alignItems:'center'}}>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <ScrollView style={{padding:10}}>
                  {UserData.wishListData.length > 0 ? 
                  <HTML html={UserData.wishListData[UserData.wishListData.length - 1].plan} />
                  :
                  null 
                  }
                </ScrollView>
              </View>
            </View>
            
          </View>
           
         </View>
       </Modal>


      <Modal isVisible={this.state.visibleModal === 'locationModal'}
             backdropColor = '#000000'
             backdropOpacity = {0.85}
             animationIn={'bounceIn'}
             animationOut={'bounceOut'}
             style={{justifyContent:'center',alignItems:'center'}}
             >
         <View style={{height:150,width:260,backgroundColor:'white',borderRadius:10}}>
           <View style={{height:150,width:260,borderRadius:10}}>
            <View style={{flex:2.2,borderColor:'#E0E0E0',borderTopLeftRadius:10,borderTopRightRadius:10,backgroundColor:'#f0a574',flexDirection:'row',justifyContent:'center',alignItems:'center',borderBottomWidth:1.5}}>
              <View style={{flex:1}}></View>
              <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#FFF',fontWeight:'bold',fontSize:17}}>Current Location</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({visibleModal:false})} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image style={{height:17,width:17}} source={Images.cancel} />
              </TouchableOpacity>

            </View>
            <View style={{flex:5,flexDirection:'row',borderBottomLeftRadius:10,borderBottomRightRadius:10,backgroundColor:'#FFF',justifyContent:'center',alignItems:'center'}}>
              {UserData.location === "updating" ?
              <Text style={{fontWeight:'bold',color:'gray',textAlign:'center'}}>Updating...</Text>
             :
              <Text style={{fontWeight:'bold',color:'gray',textAlign:'center'}}>{UserData.location}</Text>
            }
            {UserData.location === null ? 
              <View>
                <Text style={{fontWeight:'bold',color:'gray',textAlign:'center'}}>Click the button to specify the location</Text>
              </View>
            : null }
            </View>
            <View style={{flex:2,flexDirection:'row',borderRadius:10,backgroundColor:'#FFF'}}>
             <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center'}} onPress={() => this.geoLocation()} >
               <Text style={{color:'#2292ff',fontWeight:'bold'}}>Update
               </Text>
             </TouchableOpacity>
             <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center',borderLeftWidth:1.5,borderColor:'#E0E0E0'}} onPress={() => this.setState({ visibleModal: null})} >
               <Text style={{color:'#2292ff',fontWeight:'bold'}}>Close
               </Text>
             </TouchableOpacity>
           </View>
          </View>
           
         </View>
       </Modal>
       <Modal isVisible={this.state.isOpenedFellowsModal}>
          <View style={styles.fellowsModalContainer}>
            <View style={styles.fellowsModalModalTopFlex}>
              <View style={{flex:1}}>
              </View>
              <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                <Text style={styles.fellowsModalTitleText}>Fellows</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({isOpenedFellowsModal:false})} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image style={{height:13,width:13}} source={Images.exit} />
              </TouchableOpacity>
            </View>
            <View style={styles.fellowsModalFellows}>
              <ListView
                style={{marginTop:10}}
                enableEmptySections={true}
                dataSource={this.state.dataSourceFellows}
                renderRow={this.renderFellows.bind(this)}
              />
              <View style={{height:10}} ></View>
            </View>
          </View>
        </Modal>
        <Modal isVisible={this.state.isOpenedWishListModal}>
        <KeyboardAvoidingView keyboardVerticalOffset={20} behavior={"position"}>
          <View style={styles.wishListModalContainer}>
            <View style={styles.wishListModalModalTopFlex}>
              <View style={{flex:1}}>
              </View>
              <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                <Text style={styles.wishListModalTitleText}>Create a Wish</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({isOpenedWishListModal:false})} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image style={{height:13,width:13}} source={Images.exit} />
              </TouchableOpacity>
            </View>
            <View style={styles.wishListModalFellows}>
              <TextInput style={{flex:2,fontSize:20,paddingHorizontal:20,paddingVertical:10,margin:20,borderWidth:0.5,borderColor:'#d0d0d0'}} multiline={true} placeholderTextColor={"#A0A0A0"} onChangeText={(text) => this.setState({wishListText:text})} placeholder={"write a something about your wishes"} />
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={() => this.createWishList()} style={styles.wishListModalCreateButtonPlace}>
                  <Text style={styles.wishListModalCreateButtonText}>Create</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
        </Modal>
        <View style={{flex:3,flexDirection:'row'}}>
            <Image style={{position:'absolute',left:0,top:0,height:400,width:windowSize.width,resizeMode:'stretch'}} source={Images.backgroundOfProfilImage} />
            <View style={{flex:1}}>
              
              <View style={{flex:1,justifyContent:'center',alignItems:'center',opacity:0.5}}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.backButton}>
                  <Image style={styles.backButtonImage} source={Images.leftArrow} />
                </TouchableOpacity>
                
              </View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={() => this.setState({visibleModal:'locationModal'})} style={styles.profilButtons}>
                  <Image style={styles.circleLocationButton} source={Images.location} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
                <View style={{height:windowSize.height * SCALE_PROFILIMAGE ,width:windowSize.height * SCALE_PROFILIMAGE,borderRadius:(windowSize.height * SCALE_PROFILIMAGE ) / 2 ,backgroundColor:'#fff',justifyContent:'center',alignItems:'center'}}>
                  {UserData.data.profile.image !== null ? 
                  <Image style={styles.profilImagePlaceImage} source={{uri: 'https://fenicoapp.com/api/web/uploads/'+UserData.data.profile.image}} />
                  :
                  <Image style={styles.profilImagePlaceImage} source={Images.noImage}/> 
                  }

                </View>
                <Text style={styles.nameText}>{UserData.data.profile.name}</Text>
                <Text style={styles.usernameText}>@{UserData.data.username}</Text>
                <View style={{flexDirection:'row',backgroundColor:'transparent',marginTop:10}}>
                  <TouchableOpacity onPress={() => this.routeToFollow('Followers')}>
                    
                    {this.state.followerLength ? 
                      <Text style={{color:'#FFF',fontSize: windowSize.height < 500 ? 11 : 14}}>Followers {this.state.followerLength}</Text>
                    :
                      <Text style={{color:'#FFF',fontSize: windowSize.height < 500 ? 11 : 14 }}>Followers 0</Text>

                    }
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.routeToFollow('Following')}>
                    {this.state.followingLength ? 
                      <Text style={{color:'#FFF',marginLeft:15,fontSize: windowSize.height < 500 ? 11 : 14}}>Following {this.state.followingLength}</Text>
                    :
                      <Text style={{color:'#FFF',marginLeft:15,fontSize: windowSize.height < 500 ? 11 : 14 }}>Following 0</Text>

                    }
                  </TouchableOpacity>
                </View>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
             
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={() => this.routing('notificationMain')} style={styles.profilButtons}>
                  <Image style={styles.circleNotificationButton} source={Images.notification} />
                </TouchableOpacity>
              </View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={() => this.routing('profileSettings')} style={styles.profilButtons}>
                  <Image style={styles.circleSettingButton} source={Images.setting} />
                </TouchableOpacity>
              </View>
            </View>
        </View>
        <View style={{flex:1.2,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
        <Text style={{marginTop:5,fontSize:this.state.fontSize}}>I have been</Text>
        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
          <TouchableOpacity onPress={() => this.routing('profileAddStory')} style={{height:windowSize.height * SCALE_COUNTRY,width:windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) /2,margin:10,backgroundColor:'#E0E0E0'}}>
            <Image source={Images.plusOrange}  style={{position:'absolute',right:0,bottom:0,height:windowSize.height * SCALE_COUNTRY /2.5 ,width:windowSize.height * SCALE_COUNTRY /2.5}} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.routingToShowCountryStories(KeepToken.userID,'','')} style={{height:windowSize.height * SCALE_COUNTRY,width:windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) /2,margin:10,backgroundColor:'transparent'}}>
            <Image source={Images.earth}  style={{height:windowSize.height * SCALE_COUNTRY,width:windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) / 2}} />
          </TouchableOpacity>
          <ListView
            dataSource={UserData.dataSourceCountries}
            contentContainerStyle={{flexDirection:'row'}}
            renderRow={this.renderCountries.bind(this)}
          />
        </ScrollView>
        </View>
        <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#FFF',shadowColor: '#34495e',shadowOffset: {width: 2,height: 3},shadowRadius: 5,shadowOpacity: 1}}>
         <View style={{flex:1,width:windowSize.width,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
           <Text style={{marginTop:5,fontSize:this.state.fontSize}}>Wish List</Text>
           <TouchableOpacity onPress={() => this.props.navigation.navigate('wishLists',{userID:KeepToken.userID}) } style={{position:'absolute',right:5,top:5}}>
              <Image source={Images.grayList}  style={{height:22,width:22}} />
           </TouchableOpacity>
         </View>
         <View style={{flex:3,width:windowSize.width}}>
           <TouchableOpacity onPress={() => this.openWishListModal() } style={{margin:10,justifyContent:'center',alignItems:'center'}}>
             
            
               
             {UserData.wishListData.length > 0 ?

              <Text style={{fontSize:this.state.fontSize}}>{this.cleanWishList()}...</Text>
               :
               <Text style={{fontSize:this.state.fontSize}}>No wishlist</Text>
              }
             
             
           </TouchableOpacity>
         </View>
        </View>


        <View style={{flex:2,backgroundColor:'#fff',shadowColor: '#34495e',shadowOffset: {width: 2,height: 3},shadowRadius: 5,shadowOpacity: 1}}>
          <View style={{flex:1,justifyContent:'center',alignItems:'center',borderBottomWidth:1,borderColor:'#a0a0a0'}}>
            <Text style={{marginTop:5,fontSize:this.state.fontSize}}>Stories</Text>
          </View>
          <View style={{flex:3}}>
            <FlatList
              data={UserData.limitedStoryData}
              renderItem={this.renderStoryItem.bind(this)}
              ListFooterComponent={this.renderStoryListFooter.bind(this)}
            />  
          </View>
              
        </View>
        
        <Toast
            visible={this.state.visibleToast}
            position={windowSize.height-150}
            shadow={true}
            animation={true}
            hideOnPress={true}
          >{this.state.toastMessage}</Toast>
        <View style={styles.blankSpace}></View>
        <Footer />
      </View>
    );
  }
  renderStoryListFooter(){
    if(this.state.noStoryMoreData){
      return(
        <View style={{height:40,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontWeight:'700',color:'#a0a0a0'}}>No More Stories</Text>
        </View>
      )
    }else{
      return(
        <TouchableOpacity onPress={() => this.loadExtraStoryData()} style={{height:40,justifyContent:'center',alignItems:'center'}}>
          <View style={{height:26,width:120,borderRadius:13,backgroundColor:'#e0e0e0',justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontWeight:'700',color:'#a0a0a0'}}>Load More</Text>
          </View>
        </TouchableOpacity>
      )
    }
  }
  renderStoryItem(data){
    return (
        <TouchableOpacity onPress={() => this.routingToShowStory(data.item.id)} style={{flex:1,height:45,flexDirection:'row',borderBottomWidth:1,borderColor:'#a0a0a0'}}>
          <View style={{flex:1}}>
              <Image source={{uri: data.item.userStoryImages[0].img }} style={{height:null,width:null,flex:1}} />
          </View>
          <View style={{flex:2.5}}>
            <View style={{margin:5,flex:1}}>
              <HTML html={data.item.title} />
            </View>
          </View>
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {flex: 1,backgroundColor: '#F5FCFF',},
  blankSpace:{height:50},
  profilButtons:{backgroundColor:'#ecf0f1',height:windowSize.height * SCALE_BUTTON,width:windowSize.height * SCALE_BUTTON,borderRadius:(windowSize.height * SCALE_BUTTON) / 2,shadowColor: '#34495e',shadowOffset: {width: 1,height: 1.5},shadowRadius: 1,shadowOpacity: 0.5,justifyContent:'center',alignItems:'center'},
  profilButtonsLeftTop:{position:'absolute',left:30,top:40,},
  profilButtonsLeftBottom:{position:'absolute',left:30,bottom:40,},
  profilButtonsRightTop:{position:'absolute',right:30,top:40,},
  profilButtonsRightBottom:{position:'absolute',right:30,bottom:40,},
  profilImagePlaceView:{position:'absolute',left:(windowSize.width-150)/2,bottom:70,backgroundColor:'#fff',height:150,width:150,borderRadius:75,justifyContent:'center',alignItems:'center'},
  profilImagePlaceImage:{height:(windowSize.height * SCALE_PROFILIMAGE ) - 4,width:(windowSize.height * SCALE_PROFILIMAGE ) - 4,borderRadius:((windowSize.height * SCALE_PROFILIMAGE ) - 4)/2,resizeMode:'cover'},
  namePlace:{fontSize: windowSize.height < 500 ? 11 : 13,position:'absolute',bottom:10,left:(windowSize.width-200)/2,height:70,width:200,justifyContent:'center',alignItems:'center'},
  nameText:{textAlign:'center',fontSize:windowSize.height < 500 ? 11 : 13,backgroundColor:'transparent',color:'#fff'},
  usernameText:{textAlign:'center',fontSize:windowSize.height < 500 ? 11 : 12,backgroundColor:'transparent',color:'#fff'},
  circleNotificationButton:{height:windowSize.height * SCALE_BUTTON_IMAGE,width:windowSize.height * SCALE_BUTTON_IMAGE},
  circleMessageButton:{height:windowSize.height * SCALE_BUTTON_IMAGE,width:windowSize.height * SCALE_BUTTON_IMAGE},
  circleLocationButton:{height:windowSize.height * SCALE_BUTTON_IMAGE,width:windowSize.height * SCALE_BUTTON_IMAGE},
  circleSettingButton:{height:windowSize.height * SCALE_BUTTON_IMAGE,width:windowSize.height * SCALE_BUTTON_IMAGE},
  fellowsView:{position:'absolute',justifyContent:'center',alignItems:'center',right:25,bottom:75,height:50,width:50,borderRadius:25,backgroundColor:'#27ae60'},
  fellowsModalContainer:{borderRadius:5,height:300,width:windowSize.width-40,backgroundColor:'white',justifyContent:'center',alignItems:'center',flexDirection:'column'},
  fellowsModalModalTopFlex:{flexDirection:'row',flex:1,width:windowSize.width-40,backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center'},
  fellowsModalTitleText:{color:'#fff',fontWeight:'bold',fontSize:18},
  fellowsModalFellows:{flex:5,width:windowSize.width-40},
  wishListView:{position:'absolute',justifyContent:'center',alignItems:'center',right:25,bottom:75,height:50,width:50,borderRadius:25,backgroundColor:'#27ae60'},
  wishListModalContainer:{borderRadius:5,height:300,width:windowSize.width-40,backgroundColor:'white',justifyContent:'center',alignItems:'center',flexDirection:'column'},
  wishListModalModalTopFlex:{flexDirection:'row',flex:1,width:windowSize.width-40,backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center'},
  wishListModalTitleText:{color:'#fff',fontWeight:'bold',fontSize:18},
  wishListModalFellows:{flex:5,width:windowSize.width-40,justifyContent:'center',alignItems:'center'},
  wishListModalCreateButtonPlace:{height:50,width:130,backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center'},
  wishListModalCreateButtonText:{color:'#FFF',fontWeight:'bold'},
  backButton:{height:40,width:40,borderRadius:20,backgroundColor:'#34495e',opacity:0.8,justifyContent:'center',alignItems:'center'},
  backButtonImage:{opacity:1}
});
