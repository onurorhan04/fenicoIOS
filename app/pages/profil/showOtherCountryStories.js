import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,View,TouchableOpacity,Image,ScrollView,TextInput,Dimensions,ListView,Alert} from 'react-native';
var windowSize = Dimensions.get('window');
import Swiper from 'react-native-swiper';
import { observer } from 'mobx-react/native';
import Images from '../../images/images'
import Footer from '../../components/footer'
import OtherShowCountryStore from '../../stores/otherShowCountryStore';
import UserData from '../../stores/userData';
import KeepToken from '../../stores/keeptoken';
import Colours from '../../const/colours';
import Toast from '../../components/toast';
import Firebase  from 'firebase';
import Modal from 'react-native-modal'
import HTML from 'react-native-render-html';
import moment from 'moment'


@observer
export default class ShowOtherCountryStories extends Component {
  constructor(props){
    super(props)
    this.state={
      isLoaded: false,
      userID: this.props.navigation.state.params.userID,
      countryID: this.props.navigation.state.params.countryID,
      countryName: this.props.navigation.state.params.countryName,
      visibleToast: false,
      toastMessage: "",
      reportingModal: false,
      reportingText: "",
      reportingData: []
    }
  }
  getStoryData(){
    var fetch_url = ""
    if(this.state.countryID !== ""){
      fetch_url = "https://fenicoapp.com/api/v1/user-story/index?userid="+this.state.userID+"expand=userStoryImages&country_id=" + this.state.countryID
    }else{
      fetch_url = "https://fenicoapp.com/api/v1/user-story/index?userid="+this.state.userID+"expand=userStoryImages"
    }
    fetch(fetch_url, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      //console.log(responseData)
      OtherShowCountryStore.stories = responseData.items
      OtherShowCountryStore.stories.forEach(function(story) {
        if(story.userStoryLikes.length == 0){
          OtherShowCountryStore.liked.push({'story_id':story.id,'liked':false,'likeCount':0})
        }
        story.userStoryLikes.forEach(function(like) {
          OtherShowCountryStore.liked.push({'story_id':story.id,'liked':false,'likeCount':story.like_count})
        });
        story.userStoryLikes.forEach(function(like) {
          if(like.olusturan == KeepToken.userID){
            var index = OtherShowCountryStore.liked.findIndex(item =>item.story_id === story.id)
            OtherShowCountryStore.liked[index].liked = true
          }
        });
      });
      this.setState({isLoaded:true})
    }).catch((error) => {
        //Alert.alert('',JSON.stringify(error));
    });
  }
  componentWillMount(){
    this.getStoryData()
    //Alert.alert(this.state.countryID.toString())
  }
  async clickedHeart(storyID){
     var index = OtherShowCountryStore.liked.findIndex(item =>item.story_id === storyID)
     if(OtherShowCountryStore.liked[index].liked){
       OtherShowCountryStore.liked[index].likeCount--
     }else{
       OtherShowCountryStore.liked[index].likeCount++
     }
     OtherShowCountryStore.liked[index].liked = !OtherShowCountryStore.liked[index].liked
     OtherShowCountryStore.stories.push({'like':'like'})
    if(!OtherShowCountryStore.liked[index].liked){
      await fetch("https://fenicoapp.com/api/v1/user-story-like/delete?story_id=" + storyID, {
        method: "DELETE",
        headers: {
          'Authorization': 'Bearer ' + KeepToken.token
        }
      })
      .then((response) => response.json())
      .then((responseData) => {

      }).catch((error) => {
          //Alert.alert('Error',JSON.stringify(error))
      });
    }else{

      var formData = new FormData();
      formData.append('story',storyID);
      await fetch('https://fenicoapp.com/api/v1/user-story-like', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer ' + KeepToken.token
        },
        body: formData
      }).then((resp) => {
        if(resp.status == "success" || resp.status == 200){
        }
      }).catch((error) => {
        //Alert.alert('Error',JSON.stringify(error))
      })
    }
  }
  showToastMessage(text,timeout){
    this.setState({visibleToast:true,toastMessage:text.toString()})
    setTimeout(() => this.setState({visibleToast:false,toastMessage:""}), timeout);
  }
  addToWishlist( storyID, wishlistText, image ){
    console.log(storyID, wishlistText, image)
    var formData = new FormData()
    formData.append('story_id', storyID)
    formData.append('plan', wishlistText)
    formData.append('image_link', image)
    
    fetch('https://fenicoapp.com/api/v1/user-wishlist/create', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + KeepToken.token
      },
      body:formData
    })
    .then((response) => response.json())
    .then((responseData) => {
      if(responseData.status == "success"){
        UserData.wishListData.push({'plan':wishlistText.toString()})
        this.showToastMessage("WishList created succesfully..",2000)
      }
    })
  }
  reporting(){
    const { reportingData } = this.state
    const { id, title, story, city } = reportingData
    const userID = reportingData.user.id
    const username = reportingData.user.name
    const reportRef = Firebase.database().ref().child("Reports").child('Story').child(id);
    reportRef.push({id: id, username: username, title: title, story: story, city: city, reportingText: this.state.reportingText})
    this.setState({reportingModal: false, reportingText: ''})
    this.showToastMessage('Story reported.',2000)
  }
  setDataForReporting(data){
    this.setState({reportingData: data, reportingModal: true})
  }
  renderStories(rowData){
    if(rowData.like == 'like'){
      return(null)
    }else{
      var index = OtherShowCountryStore.liked.findIndex(item =>item.story_id === rowData.id)
      return(
      <View style={styles.borderContent}>
        <View style={{height:60,flexDirection:'row'}}>
          <TouchableOpacity style={{flex:1,marginLeft:10,justifyContent:'center',alignItems:'center'}}>
           {rowData.user.image.length < 40 ?
             <Image style={{height:46,width:46,borderRadius:23}} source={Images.noImage} />
             :
             <Image style={{height:46,width:46,borderRadius:23}} source={{uri:rowData.user.image}} />
           }
          </TouchableOpacity>
          <View style={{flex:4,flexDirection:'column',justifyContent:'center'}}>
            <Text style={{color:'#808080',marginLeft:10,fontSize:15,fontWeight:'bold'}}>{rowData.user.name}</Text>
            <Text style={{color:'#A0A0A0',marginLeft:10,fontSize:12}}>{rowData.cityDetails} {moment(rowData.olusturuldu).format("MMM Do YY") == "Invalid date" ? "" : moment(rowData.olusturuldu).format("MMM Do YY") } </Text>
          </View>
          <TouchableOpacity onPress={() => this.setDataForReporting(rowData) } style={{flex:1,margin:7,justifyContent:'center',alignItems:'flex-end'}}>
            <Image style={{height:30,width:30}} source={Images.extraGray} />
          </TouchableOpacity>
        </View>
        <View style={{height:300,width:windowSize.width-20,backgroundColor:'white'}}>
          <Swiper width={windowSize.width-20} horizontal={true} removeClippedSubviews={false} showsPagination={false}>
          {rowData.image.map((image) => {
            return (
              <View style={{width:windowSize.width-20,justifyContent:'center',alignItems:'center'}} >
                <Image style={{height:300,width:windowSize.width-50,resizeMode:'contain'}} source={{uri:image}} />
              </View>
            )
          })}
          </Swiper>
        </View>
        <View style={{marginLeft:15,marginRight:15,marginBottom:15}}>
          <HTML html={rowData.title} />
          <HTML html={rowData.story} />
          
        </View>
        <View style={{flex:1,height:50,width:null,borderTopWidth:1,borderColor:'#A0A0A0',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
          <TouchableOpacity onPress={() => this.addToWishlist(rowData.id, rowData.title, rowData.image[0])} style={{marginLeft:10}}>
            <Image style={{height:25,width:25}} source={Images.addToWishList}/>
          </TouchableOpacity>
          <View style={{flex:1}}></View>
          <TouchableOpacity onPress={() => this.clickedHeart(rowData.id)} style={{flex:1,flexDirection:'row',justifyContent:'flex-end',alignItems:'center'}}>
            <Text style={{color:'#ffa500'}}>{OtherShowCountryStore.liked[index].likeCount}</Text>
            <Image style={{marginRight:5,height:28,width:28}} source={OtherShowCountryStore.liked[index].liked == false ? Images.heartbordered : Images.heartfilled} />
          </TouchableOpacity>
        </View>
      </View>
      )
    }
  }
  render(){

    if(this.state.isLoaded){
    return (
      <View style={styles.container}>
      <Modal style={{justifyContent:'center',alignItems:'center'}} isVisible={this.state.reportingModal}>
          <View style={{height:220,width:windowSize.width-50,backgroundColor:'#fff',borderRadius:6}}>
            <View style={{flex:1.2,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
              <View style={{flex:1}}></View>
              <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#e74c3c',fontSize:16}} >Reporting</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({reportingModal: false}) } style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Image style={{height:13,width:13}} source={Images.grayCancel} />
              </TouchableOpacity>
              
            </View>
            <View style={{flex:3}}>
              <TextInput multiline={true} onChangeText={(text) => this.setState({reportingText:text})} placeholder={"Please, describe the problem"} placeholderTextColor={'#A0A0A0'}  style={{marginLeft:10,borderWidth:1,borderColor:'#d0d0d0',height:95,width:windowSize.width-70,fontSize:15}} />
            </View>
            <TouchableOpacity onPress={() => this.reporting()} style={{flex:1.2,flexDirection:'row',backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center',borderBottomLeftRadius:6,borderBottomRightRadius:6}}>
                <View style={{height:35,justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:'#fff',fontSize:18}}>Report</Text>
                </View>
            </TouchableOpacity>
          </View>
      </Modal>
      <View style={styles.header}>
        <View style={styles.headerBlank}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.headerLeft}>
           <Image style={styles.headerLeftImage} source={Images.leftArrow} />
          </TouchableOpacity>
          <View style={styles.headerMiddle}>
            <Text style={styles.headerMiddleText}>Stories {this.state.countryID ? "(" + this.state.countryName + ")" : null }</Text>
          </View>
          <View onPress={() => this.save()} style={styles.headerRight}>
            <Text style={styles.headerRightText}></Text>
          </View>
        </View>
      </View>
        <ScrollView style={{margin:10,backgroundColor:'transparent'}}>
          <ListView
            style={styles.listview}
            dataSource={OtherShowCountryStore.dataSourceStories}
            renderRow={this.renderStories.bind(this)}
          />

          <View style={styles.blankSpace}></View>

        </ScrollView>
        <Toast
            visible={this.state.visibleToast}
            position={windowSize.height-150}
            shadow={true}
            animation={true}
            hideOnPress={true}
          >{this.state.toastMessage}</Toast>
        <Footer />
      </View>
    );
  }else{
    return null
  }
  }
}
const styles = StyleSheet.create({
  container: {flex: 1,backgroundColor: '#e9edf1',},
  content:{flex:1,borderWidth:1,borderColor:'black'},
  blankSpace:{height:50},
  borderContent:{flex:1,marginTop:20,elevation: 1,borderWidth: 1,borderColor: '#ddd',borderBottomWidth: 0,backgroundColor:'white'},
  listview:{backgroundColor:'#e9edf1',},
  headerBlank:{marginTop:windowSize.height < 500 ? 0 : 20 ,height:50,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  header:{height:windowSize.height < 500 ? 50 : 70 ,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerLeft:{flex:1,justifyContent:'center',alignItems:'center'},
  headerMiddle:{flex:4,justifyContent:'center',alignItems:'center'},
  headerRight:{flex:1,justifyContent:'center',alignItems:'center'},
  headerRightText:{color:'#FFF',fontWeight:'bold'},
  headerMiddleText:{color:'#FFF',fontWeight:'bold',fontSize: windowSize.height < 500  ? 15 : 18},
  headerLeftImage:{height:25,width:25},
});
