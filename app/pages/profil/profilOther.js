import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
  ListView,
  ScrollView,
  TextInput,
  FlatList
} from 'react-native';
const windowSize = Dimensions.get('window');
import Modal from 'react-native-animated-modal';
import Images from '../../images/images';
import KeepToken from '../../stores/keeptoken';
import UserData from '../../stores/userData';
import ProfilOtherStore from '../../stores/profilOtherStore';
import { observer } from 'mobx-react/native';
import Footer from '../../components/footer';
import ModalBottom from 'react-native-modal';
import Toast from '../../components/toast';
import Firebase  from 'firebase';
import HTML from 'react-native-render-html';

const SCALE_PROFILIMAGE = 0.2
const SCALE_COUNTRY = 0.075
const SCALE_BUTTON = 0.075
const SCALE_BUTTON_IMAGE = 0.060

@observer
export default class ProfilOther extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      isLoaded:false,
      dataSource: ds.cloneWithRows(['row 1', 'row 2', 'row 2', 'row 2', 'row 2', 'row 2', 'row 2']),
      visibleModal:false,
      userID:this.props.navigation.state.params.userID,
      username:this.props.navigation.state.params.username,
      isFellowed:false,
      isFellowingFetchLoaded:false,
      wishListModal: false,
      visibleBlockModal: false,
      visibleAskBlockModal: false,
      visibleToast: false,
      toastMessage: "",
      meBlocked: false,
      reportingModal: false,
      reportingText: '',
      isReportClicked: false,
      noMoreStoryData: false,
      followerLength: null,
      followingLength: null,
      fontSize:14
    };
  }
  createFellow(){
    var formData = new FormData()
    formData.append('user', KeepToken.userID)
    formData.append('follower',this.state.userID)
    fetch('https://fenicoapp.com/api/v1/user-follower/create', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + KeepToken.token
      },
      body:formData
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.setState({isFellowed:true})
    })
    .done();
  }
  checkFellow(){
    fetch("https://fenicoapp.com/api/v1/user-follower/search?user="+KeepToken.userID +"&follower="+this.state.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      //console.log(responseData)
      if(responseData.status === 404){this.setState({isFellowed:false})}else{
        if(responseData.items.length > 0){this.setState({isFellowed:true})}
      }

      this.setState({isFellowingFetchLoaded:true})

    }).catch((error) => {
        //console.log(error)
    });
  }
  fetchUserAllData(){
    fetch("https://fenicoapp.com/api/v1/user/view?id="+ this.state.userID +"&expand=profile,userWishlists,userStories", {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      ProfilOtherStore.data = responseData
      ProfilOtherStore.storyData = responseData.userStories
      this.limitStoryData()
      ProfilOtherStore.wishListData = responseData.userWishlists
      ProfilOtherStore.location = responseData.profile.location
      this.fetchUserCountriesData()
    })
  }
  fetchUserCountriesData(){
    fetch("https://fenicoapp.com/api/v1/user/countries?id="+this.state.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      ProfilOtherStore.countryData = responseData.data
      this.setState({isLoaded:true})
    })
  }
  limitStoryData(){
    //eger length 5 ten fazla ise  önceden yapılmıs bi daha yapma ..
    if(ProfilOtherStore.limitedStoryData.length > 5){
      if(ProfilOtherStore.limitedStoryData.length == ProfilOtherStore.storyData.length){
        this.setState({noStoryMoreData:true})
      }
    }else{
      ProfilOtherStore.limitedStoryData = ProfilOtherStore.storyData.slice(0,5)
    }
  }
  loadExtraStoryData(){
    ProfilOtherStore.limitedStoryData = ProfilOtherStore.storyData.slice(0,ProfilOtherStore.limitedStoryData.length + 5)
    if(ProfilOtherStore.limitedStoryData.length == ProfilOtherStore.storyData.length){
      this.setState({noStoryMoreData:true})
    }
  }
  getFollowerLength(){
    fetch("https://fenicoapp.com/api/v1/user-follower/follower?id="+ this.state.userID, {
          method: "GET",
          headers: {
          'Authorization': 'Bearer ' + KeepToken.token
          }
      })
      .then((response) => response.json())
      .then((responseData) => {
          this.setState({followerLength: responseData.items.length})
      })
  }
  getFollowingLength(){
    fetch("https://fenicoapp.com/api/v1/user-follower/index?id=" + this.state.userID, {
          method: "GET",
          headers: {
              'Authorization': 'Bearer ' + KeepToken.token
          }
      })
      .then((response) => response.json())
      .then((responseData) => {
          this.setState({followingLength: responseData.items.length})
      })
  }
  componentDidMount(){
    this.getFollowerLength()
    this.getFollowingLength()
  }
  routeToFollow(filtre){
    this.props.navigation.navigate('followInfo',{userID:this.state.userID,filtre:filtre})
  }
  setFontSize(){
    if(windowSize.height < 500){ this.setState({ fontSize: 12}) }
  }
  componentWillMount(){
    //Alert.alert(this.state.userID.toString())
    this.setFontSize()
    //Alert.alert(this.state.userID.toString(),KeepToken.userID.toString())
    this.fetchBlocks()
    this.fetchUserAllData()
    this.checkFellow()
  }
  routingToShowCountryStories(userID,countryID,countryName){
    this.props.navigation.navigate('showOtherCountryStories',{userID:userID,countryID:countryID,countryName:countryName})
  }
  routingToShowStory(storyID){
     this.props.navigation.navigate('showStory',{storyID:storyID})
  }
  routing(routeName){
    this.props.navigation.navigate(routeName)
  }
  routingToRoom(){
    this.props.navigation.navigate('room',{contactUserID:this.state.userID,contactUsername:this.state.username,contactImage:'https://fenicoapp.com/api/web/uploads/'+ProfilOtherStore.data.profile.image});
  }
  cleanWishList(){
    var wishListPlan = ProfilOtherStore.wishListData[ProfilOtherStore.wishListData.length - 1].plan
    var splicedPlan = wishListPlan.slice(0,30)
    var inTag = false
    var cleanedPlan = ""
    for( let i = 0; i < splicedPlan.length; i++ ){
      if(!inTag){
        if(splicedPlan.substr(i,1) == "<" && !inTag){
          inTag = true
        }else{
          cleanedPlan += splicedPlan.substr(i,1)
          inTag = false
        }

      }else{
        if(splicedPlan.substr(i,1) == ">"){
          inTag = false
        }
      }
    }
    return cleanedPlan
  }
  fetchBlocks(){
    fetch("https://fenicoapp.com/api/v1/user-block/search?user="+this.state.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      if(typeof responseData.items !== "undefined"){
        for(let i = 0; i<responseData.items.length; i++){
          if(responseData.items[i].blocked.id == KeepToken.userID){
            this.setState({meBlocked:true})
          }
        }
      }
    })
  }
  showToastMessage(text,timeout){
    this.setState({visibleToast:true,toastMessage:text.toString()})
    setTimeout(() => this.setState({visibleToast:false,toastMessage:""}), timeout);
  }
  blockHim(){
    this.setState({visibleBlockModal:null})
    var formData = new FormData()
    formData.append('blocked',this.state.userID)
    fetch('https://fenicoapp.com/api/v1/user-block/create', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + KeepToken.token
      },
      body:formData
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.showToastMessage("You successfully blocked " + this.state.username,2000)
    })
  }
  reportHim(){
    const { userID, username } = this.state
    const reportedFromID = KeepToken.userID
    const reportedFromUsername = KeepToken.username
    
    const reportRef = Firebase.database().ref().child("Reports").child('User').child(userID);
    reportRef.push({id: userID, username: username, reportingText: this.state.reportingText, reportedFromID: reportedFromID, reportedFromUsername: reportedFromUsername})
    this.setState({reportingModal: false, reportingText: ''})
  
    this.showToastMessage('User reported.',2000)
  }
  openReportingModal(){
     this.setState({isReportClicked: true, visibleBlockModal: false})
  }
  openWishListModal(){
    if(ProfilOtherStore.wishListData.length > 0){
      this.setState({wishListModal: true})
    }
  }
  blockAndReportingModalClosed(){
    if(this.state.isReportClicked){
     this.setState({reportingModal: true})
    }
  }
  renderCountries(rowData){
    return(
      <TouchableOpacity onPress={() => this.routingToShowCountryStories(this.state.userID,rowData.id,rowData.name)} style={{height:windowSize.height * SCALE_COUNTRY,width:windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) / 2,margin:10}}>
       <Image source={{uri: rowData.flag + "/128/" + rowData.name + ".png"}} style={{height:windowSize.height * SCALE_COUNTRY,width:windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) / 2,resizeMode:'stretch'}} />
      </TouchableOpacity>
    )
  }
  render() {
    const { visibleAskBlockModal } = this.state
    if(!this.state.isLoaded || !this.state.isFellowingFetchLoaded){return(null)}
    return (
      <View style={styles.container}>
      <ModalBottom onModalHide={() => this.blockAndReportingModalClosed() } isVisible={this.state.visibleBlockModal}>
        <View style={{position:'absolute',left:0,bottom:55,backgroundColor:'#fff',height:135,width:windowSize.width-40,borderRadius:10}}>
          <View style={{height:35,justifyContent:'center',alignItems:'center',borderBottomWidth:1,borderColor:'#e0e0e0'}}>
            <Text style={{color:'#A0A0A0',fontSize:12}}>More</Text>
          </View>
          <TouchableOpacity onPress={() => this.blockHim()} activeOpacity={1} style={{height:50,justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#ff5b40'}}>Block User</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.openReportingModal() } activeOpacity={1} style={{height:50,justifyContent:'center',alignItems:'center',borderTopWidth:1,borderColor:'#e0e0e0'}}>
            <Text style={{color:'#ff5b40'}}>Report User</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity activeOpacity={1} onPress={() => this.setState({visibleBlockModal:false}) } style={{justifyContent:'center',alignItems:'center',position:'absolute',bottom:0,left:0,height:50,width:windowSize.width-40,backgroundColor:'#fff',borderRadius:10}}>
          <Text style={{color:'#5c5c5c'}}>Cancel</Text>
        </TouchableOpacity>
      </ModalBottom>

      <Modal isVisible={this.state.wishListModal}
             backdropColor = '#000000'
             backdropOpacity = {0.85}
             animationIn={'bounceIn'}
             animationOut={'bounceOut'}
             style={{justifyContent:'center',alignItems:'center'}}
             >
         <View style={{height:300,width:windowSize.width-60,backgroundColor:'white',borderRadius:10}}>
           <View style={{height:290,width:windowSize.width-60,borderRadius:10}}>
            <View style={{flex:2,borderColor:'#E0E0E0',borderTopLeftRadius:10,borderTopRightRadius:10,backgroundColor:'#f0a574',flexDirection:'row',justifyContent:'center',alignItems:'center',borderBottomWidth:1.5}}>
              <View style={{flex:1}}></View>
              <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#FFF',fontWeight:'bold',fontSize:17}}>Wish</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({wishListModal :false})} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image style={{height:17,width:17}} source={Images.cancel} />
              </TouchableOpacity>

            </View>
            <View style={{flex:9,flexDirection:'row',borderBottomLeftRadius:10,borderBottomRightRadius:10,backgroundColor:'#FFF',justifyContent:'center',alignItems:'center'}}>
              <ScrollView style={{padding:10}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  {ProfilOtherStore.wishListData.length > 0 ? 
                    <HTML  html={ProfilOtherStore.wishListData[ProfilOtherStore.wishListData.length - 1].plan} />
                  : 
                    null
                  }
                </View>
              </ScrollView>
            </View>
            
          </View>
           
         </View>
       </Modal>
      <Modal style={{justifyContent:'center',alignItems:'center'}} isVisible={this.state.reportingModal}>
          <View style={{height:220,width:windowSize.width-50,backgroundColor:'#fff',borderRadius:6}}>
            <View style={{flex:1.2,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
              <View style={{flex:1}}></View>
              <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#e74c3c',fontSize:16}} >Reporting</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({reportingModal: false}) } style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Image style={{height:13,width:13}} source={Images.grayCancel} />
              </TouchableOpacity>
              
            </View>
            <View style={{flex:3}}>
              <TextInput multiline={true} onChangeText={(text) => this.setState({reportingText:text})} placeholder={"Please, describe the problem"} placeholderTextColor={'#A0A0A0'}  style={{marginLeft:10,borderWidth:1,borderColor:'#d0d0d0',height:95,width:windowSize.width-70,fontSize:15}} />
            </View>
            <TouchableOpacity onPress={() => this.reportHim()} style={{flex:1.2,flexDirection:'row',backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center',borderBottomLeftRadius:6,borderBottomRightRadius:6}}>
                <View style={{height:35,justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:'#fff',fontSize:18}}>Report</Text>
                </View>
            </TouchableOpacity>
          </View>
      </Modal>
      <Modal isVisible={this.state.visibleModal === 'locationModal'}
             backdropColor = '#000000'
             backdropOpacity = {0.85}
             animationIn={'bounceIn'}
             animationOut={'bounceOut'}
             style={{justifyContent:'center',alignItems:'center'}}
             >
         <View style={{height:150,width:260,backgroundColor:'#f0a574',borderRadius:10}}>
           <View style={{flex:2,borderColor:'#E0E0E0',flexDirection:'row',justifyContent:'center',alignItems:'center',borderBottomWidth:1.5}}>
             <View style={{flex:1}}></View>
             <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
               <Text style={{color:'#FFF',fontWeight:'bold',fontSize:17}}>Current Location</Text>
             </View>
             <TouchableOpacity onPress={() => this.setState({visibleModal:false})} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
               <Image style={{height:17,width:17}} source={Images.cancel} />
             </TouchableOpacity>

           </View>
           <View style={{flex:5,flexDirection:'row',borderBottomLeftRadius:10,borderBottomRightRadius:10,backgroundColor:'#FFF',justifyContent:'center',alignItems:'center'}}>
             {ProfilOtherStore.location === null ? 
             <Text style={{fontWeight:'bold',color:'gray'}}>Location not specified</Text>
             : null }
             <Text style={{fontWeight:'bold',color:'gray',textAlign:'center'}}>{ProfilOtherStore.location}</Text>
           </View>
         </View>
       </Modal>
        <View style={{flex:3,flexDirection:'row'}}>
            <Image style={{position:'absolute',left:0,top:0,height:400,width:windowSize.width,resizeMode:'stretch'}} source={Images.backgroundOfProfilImage} />
           
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              {this.state.isFellowed || this.state.meBlocked ? null :
                <View style={{flex:0.7,justifyContent:'flex-end',alignItems:'center',opacity:0.5}}>
                  <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.backButton}>
                    <Image style={styles.backButtonImage} source={Images.leftArrow} />
                  </TouchableOpacity>
                </View>
               }
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                {this.state.isFellowed || this.state.meBlocked ?
               <View style={{opacity:0.5}}>
                 <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.backButton}>
                   <Image style={styles.backButtonImage} source={Images.leftArrow} />
                 </TouchableOpacity>
               </View>
              :
              <TouchableOpacity onPress={() => this.createFellow()} style={styles.profilButtons}>
                <Image style={styles.circleNotificationButton} source={Images.addUser} />
              </TouchableOpacity>
            }
              </View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                {this.state.isFellowed ?
              <TouchableOpacity onPress={() => this.setState({visibleModal:'locationModal'})} style={styles.profilButtons}>
                <Image style={styles.circleLocationButton} source={Images.location} />
              </TouchableOpacity>
              :
              <TouchableOpacity style={styles.profilButtons}>
                <Image style={styles.circleLocationButton} source={Images.locationPassive} />
              </TouchableOpacity>
             }
              </View>
            </View>
            <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
              <View style={{height:windowSize.height * SCALE_PROFILIMAGE,width:windowSize.height * SCALE_PROFILIMAGE,borderRadius:(windowSize.height * SCALE_PROFILIMAGE) / 2,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
                {ProfilOtherStore.data.profile.image !== null ? 
                  <Image style={styles.profilImagePlaceImage} source={{uri: 'https://fenicoapp.com/api/web/uploads/'+ProfilOtherStore.data.profile.image}} />
                : 
                  <Image style={styles.profilImagePlaceImage} source={Images.noImage} />
                }
              </View>
                <Text style={styles.nameText}>{ProfilOtherStore.data.profile.name}</Text>
                <Text style={styles.usernameText}>@{ProfilOtherStore.data.username}</Text>
                <Text style={styles.blockedText}>{this.state.meBlocked ? "You were blocked" : null }</Text>
                <View style={{flexDirection:'row',backgroundColor:'transparent'}}>
                  <TouchableOpacity onPress={() => this.routeToFollow('Followers')}>
                    {this.state.followerLength ? 
                      <Text style={{color:'#FFF',fontSize: windowSize.height < 500 ? 11 : 14 }}>Followers {this.state.followerLength}</Text>
                    :
                      <Text style={{color:'#FFF',fontSize: windowSize.height < 500 ? 11 : 14 }}>Followers 0</Text>

                    }
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.routeToFollow('Following')}>
                    {this.state.followingLength ? 
                      <Text style={{color:'#FFF',marginLeft:15,fontSize: windowSize.height < 500 ? 11 : 14}}>Following {this.state.followingLength}</Text>
                    :
                      <Text style={{color:'#FFF',marginLeft:15,fontSize: windowSize.height < 500 ? 11 : 14 }}>Following 0</Text>

                    }
                  </TouchableOpacity>
                </View>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              {this.state.isFellowed || this.state.meBlocked ? null :
                <View style={{flex:0.7,justifyContent:'flex-end',alignItems:'center',opacity:0.5}}>
                  
                </View>
               }
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                {this.state.meBlocked ? 
              null
             :
              <TouchableOpacity onPress={() => this.setState({visibleBlockModal:true})} style={styles.profilButtons}>
                <Image style={styles.circleNotificationButton} source={Images.more} />
              </TouchableOpacity>
              }
              </View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                 {this.state.isFellowed && !this.state.meBlocked ?
              <TouchableOpacity onPress={() => this.routingToRoom()} style={styles.profilButtons}>
                <Image style={styles.circleMessageButton} source={Images.message} />
              </TouchableOpacity>
              :
              <TouchableOpacity style={styles.profilButtons}>
                <Image style={styles.circleMessageButton} source={Images.messagePassive} />
              </TouchableOpacity>
             }
              </View>
            </View>
        
        </View>
        <View style={{flex:1.2,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
        <Text style={{marginTop:5,fontSize: this.state.fontSize}}>I have been</Text>
        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
          <TouchableOpacity onPress={() => this.routingToShowCountryStories(this.state.userID,'','')} style={{height:windowSize.height * SCALE_COUNTRY,width:windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) /2,margin:10,backgroundColor:'transparent'}}>
            <Image source={Images.earth}  style={{height:windowSize.height * SCALE_COUNTRY,width:windowSize.height * SCALE_COUNTRY,borderRadius:(windowSize.height * SCALE_COUNTRY) /2}} />
          </TouchableOpacity>
          <ListView
            dataSource={ProfilOtherStore.dataSourceCountries}
            contentContainerStyle={{flexDirection:'row'}}
            renderRow={this.renderCountries.bind(this)}
          />
        </ScrollView>
        </View>
        <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#FFF',shadowColor: '#34495e',shadowOffset: {width: 2,height: 3},shadowRadius: 5,shadowOpacity: 1}}>
         <View style={{flex:1,width:windowSize.width,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
           <Text style={{marginTop:5,fontSize:this.state.fontSize}}>Wish List</Text>
           <TouchableOpacity onPress={() => this.props.navigation.navigate('wishLists',{userID:this.state.userID}) } style={{position:'absolute',right:5,top:5}}>
              <Image source={Images.grayList}  style={{height:22,width:22}} />
           </TouchableOpacity>
         </View>
         <View style={{flex:3,width:windowSize.width}}>
           <TouchableOpacity onPress={() => this.openWishListModal()} style={{margin:10,justifyContent:'center',alignItems:'center'}}>
             
             {ProfilOtherStore.wishListData.length > 0 ?
               <Text style={{fontSize: this.state.fontSize}} >{this.cleanWishList()}...</Text>
               :
              <Text style={{fontSize: this.state.fontSize}}>"No wishlist"</Text>
              }
             
           </TouchableOpacity>
         </View>
        </View>
        <View style={{flex:2,backgroundColor:'#fff',shadowColor: '#34495e',shadowOffset: {width: 2,height: 3},shadowRadius: 5,shadowOpacity: 1}}>
          <View style={{flex:1,justifyContent:'center',alignItems:'center',borderBottomWidth:1,borderColor:'#a0a0a0'}}>
            <Text style={{marginTop:5,fontSize:this.state.fontSize}}>Stories</Text>
          </View>
          <View style={{flex:3}}>
            <FlatList
              data={ProfilOtherStore.limitedStoryData}
              renderItem={this.renderStoryItem.bind(this)}
              ListFooterComponent={this.renderStoryListFooter.bind(this)}
            />  
          </View>
          
        </View>
        <Toast
            visible={this.state.visibleToast}
            position={windowSize.height-150}
            shadow={true}
            animation={true}
            hideOnPress={true}
          >{this.state.toastMessage}</Toast>
        <View style={styles.blankSpace}></View>
        <Footer />
      </View>
    );
  }
  renderStoryListFooter(){
    if(this.state.noStoryMoreData){
      return(
        <View style={{height:40,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontWeight:'700',color:'#a0a0a0'}}>No More Stories</Text>
        </View>
      )
    }else{
      return(
        <TouchableOpacity onPress={() => this.loadExtraStoryData()} style={{height:40,justifyContent:'center',alignItems:'center'}}>
          <View style={{height:26,width:120,borderRadius:13,backgroundColor:'#e0e0e0',justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontWeight:'700',color:'#a0a0a0'}}>Load More</Text>
          </View>
        </TouchableOpacity>
      )
    }
  }
  renderStoryItem(data){
    return (
        <TouchableOpacity onPress={() => this.routingToShowStory(data.item.id)} style={{flex:1,height:40,flexDirection:'row',borderBottomWidth:1,borderColor:'#a0a0a0'}}>
          <View style={{flex:1}}>
            {data.item.userStoryImages.length == 0 ? 
              <View style={{flex:1,backgroundColor:'#a0a0a0'}}></View>
            : 
              <Image source={{uri: data.item.userStoryImages[0].img }} style={{height:null,width:null,flex:1}} />
            }
          </View>
          <View style={{flex:2.5}}>
            <View style={{margin:5,flex:1}}>
                <HTML html={data.item.title} />
            </View>
          </View>
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {flex: 1,backgroundColor: '#F5FCFF',},
  blankSpace:{height:50},
  profilButtons:{backgroundColor:'#ecf0f1',height:windowSize.height * SCALE_BUTTON,width:windowSize.height * SCALE_BUTTON,borderRadius:(windowSize.height * SCALE_BUTTON) / 2,shadowColor: '#34495e',shadowOffset: {width: 1,height: 1.5},shadowRadius: 1,shadowOpacity: 0.5,justifyContent:'center',alignItems:'center'},
  profilButtonsLeftTop:{position:'absolute',left:30,top:40,},
  profilButtonsLeftBottom:{position:'absolute',left:30,bottom:40,},
  profilButtonsRightTop:{position:'absolute',right:30,top:40,},
  profilButtonsRightBottom:{position:'absolute',right:30,bottom:40,},
  profilImagePlaceView:{position:'absolute',left:(windowSize.width-150)/2,bottom:70,backgroundColor:'#fff',height:150,width:150,borderRadius:75,justifyContent:'center',alignItems:'center'},
  profilImagePlaceImage:{height:(windowSize.height * SCALE_PROFILIMAGE ) - 4,width:(windowSize.height * SCALE_PROFILIMAGE ) - 4,borderRadius:((windowSize.height * SCALE_PROFILIMAGE ) - 4)/2,resizeMode:'cover'},
  namePlace:{fontSize: windowSize.height < 500 ? 11 : 14,position:'absolute',bottom:10,left:(windowSize.width-200)/2,height:70,width:200,justifyContent:'center',alignItems:'center'},
  nameText:{textAlign:'center',fontSize: windowSize.height < 500 ? 11 : 13,backgroundColor:'transparent',color:'#fff'},
  usernameText:{textAlign:'center',fontSize:windowSize.height < 500 ? 11 : 12,backgroundColor:'transparent',color:'#fff'},
  blockedText:{fontSize:windowSize.height < 500 ? 11 : 14,backgroundColor:'transparent',color:'#fff'},
  circleNotificationButton:{height:windowSize.height * SCALE_BUTTON_IMAGE,width:windowSize.height * SCALE_BUTTON_IMAGE},
  circleMessageButton:{height:windowSize.height * SCALE_BUTTON_IMAGE,width:windowSize.height * SCALE_BUTTON_IMAGE},
  circleLocationButton:{height:windowSize.height * SCALE_BUTTON_IMAGE,width:windowSize.height * SCALE_BUTTON_IMAGE},
  circleSettingButton:{height:windowSize.height * SCALE_BUTTON_IMAGE,width:windowSize.height * SCALE_BUTTON_IMAGE},
  backButton:{height:40,width:40,borderRadius:20,backgroundColor:'#34495e',opacity:0.8,justifyContent:'center',alignItems:'center'},
  backButtonImage:{opacity:1}
});
