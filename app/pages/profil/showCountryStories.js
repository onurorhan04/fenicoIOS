import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,View,TouchableOpacity,Image,ScrollView,Dimensions,ListView,Alert} from 'react-native';
var windowSize = Dimensions.get('window');
import Swiper from 'react-native-swiper';
import { observer } from 'mobx-react/native';
import Images from '../../images/images'
import Footer from '../../components/footer'
import ShowCountryStore from '../../stores/showCountryStore';
import UserData from '../../stores/userData';
import KeepToken from '../../stores/keeptoken';
import Colours from '../../const/colours';
import Toast from '../../components/toast';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import HTML from 'react-native-render-html';
import moment from 'moment'

@observer
export default class ShowCountryStories extends Component {
  constructor(props){
    super(props)
    this.state={
      isLoaded: false,
      userID: this.props.navigation.state.params.userID,
      countryID: this.props.navigation.state.params.countryID,
      countryName: this.props.navigation.state.params.countryName,
      visibleToast: false,
      toastMessage: '',
      loadingOverlay: false,
      loadingOverlayMessage: ''
    }
  }
  getStoryData(){
    var fetch_url = ""
    if(this.state.countryID !== ""){
    
      fetch_url = "https://fenicoapp.com/api/v1/user-story/index?userid="+this.state.userID+"expand=userStoryImages&country_id=" + this.state.countryID
    }else{
      fetch_url = "https://fenicoapp.com/api/v1/user-story/index?userid="+this.state.userID+"expand=userStoryImages"
    }
    fetch(fetch_url, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      //console.log(responseData)

      //Alert.alert('',JSON.stringify(responseData.items))
      ShowCountryStore.stories = responseData.items


      ShowCountryStore.stories.forEach(function(story) {
        if(story.userStoryLikes.length == 0){
          ShowCountryStore.liked.push({'story_id':story.id,'liked':false,'likeCount':0})
        }
        story.userStoryLikes.forEach(function(like) {
          ShowCountryStore.liked.push({'story_id':story.id,'liked':false,'likeCount':story.like_count})
        });

        story.userStoryLikes.forEach(function(like) {
          if(like.olusturan == KeepToken.userID){
            var index = ShowCountryStore.liked.findIndex(item =>item.story_id === story.id)
            ShowCountryStore.liked[index].liked = true
          }

        });
      });
      this.setState({isLoaded:true})


      //Alert.alert('',JSON.stringify(ShowCountryStore.liked))

    }).catch((error) => {
       // this.props.navigation.goBack(null)
    });
  }
  componentWillMount(){
      this.getStoryData()
  }

  showToastMessage(text,timeout){
    this.setState({visibleToast:true,toastMessage:text.toString()})
    setTimeout(() => this.setState({visibleToast:false,toastMessage:""}), timeout);
  }
  fetchUserCountriesData(){
    fetch("https://fenicoapp.com/api/v1/user/countries?id="+KeepToken.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      UserData.countryData = responseData.data
      this.setState({loadingOverlayMessage:'Story removed, You will be redirected to the previous page within few seconds..'})
      setTimeout(() => this.redirect(), 2000);
    })
  }
  fetchUserAllData(){
    fetch("https://fenicoapp.com/api/v1/user/view?id="+ KeepToken.userID +"&expand=profile,userWishlists,userStories", {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      UserData.data = responseData
      UserData.storyData = responseData.userStories
      UserData.limitedStoryData = UserData.storyData.slice(0,UserData.shownStoryDataLength)
      UserData.wishListData = responseData.userWishlists
      UserData.location = responseData.profile.location

      this.fetchUserCountriesData()

    })
  }
  deleteStory(storyID){
    this.setState({loadingOverlay: true,loadingOverlayMessage:'Story is removing..'})
    fetch("https://fenicoapp.com/api/v1/user-story/delete?id=" + storyID, {
      method: "DELETE",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.fetchUserAllData()
    }).catch((err) => {
      this.fetchUserAllData()
    })
  }
  redirect(){
    this.setState({loadingOverlay: false})
    this.props.navigation.goBack(null)
  }
  addToWishlist( storyID, wishlistText, image ){
    //Alert.alert('',image.toString())
    var formData = new FormData()
    formData.append('story_id', storyID)
    formData.append('plan', wishlistText)
    formData.append('image_link', image)
    fetch('https://fenicoapp.com/api/v1/user-wishlist/create', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + KeepToken.token
      },
      body:formData
    })
    .then((response) => response.json())
    .then((responseData) => {
      if(responseData.status == "success"){
        UserData.wishListData.push({'plan':wishlistText.toString()})
        this.showToastMessage("WishList created succesfully..",2000)
      }
    })
  }
  async clickedHeart(storyID){
     var index = ShowCountryStore.liked.findIndex(item =>item.story_id === storyID)
     if(ShowCountryStore.liked[index].liked){
       ShowCountryStore.liked[index].likeCount--
     }else{
       ShowCountryStore.liked[index].likeCount++
     }
     ShowCountryStore.liked[index].liked = !ShowCountryStore.liked[index].liked
     ShowCountryStore.stories.push({'like':'like'})
    if(!ShowCountryStore.liked[index].liked){
      await fetch("https://fenicoapp.com/api/v1/user-story-like/delete?story_id=" + storyID, {
        method: "DELETE",
        headers: {
          'Authorization': 'Bearer ' + KeepToken.token
        }
      })
      .then((response) => response.json())
      .then((responseData) => {

      }).catch((error) => {
          //Alert.alert('Error',JSON.stringify(error))
      });
    }else{

      var formData = new FormData();
      formData.append('story',storyID);
      await fetch('https://fenicoapp.com/api/v1/user-story-like', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer ' + KeepToken.token
        },
        body: formData
      }).then((resp) => {
        if(resp.status == "success" || resp.status == 200){
        }
      }).catch((error) => {
        //Alert.alert('Error',JSON.stringify(error))
      })
    }
  }
  
  createNoLocationStory(){
    this.props.navigation.navigate('addStoryWithoutLoc')
  }
  renderStories( rowData ){
    if(rowData.like == 'like'){
      return(null)
    }else{
      var index = ShowCountryStore.liked.findIndex(item =>item.story_id === rowData.id)
      return(
      <View style={styles.borderContent}>
        <View style={{height:60,flexDirection:'row'}}>
          <TouchableOpacity style={{flex:1,marginLeft:10,justifyContent:'center',alignItems:'center'}}>
           {rowData.user.image.length < 40 ?
             <Image style={{height:46,width:46,borderRadius:23}} source={Images.noImage} />
             :
             <Image style={{height:46,width:46,borderRadius:23}} source={{uri:rowData.user.image}} />
           }
          </TouchableOpacity>
          <View style={{flex:4,flexDirection:'column',justifyContent:'center'}}>
            <Text style={{color:'#808080',marginLeft:10,fontSize:15,fontWeight:'bold'}}>{rowData.user.name}</Text>
            <Text style={{color:'#A0A0A0',marginLeft:10,fontSize:12}}>{rowData.cityDetails} {moment(rowData.olusturuldu).format("MMM Do YY") == "Invalid date" ? "" : moment(rowData.olusturuldu).format("MMM Do YY") } </Text>
          </View>
          <TouchableOpacity onPress={() => this.deleteStory(rowData.id) } style={{flex:1,margin:7,justifyContent:'center',alignItems:'flex-end'}}>
            <Image style={{height:30,width:30}} source={Images.trash} />
          </TouchableOpacity>
        </View>
        <View style={{height:300,width:windowSize.width-20,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
          <Swiper width={windowSize.width-20} horizontal={true} removeClippedSubviews={false} showsPagination={false} >
          {rowData.image.map((image) => {
            return (
              <View style={{justifyContent:'center',alignItems:'center',width:windowSize.width-20}} >
                <Image style={{height:300,width:windowSize.width-50,resizeMode:'contain'}} source={{uri:image}} />
              </View>
            )
          })}
          </Swiper>
        </View>
        <View style={{marginLeft:15,marginRight:15,marginBottom:15}}>
          <HTML html={rowData.title} />
          <HTML html={rowData.story} />
          
          
        </View>
        <View style={{flex:1,height:50,width:null,borderTopWidth:1,borderColor:'#A0A0A0',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
          <TouchableOpacity onPress={() => this.addToWishlist(rowData.id, rowData.title, rowData.image[0])} style={{marginLeft:10}}>
            <Image style={{height:25,width:25}} source={Images.addToWishList}/>
          </TouchableOpacity>
        <View style={{flex:1}}></View>
          <TouchableOpacity onPress={() => this.clickedHeart(rowData.id)} style={{flex:1,flexDirection:'row',justifyContent:'flex-end',alignItems:'center'}}>
            {ShowCountryStore.liked[index].likeCount !== undefined && ShowCountryStore.liked[index].likeCount !== null ? 
            <Text style={{color:'#ffa500'}}>0</Text>
            : 
            <Text style={{color:'#ffa500'}}>{ShowCountryStore.liked[index].likeCount}</Text>
            }
            <Image style={{marginRight:5,height:28,width:28}} source={ShowCountryStore.liked[index].liked == false ? Images.heartbordered : Images.heartfilled} />
          </TouchableOpacity>
        </View>
      </View>
      )
    }
  }
  render(){
    
    if(this.state.isLoaded){
    return (
      <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.headerBlank}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.headerLeft}>
           <Image style={styles.headerLeftImage} source={Images.leftArrow} />
          </TouchableOpacity>
          <View style={styles.headerMiddle}>
            <Text style={styles.headerMiddleText}>Stories {this.state.countryID ? "(" + this.state.countryName + ")" : null }</Text>
          </View>
          {this.state.countryID == "" ? 
            <TouchableOpacity onPress={() => this.createNoLocationStory()} style={styles.headerRight}>
            <Image source={Images.whiteBasePlus} style={{height:20,width:20,borderRadius:10}} />
          </TouchableOpacity>
          :
          <TouchableOpacity style={styles.headerRight}>
            
          </TouchableOpacity>
          }
          
        </View>
      </View>
        <ScrollView style={{margin:10,backgroundColor:'transparent'}}>
          <ListView
            style={styles.listview}
            dataSource={ShowCountryStore.dataSourceStories}
            renderRow={this.renderStories.bind(this)}
          />

          <View style={styles.blankSpace}></View>

        </ScrollView>
        <Toast
            visible={this.state.visibleToast}
            position={windowSize.height-150}
            shadow={true}
            animation={true}
            hideOnPress={true}
          >{this.state.toastMessage}</Toast>
        <OrientationLoadingOverlay
          visible={this.state.loadingOverlay}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message={this.state.loadingOverlayMessage}/>
        <Footer />
      </View>
    );
  }else{
    return null
  }
  }
}
const styles = StyleSheet.create({
  container: {flex: 1,backgroundColor: '#e9edf1',},
  content:{flex:1,borderWidth:1,borderColor:'black'},
  blankSpace:{height:50},
  borderContent:{flex:1,marginTop:20,elevation: 1,borderWidth: 1,borderColor: '#ddd',borderBottomWidth: 0,backgroundColor:'white'},
  listview:{backgroundColor:'#e9edf1',},
  headerBlank:{marginTop:windowSize.height < 500 ? 0 : 20 ,height:50,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  header:{height:windowSize.height < 500 ? 50 : 70,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerLeft:{flex:1,justifyContent:'center',alignItems:'center'},
  headerMiddle:{flex:4,justifyContent:'center',alignItems:'center'},
  headerRight:{flex:1,justifyContent:'center',alignItems:'center'},
  headerRightText:{color:'#FFF',fontWeight:'bold'},
  headerMiddleText:{color:'#FFF',fontWeight:'bold',fontSize:windowSize.height < 500  ? 15 : 18},
  headerLeftImage:{height:25,width:25},
});
//story_id image plan
