import React, { Component } from 'react'
import {AppRegistry,StyleSheet,Text,View,TouchableOpacity,Platform,Image,Dimensions,TextInput,Alert,ListView} from 'react-native'
const windowSize = Dimensions.get('window')
import ImagePicker from 'react-native-image-picker'
import Images from '../../images/images'
import UserData from '../../stores/userData'
import KeepToken from '../../stores/keeptoken'
import ProfileStories from '../../stores/profilStories'
import ProfilSettingsStore from '../../stores/profilSettingsStore'
import Switch from 'react-native-switch-pro'
import RNFetchBlob from 'react-native-fetch-blob'
import Toast from '../../components/toast'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { observer } from 'mobx-react/native'
import DB from '../../realmDB/databases'
import Button from 'apsl-react-native-button'
import Modal from 'react-native-animated-modal';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';


import {FBLogin, FBLoginManager} from 'react-native-facebook-login'
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
@observer
export default class ProfileSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageSource: "",
      isLoaded: false,
      changedPhoto: "",
      changedName: "",
      changedUsername: "",
      changedShowmessage: "",
      visibleToast: false,
      toastMessage: "",
      fbLogin: false,
      isOpenedBlockedUsersModal: false,
      loadingOverlay: false,
      dataSourceBlockedUsers: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    }
  }
  fetchUserAllData(){
    fetch("https://fenicoapp.com/api/v1/user/view?id="+ KeepToken.userID +"&expand=profile,userWishlists,userStories", {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.setState({ loadingOverlay: false, visibleToast:1,toastMessage:'Profil succesfully updated'})
      UserData.data = responseData
      UserData.storyData = responseData.userStories
      UserData.wishListData = responseData.userWishlists
      setTimeout(() => this.setState({visibleToast:false,position:0,text:""}), 2000);
    })
  }
  openLibrary(){
    var options = {
      title: 'Select Avatar',
      quality:1,
      maxWidth:400,
      noData:false,
      customButtons: [
        {name: 'fb', title: 'Choose Photo from Facebook'}
      ],

      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      }
      else if (response.error) {
        console.log(error)
      }
      else if (response.customButton) {

      }
      else {
        const source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};
        if (Platform.OS === 'ios') {
          console.log('response.uri: ', response.uri)
          const source = {uri: response.uri.replace('file://', ''), isStatic: true};
          console.log('source: ', source)
        } else {
          const source = {uri: response.uri, isStatic: true};
        }
        if(source){
          this.setState({
            changedPhoto: source
          });
          console.log('state', this.state.changedPhoto)
        }

      }
    });
  }
  getProfileData(){
    fetch("https://fenicoapp.com/api/v1/user-profile/view?id="+KeepToken.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
        this.getBlockedUsers()
        UserData.userProfileData=responseData.items[0]
        ProfileStories.photo = responseData.items[0].image

        console.log(UserData.userProfileData)

        if(UserData.userProfileData.show_message == 0 ){
          ProfileStories.showMessage = false
        }else{
          ProfileStories.showMessage = true
        }
    }).catch((error) => {
        console.log(error);
    });
  }
  changeSPLogin(){
    if(this.state.changedUsername != ""){
      const loginObjects = DB.objects('SPLoginSchema')
      const username = this.state.changedUsername
      const password = loginObjects[0].password
      DB.write(() => {
        DB.delete(loginObjects)
        const login = DB.create('SPLoginSchema',
          {
            username: username,
            password: password 
          }
        )
      })
    }
  }
  changeFBLogin(){
    if(this.state.changedUsername != ""){
      const loginObjects = DB.objects('FaceBookLoginSchema')

      const username = this.state.changedUsername
      const password = loginObjects[0].password
      const fbtoken = loginObjects[0].fbtoken
      const email = loginObjects[0].email
      
      DB.write(() => {
        DB.delete(loginObjects)
        const login = DB.create('FaceBookLoginSchema',
          {
            username: username,
            password: password,
            fbtoken: fbtoken,
            email: email
            
          }
        )
      })
    }
  }
  async save(){
    if(this.state.changedName != "" || this.state.changedPhoto != ""){
      this.setState({ loadingOverlay: true })
      var formDataForURLENCODED = [];
      if(this.state.changedName != ""){
        formDataForURLENCODED.push(encodeURIComponent('name') + "=" + encodeURIComponent(this.state.changedName));
      }
      if(this.state.changedPhoto != ""){
        formDataForURLENCODED.push(encodeURIComponent('image') + "=" + encodeURIComponent(RNFetchBlob.wrap(this.state.changedPhoto.uri)))
      }
      const body=formDataForURLENCODED.join("&");
      await fetch('https://fenicoapp.com/api/v1/user-profile/update?id='+KeepToken.userID, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
          'Authorization': 'Bearer ' + KeepToken.token
        },
        body
      }).then((resp) => {
        console.log('settings update: ', resp)
        if(resp.status == "success" || resp.status == 200){
          this.fetchUserAllData()

        }
      }).catch((err) => {
        //Alert.alert('Error',JSON.stringify(err))
      })
    }
    if(this.state.changedUsername != "" || this.state.changedShowmessage != ""){
      this.setState({ loadingOverlay: true })
      var formDataForURLENCODED = [];
      if(this.state.changedUsername != ""){
        formDataForURLENCODED.push(encodeURIComponent('username') + "=" + encodeURIComponent(this.state.changedUsername));
      }
      if(this.state.changedShowmessage){
        if(this.state.changedShowmessage === "true"){
          formDataForURLENCODED.push(encodeURIComponent('show_message') + "=" + encodeURIComponent(1));
        }else{
          formDataForURLENCODED.push(encodeURIComponent('show_message') + "=" + encodeURIComponent(0));
        }
      }
      console.log('formdataiamge: ', formDataForURLENCODED)
      const body=formDataForURLENCODED.join("&");
      await fetch('https://fenicoapp.com/api/v1/user/update?id='+KeepToken.userID, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
          'Authorization': 'Bearer ' + KeepToken.token
        },
        body
      }).then((resp) => {
        if(resp.status == "success" || resp.status == 200){
          if(this.state.fbLogin){
            this.changeFBLogin()
          }else{
            this.changeSPLogin()
          }
          this.fetchUserAllData()
        }
      }).catch((err) => {
        //Alert.alert('Error',JSON.stringify(err))
      })
    }
  }
  componentWillMount(){
    const fbLoginInfo = DB.objects('FaceBookLoginSchema')
    if(fbLoginInfo.length > 0 ){
      this.setState({fbLogin:true})
    }
  }
  componentDidMount(){
   this.getProfileData()
  }
  changedSwitch(value){
    ProfileStories.showMessage = value
    this.setState({changedShowmessage:value.toString()})
  }
  fbLogout(){
    const deleteObjects = DB.objects('FaceBookLoginSchema')
    DB.write(() => {
      DB.delete(deleteObjects)
    })
    this.props.navigation.dispatch({type: 'Reset', index: 0, actions: [{ type: 'Navigate', routeName: 'loginPage'}] })
  }
  logout(){
    const deleteObjects = DB.objects('SPLoginSchema')
    DB.write(() => {
      DB.delete(deleteObjects)
    })
    this.props.navigation.dispatch({type: 'Reset', index: 0, actions: [{ type: 'Navigate', routeName: 'loginPage'}] })
  }
  getBlockedUsers(){
    fetch("https://fenicoapp.com/api/v1/user-block/search?user="+KeepToken.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.setState({
        isLoaded:true
      })
      console.log(responseData)
      responseData.status ? ProfilSettingsStore.blockedUsers = [] : ProfilSettingsStore.blockedUsers = responseData.items
      
      //this.setState({dataSourceBlockedUsers:this.state.dataSourceBlockedUsers.cloneWithRows(responseData.items)})
    }).catch((error) => {
      ProfilSettingsStore.blockedUsers = []
        console.log(error);
    });
  }
  undoBlock(ID){
    let newArray = ProfilSettingsStore.blockedUsers.filter(function(item){
      return item.id !== ID
    });
    ProfilSettingsStore.blockedUsers = newArray
    fetch('https://fenicoapp.com/api/v1/user-block/delete?id='+ID, {
      method: 'DELETE',
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    }).then((resp) => {
    }).catch((err) => {
    })
  }
  renderPhoto(){
    if(ProfileStories.photo !== null){
      return <Image style={styles.profilImagePlaceImage} source={{uri:  "https://fenicoapp.com/api/web/uploads/" + ProfileStories.photo}} />
    }else{
      return <Image style={styles.profilImagePlaceImage} source={Images.noImage} />
    }
  }
  renderBlockedUsers(rowData){
    var imageView = ""
    if(rowData.blocked.image == null || rowData.blocked.image == "" || rowData.blocked.image.length < 40){
       imageView = <Image style={{height:40,width:40,borderRadius:20}} source={Images.noImage} />
    }else{
       imageView = <Image style={{height:40,width:40,borderRadius:20}} source={{uri: rowData.blocked.image}} />
    }
    return(
      <View style={{flexDirection:'row',height:50,justifyContent:'center',alignItems:'center'}}>
        <View style={{flex:1,justifyContent:'center',alignItems:'center',marginLeft:9}}>
          {imageView}
        </View>
        <View style={{flex:5,justifyContent:'center'}}>
          <Text style={{marginBottom:5,marginLeft:10}}>{rowData.blocked.name} </Text>
        </View>
        <TouchableOpacity onPress={() => this.undoBlock(rowData.id)} style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
          <Image source={Images.undo} style={{height:29,width:29}} />
        </TouchableOpacity>
      </View>
    )
  }
  render() {
    if(!this.state.isLoaded){
      return(
        <View></View>
      )
    }else{
      return (
        <View style={styles.container}>
          <Modal style={{justifyContent:'center',alignItems:'center'}} isVisible={this.state.isOpenedBlockedUsersModal}>
            <View style={styles.blockedUsersModalContainer}>
              <View style={styles.blockedUsersModalTopFlex}>
                <View style={{flex:1}}>
                </View>
                <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                  <Text style={styles.blockedUsersModalTitleText}>Blocked Users</Text>
                </View>
                <TouchableOpacity onPress={() => this.setState({isOpenedBlockedUsersModal:false})} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Image style={{height:13,width:13}} source={Images.exit} />
                </TouchableOpacity>
              </View>
              
              <View style={styles.blockedUsersContent}>
                {ProfilSettingsStore.blockedUsers.length > 0 ? 
                  <ListView
                    style={{marginTop:10}}
                    enableEmptySections={true}
                    dataSource={ProfilSettingsStore.blockedUsersDataSource}
                    renderRow={this.renderBlockedUsers.bind(this)}
                  />
                
                : 
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>

                  <Text style={{marginBottom:30,fontSize:17,fontWeight:'600',color:'gray'}}>No one is blocked</Text>
                </View>
                }
                <View style={{height:10}} ></View>
              </View>
            </View>
          </Modal>
          <View style={{flex:1,flexDirection:'row',backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
            <Image style={{position:'absolute',left:0,top:0,height:400,width:windowSize.width,resizeMode:'stretch'}} source={Images.backgroundOfProfilImage} />
            <View style={{flex:1}}>
              <View style={{flex:1,flexDirection:'row'}}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={{flex:2,justifyContent:'flex-start',alignItems:'center'}}>
                  <Image style={{height:33,width:33,marginTop:35}} source={Images.leftArrow} />
                </TouchableOpacity>
                <View style={{flex:1}}></View>
              </View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={() => this.openLibrary()} style={styles.profilButtons}>
                <Image style={{height:30,width:30}} source={Images.camera} />
              </TouchableOpacity>
              </View>
              
              
            </View>
            <View style={{flex:2,alignItems:'center'}}>
              
              <View>
                {this.state.changedPhoto == "" ?
                  this.renderPhoto()
               :
                <Image style={styles.profilImagePlaceImage} source={this.state.changedPhoto} />
               }
              </View>
               
            </View>

            <View style={{flex:1}}>
              <View style={{flex:1}}></View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={() => this.save()} style={styles.profilButtons}>
                 <Image style={{height:30,width:30}} source={Images.save} />
                </TouchableOpacity>
              </View>
            </View>
            
           
          </View>
          <View style={{flex:2,backgroundColor:'white'}}>
          <KeyboardAwareScrollView>
            <View style={styles.placeSettingRow}>
              <View style={styles.rowTitle}><Text>Name</Text></View>
              <View style={styles.rowContent}>
                <TextInput
                style={styles.textinput}
                onChangeText={(value) => this.setState({changedName:value})}
                defaultValue={UserData.userProfileData.name}/>
              </View>
            </View>
            <View style={styles.placeSettingRow}>
              <View style={styles.rowTitle}><Text>Nick</Text></View>
              <View style={styles.rowContent}>
                <TextInput
                style={styles.textinput}
                onChangeText={(value) => this.setState({changedUsername:value})}
                defaultValue={UserData.userProfileData.username}/>
              </View>
            </View>
            <View style={styles.placeSettingRow}>
              <View style={styles.rowTitle}><Text>Show Message</Text></View>
              <View style={styles.switchPlace}>
                <Switch onSyncPress={(value) => this.changedSwitch(value)} value={ProfileStories.showMessage}/>
              </View>
            </View>
            <View style={styles.placeSettingRow}>
              <View style={styles.rowTitle}><Text>Blocked People</Text></View>
              <TouchableOpacity onPress={() => this.setState({isOpenedBlockedUsersModal: true})} style={styles.blockedPlace}>
                <Image style={styles.listImage} source={Images.list}/>
              </TouchableOpacity>
            </View>
            <View style={styles.placeSettingRowBottom}>
              <View style={styles.rowTitle}><Text>Logout</Text></View>
              {this.state.fbLogin ? 
              <View style={styles.logoutPlace}>
                <FBLogin 
                  buttonView = {<FBLogoutView />}
                  loginBehavior={FBLoginManager.LoginBehaviors.Native}
                  onLogout={this.fbLogout.bind(this)}
                >
                </FBLogin>
              </View>
              : 
              <TouchableOpacity onPress={() => this.logout()} style={styles.logoutPlace}>
                  <Image style={styles.logoutImage} source={Images.logout}/>
                </TouchableOpacity>
              }
            </View>
            </KeyboardAwareScrollView>
          </View>
          <OrientationLoadingOverlay
            visible={this.state.loadingOverlay}
            color="white"
            indicatorSize="large"
            messageFontSize={24}
            message="Settings are changing. Please wait.."
          />
          <Toast
            visible={this.state.visibleToast === 1}
            position={windowSize.height-150}
            shadow={true}
            animation={true}
            hideOnPress={true}
          >{this.state.toastMessage}</Toast>
        </View>
      );
    }
  }
}
class FBLogoutView extends Component {
  static contextTypes = {
    isLoggedIn: React.PropTypes.bool,
    isLoggedWithFB:React.PropTypes.bool,
    login: React.PropTypes.func,
    logout: React.PropTypes.func,
    props: React.PropTypes.object,
  }
  constructor(props){
    super(props)
  }
  render(){
    return(
      <Button style={{flexDirection:'row',backgroundColor:'transparent',width:50}}
       onPress={() => { this.context.logout() }} >
        <Image style={styles.logoutImage} source={Images.logout} />
      </Button>
    )
  }
}
const styles = StyleSheet.create({
  container: {flex: 1,backgroundColor: '#F5FCFF',},
  profilImagePlaceView:{position:'absolute',left:(windowSize.width-150)/2,bottom:70,top:20,backgroundColor:'#fff',height:150,width:150,borderRadius:75,justifyContent:'center',alignItems:'center'},
  profilImagePlaceImage:{height:146,width:146,borderRadius:73,resizeMode:'cover'},
  profilButtons:{justifyContent:'center',alignItems:'center',backgroundColor:'#ecf0f1',height:50,width:50,borderRadius:25,shadowColor: '#34495e',shadowOffset: {width: 1,height: 1.5},shadowRadius: 1,shadowOpacity: 0.5},
  profilButtonsLeftBottom:{position:'absolute',left:30,bottom:20,},
  profilButtonsRightBottom:{position:'absolute',right:30,bottom:20,},
  placeSettingRow:{height:50,backgroundColor:'white',borderTopWidth:1,borderColor:'#a0a0a0',flexDirection:'row'},
  placeSettingRowBottom:{height:50,backgroundColor:'white',borderTopWidth:1,borderColor:'#a0a0a0',borderBottomWidth:0.5,flexDirection:'row'},
  rowTitle:{justifyContent:'center',alignItems:'center',padding:10,backgroundColor:'white'},rowContent:{justifyContent:'center',alignItems:'center',},
  textinput:{height:30,width:250,color:'#FFA500'},
  switchPlace:{flex:1,padding:10,justifyContent:'center',alignItems:'flex-end'},
  blockedPlace:{flex:1,padding:10,justifyContent:'center',alignItems:'flex-end'},
  logoutPlace:{flex:1,padding:10,justifyContent:'center',alignItems:'flex-end'},
  listImage:{height:26,width:26},
  logoutImage:{height:26,width:26},

  blockedUsersView:{position:'absolute',justifyContent:'center',alignItems:'center',right:25,bottom:75,height:50,width:50,borderRadius:25,backgroundColor:'#27ae60'},
  blockedUsersModalContainer:{borderRadius:5,height:300,width:windowSize.width-40,backgroundColor:'white',justifyContent:'center',alignItems:'center',flexDirection:'column'},
  blockedUsersModalTopFlex:{flexDirection:'row',flex:1,width:windowSize.width-40,backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center'},
  blockedUsersTitleText:{color:'#fff',fontWeight:'bold',fontSize:18},
  blockedUsersContent:{flex:5,width:windowSize.width-40},
  blockedUsersModalTitleText:{color:'#fff',fontWeight:'bold',fontSize:18},

  
});
