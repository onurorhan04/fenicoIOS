import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
  Alert,
  TouchableOpacity,
  TextInput,
  ListView,
  KeyboardAvoidingView,
  Keyboard,
  Animated
} from 'react-native';
import Images from '../../images/images';
import Footer from '../../components/footer'
import KeepToken from '../../stores/keeptoken';
import UserData from '../../stores/userData';
import MagnetStore from '../../stores/magnetStore';
import NotificationStore from '../../stores/notificationStore';
import TestStore from '../../stores/testStore';
import DB from '../../realmDB/databases';
import Toast from '../../components/toast';
import Modal from 'react-native-modal'
import { observer } from 'mobx-react/native';
import MapView,{Marker,Callout} from 'react-native-maps';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
var {GooglePlacesAutocomplete} = require('react-native-google-places-autocomplete');
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import ModalBox from 'react-native-modalbox'
var Firebase  = require('firebase');
import moment from 'moment'
var { width, height } = Dimensions.get('window')
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const AnimatedTI = Animated.createAnimatedComponent(TextInput)
@observer
export default class MagnetMain extends Component {
  constructor(props) {
    super(props);
    this.state={
      isLoaded:false,
      allMagnets:[],
      instantMarker:[],
      subscribeMarker:[],
      instantSubscribeMarker:[],
      isLoadedMagnets:false,
      testLocation:{
        latitude: 41.214332,
        longitude: 28.909869,
      },
      isActivePin:false,
      isActiveLocation:false,
      isActiveSubcribe:false,

      isOpenedInstantMarkerModal:false,
      isOpenedSubscribeMarkerModal:false,
      isOpenedAllMarkerCommentModal:false,
      createSubscribeModal:false,
      deleteSubscribeModal:false,
      reportingModal:false,
      text:"",
      reportingText:"",
      reportingType:"",
      reportingCommentArray:[],
      wishListText: "",
      visibleToast: false,
      toastMessage: "",
      modalSearch: false,
      tester: false,
      intervalID: '',

      dataSourceComment: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      allMarkerCommentModalArray:[{markerID:"",markerTitle:"",markerKey:"",markerUserID:"",markerName:"",markerImage:""}],
      markerModalTitleFont: new Animated.Value(20),
      markerModalContenFont: new Animated.Value(14),
      markerModalSendFont: new Animated.Value(17),
      markerModalTextInputHeight: new Animated.Value(35),
      markerModalTextInputFont: new Animated.Value(16),
      markerModalProfilePhotoSize: new Animated.Value(40),
      markerModalProfileNameFont: new Animated.Value(15),
      markerModalIconSize: new Animated.Value(19),

      markerModalSizesChanged: false
      
    }
    this.locationsRef = this.getRef().child('Magnets');
    this.subscribeRef = this.getRef().child('Subscribe').child(KeepToken.userID);
    this.handlePress = this.handlePress.bind(this);

  }

  animateMarkerModal(){
    this.setState({ markerModalSizesChanged: true })
    let duration = 100
    Animated.parallel([
      Animated.timing(
        this.state.markerModalTitleFont,
        {
          toValue:14,
          duration:duration
        }
      ),
      Animated.timing(
        this.state.markerModalIconSize,
        {
          toValue:14,
          duration:duration
        }
      ),
      Animated.timing(
        this.state.markerModalProfilePhotoSize,
        {
          toValue:30,
          duration:duration
        }
      ),
      Animated.timing(
        this.state.markerModalContenFont,
        {
          toValue:11,
          duration:duration
        }
      ),
      Animated.timing(
        this.state.markerModalSendFont,
        {
          toValue:15,
          duration:duration
        }
      ),
      Animated.timing(
        this.state.markerModalTextInputHeight,
        {
          toValue:30,
          duration:duration
        }
      ),
      Animated.timing(
        this.state.markerModalTextInputFont,
        {
          toValue:13,
          duration:duration
        }
      ),

    ]).start()
  }
  removeAnimateMarkerModal(){

    if(this.state.markerModalSizesChanged){
      let duration = 100
      this.setState({ markerModalSizesChanged: false })
      Animated.parallel([
        Animated.timing(
          this.state.markerModalTitleFont,
          {
            toValue:20,
            duration:duration
          }
        ),
        Animated.timing(
          this.state.markerModalIconSize,
          {
            toValue:20,
            duration:duration
          }
        ),
        Animated.timing(
          this.state.markerModalProfilePhotoSize,
          {
            toValue:40,
            duration:duration
          }
        ),
        Animated.timing(
          this.state.markerModalContenFont,
          {
            toValue:14,
            duration:duration
          }
        ),
        Animated.timing(
          this.state.markerModalSendFont,
          {
            toValue:17,
            duration:duration
          }
        ),
        Animated.timing(
          this.state.markerModalTextInputHeight,
          {
            toValue:35,
            duration:duration
          }
        ),
        Animated.timing(
          this.state.markerModalTextInputFont,
          {
            toValue:16,
            duration:duration
          }
        ),
      ]).start()
    }

  }
  getRef() {
    return Firebase.database().ref();
  }
  fetchUserAllData(){
    fetch("https://fenicoapp.com/api/v1/user/view?id="+ KeepToken.userID +"&expand=profile,userWishlists,userStories", {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      //console.log(responseData)
      UserData.data = responseData
      UserData.storyData = responseData.userStories
      reversedStoryData = UserData.storyData.reverse()
      UserData.storyData = reversedStoryData
     
      UserData.wishListData = responseData.userWishlists
      UserData.location = responseData.profile.location
      this.fetchUserCountriesData()
    })
  }
  fetchUserCountriesData(){
    fetch("https://fenicoapp.com/api/v1/user/countries?id="+KeepToken.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      UserData.countryData = responseData.data
      this.setState({isLoaded:true})
    })
  }
  listenForNotification(){
    //WILL BE ADDED MAGNET NOTIFICATIONS
    const messageNotificationRef = Firebase.database().ref().child("Notifications").child("MessageType").child(KeepToken.userID)
    messageNotificationRef.on('value', (snap) => {
      var messageNotifArray = []
      snap.forEach((child) => {
        var messageItems = {key:{},data:[]};
        child.forEach((data) => {
          messageItems.key = child.key
          messageItems.data.push({
            dateTime:data.val().dateTime,
            from:data.val().from,
            fromName:data.val().fromName,
            text:data.val().text,
            to:data.val().to,
            notifType:data.val().notifType,
            key:child.key
          });
        });
        const changeContactNotifCount = Firebase.database().ref().child("Contact").child(KeepToken.userID).child(messageItems.key).child('notifCount')
        changeContactNotifCount.update({notifCount:messageItems.data.length})
        
        console.log('messageItemsDataLength: ',messageItems.data.length, messageItems.key )
        messageNotifArray.push(messageItems)
        
      }); 
      //console.log(messageNotifArray[0])
      NotificationStore.messageNotifData = messageNotifArray
      //NotificationStore.preparedNotificationData = NotificationStore.messageNotifData.concat(NotificationStore.commentNotifData)
      
      NotificationStore.notificationCount = NotificationStore.preparedNotificationData.length
    })
    //COMMENT TYPE
    NotificationStore.commentNotifData = []
    NotificationStore.commentNotificationKey = []
    NotificationStore.preparedNotificationData = []
    NotificationStore.notificationCount = 0

    let commentNotificationKey = []
    const commentNotificationRef = Firebase.database().ref().child("Notifications").child("CommentType").child(KeepToken.userID)
    commentNotificationRef.on('value', (snap) => {
      let commentItems = [];
      snap.forEach((data) => {
        commentItems.push({
          commentFrom : data.val().commentFrom,
          commentText : data.val().commentText,
          date : data.val().date,
          magnetKey : data.val().magnetKey,
          magnetLatitude : data.val().magnetLatitude,
          magnetLongitude : data.val().magnetLongitude,
          magnetTitle : data.val().magnetTitle,
          notifType : data.val().notifType,
          key: data.key
        });
        commentNotificationKey.push({key:data.key})
      });
      NotificationStore.commentNotifData = commentItems
      NotificationStore.commentNotificationKey = commentNotificationKey
      //NotificationStore.preparedNotificationData = NotificationStore.commentNotifData.concat(NotificationStore.messageNotifData)
      NotificationStore.preparedNotificationData = NotificationStore.commentNotifData
      
      NotificationStore.notificationCount = NotificationStore.preparedNotificationData.length
    })
  }
  listenForItems(locationsRef) {
    //let specific = moment().utcOffset(60).format('YYYY-MM-DD HH:mm')
    //23.00
    //Alert.alert(specific.toString())
    //let oldTime = 'Sat Sep 23 2017 00:16:00 GMT+0300'
    //let newTime = moment();
    //let difference = moment(moment(oldTime).diff(newTime)).format('H')
    //let difference = newTime.diff(oldTime,'minutes')
    //Alert.alert(difference.toString(),oldTime.toString() + newTime.toString())
    /*let magnetTime = '1506088882999'
    let currentTime = Firebase.database.ServerValue.TIMESTAMP.getTime()
    var diffTest = currentTime - magnetTime;
    var msecTest = diffTest;
    var hhTest = Math.floor(msecTest / 1000 / 60 / 60);
    
    console.log('----')
    console.log(hhTest)*/

    locationsRef.on('value', (snap) => {
      //locationsRef.push({serverTime: Firebase.database.ServerValue.TIMESTAMP})
      //const userRef = Firebase.database().ref().child('Users').child(KeepToken.userID)
        Firebase.database().ref('/Users/' + KeepToken.userID).once('value').then(function(snapshot) {
          const serverTimeData = (snapshot.val() && snapshot.val().serverTime)
          //this.setState({serverTime: serverTimeData})
          MagnetStore.serverTime = serverTimeData
          //console.log(serverTimeData)
          const userRef = Firebase.database().ref().child('Users').child(KeepToken.userID)
          userRef.update({serverTime: Firebase.database.ServerValue.TIMESTAMP})
          // ...
        }.bind(this));
      
         var items = [];
         snap.forEach((child) => {
            /*var dateMagnetTime = new Date(child.val().dateCreated);
            var dateCurrentTime = new Date(this.state.serverTime);
            var diff = dateCurrentTime.getTime() - dateMagnetTime.getTime(); */
            var diff = MagnetStore.serverTime - child.val().dateCreated
            var msec = diff;
            var hh = Math.floor(msec / 1000 / 60 / 60);
            //console.log('servertime: ', MagnetStore.serverTime)
            //console.log('dateCreated: ', child.val().dateCreated)
            
            //console.log(hh, child.val().title)
            //console.log(hh, child.val().title, child.val().status)
            if(hh < 3 && child.val().status == 'active'){
              if(child.val().id == KeepToken.userID){
                //OLUŞTURULDUGUNDA INIT COMMENT ATIYORUM.. NOTIFICATION ICIN
                let coordinate = { latitude:child.val().latitude,longitude:child.val().longitude }
                this.checkCommentData(child.key,'A magnet was created',child.val().title,coordinate)
                MagnetStore.myKey = child.key
              }

              const magnetData = {
                id: child.val().id,
                coordinate:{latitude:child.val().latitude,longitude:child.val().longitude},
                title:child.val().title,
                userID:child.val().userID,
                name:child.val().name,
                image:child.val().image,
                time:child.val().time,
                status:child.val().status,
                dateCreated:child.val().dateCreated,
                _key: child.key
              }
              /*
              items.push({
                id: child.val().id,
                coordinate:{latitude:child.val().latitude,longitude:child.val().longitude},
                title:child.val().title,
                userID:child.val().userID,
                name:child.val().name,
                image:child.val().image,
                time:child.val().time,
                status:child.val().status,
                dateCreated:child.val().dateCreated,
                _key: child.key
              });
              */
              let isGotMagnet = MagnetStore.allMagnets.findIndex( i => i.userID == magnetData.userID )
              if(isGotMagnet == -1){
                  MagnetStore.allMagnets.push(magnetData)
              }
            }else{
              const magnetRef = Firebase.database().ref().child('Magnets').child(child.key.toString())
              magnetRef.update({status: 'passive'})

              let isGotMagnet = MagnetStore.allMagnets.findIndex( i => i.userID == child.val().userID )
              if( isGotMagnet == -1 ){
                MagnetStore.allMagnets.splice(isGotMagnet)
                if( child.val().userID == KeepToken.userID ){
                  MagnetStore.myKey = ""
                }
              }
            }
            //MagnetStore.allMagnets = items
          })
        
      this.setState({
        isLoadedMagnets:true
      });
    })
  }
  listenForSubscribe(subscribeRef){
    subscribeRef.on('value',(snap) => {
      var items = []
      snap.forEach((child) => {
        items.push({
          id:child.val().id,
          coordinate:{latitude:child.val().latitude,longitude:child.val().longitude}
        })
      })
      this.setState({subscribeMarker:items})
    })
  }
  createMagnet(){
    const { text } = this.state
    if(text != ""){

      if(MagnetStore.myKey != ""){
          const deleteMagnetRef = Firebase.database().ref().child('Magnets').child(MagnetStore.myKey)
          deleteMagnetRef.remove()
      }
      
      //AUTO SUBSCRIBER PLACES
      const subscribeRef = Firebase.database().ref().child('Subscribe').child(KeepToken.userID)
      subscribeRef.remove()
      subscribeRef.push({
        id:KeepToken.userID,
        latitude:this.state.instantMarker[0].coordinate.latitude,
        longitude:this.state.instantMarker[0].coordinate.longitude
      })
      
        let image = ""
        if(UserData.data.profile.image !== null){
          image = UserData.data.profile.image
        }else{
          image = ""
        }

        const pushLocationRef = Firebase.database().ref().child('Magnets')
        pushLocationRef.push({
          id:this.state.instantMarker[0].id,
          latitude:this.state.instantMarker[0].coordinate.latitude,
          longitude:this.state.instantMarker[0].coordinate.longitude,
          title:text,
          userID:UserData.data.profile.user_id,
          name:UserData.data.profile.name,
          image:image,
          status:'active',
          time:moment().utcOffset(60).format('YYYY-MM-DD HH:mm'),
          dateCreated:Firebase.database.ServerValue.TIMESTAMP,
          type:'magnet'
        });
        this.setState({ isOpenedInstantMarkerModal: false , instantMarker: [], text: "", isActivePin: false })
        MagnetStore.magnetPinBG = '#FFF'
        
    }
    
  }
  checkCommentData(markerKey,commentText,magnetTitle,magnetCoordinate){
    let commentedData = DB.objects('CommentedData');
    let checker = 0
    if(commentedData.length > 0){
      for(let i=0; i<commentedData.length; i++){
        if(commentedData[i].magnetKey == markerKey && commentedData[i].userID == KeepToken.userID){
          break;
        }
        if(i == commentedData.length-1 && checker == 0){
          const commented = Firebase.database().ref().child("Commented")
          commented.push({userID:KeepToken.userID,username:KeepToken.username,commentText:commentText,magnetTitle:magnetTitle,magnetKey:markerKey,coordinate:magnetCoordinate})
          DB.write(() => {
            let fcmc = DB.create('CommentedData',
               {
                 userID: KeepToken.userID.toString(),
                 magnetKey: markerKey.toString()
               }
             );
          });
        }
      }
    }else{
      const commented = Firebase.database().ref().child("Commented")
      commented.push({userID:KeepToken.userID,username:KeepToken.username,commentText:commentText,magnetTitle:magnetTitle,magnetKey:markerKey,coordinate:magnetCoordinate})
      DB.write(() => {
        let fcmc = DB.create('CommentedData',
           {
             userID: KeepToken.userID.toString(),
             magnetKey: markerKey.toString()
           }
         );
      });
    }


  }
  addComment(markerKey,commentText,fromID,ai_key){
    var image = ""
    if(UserData.data.profile.image != null){
      var image = "https://fenicoapp.com/api/web/uploads/" + UserData.data.profile.image
    }
    const commentRef = Firebase.database().ref().child("Magnets").child(markerKey).child("comments");
    commentRef.push({ai_key:ai_key,comment:commentText,from:fromID,fromName:UserData.data.profile.name,image:image})
    this.textInputComment.setNativeProps({text: ''});
    this.checkCommentData(markerKey,commentText,this.state.allMarkerCommentModalArray[0].markerTitle,this.state.allMarkerCommentModalArray[0].markerCoordinate)
  }
  handlePress(e){
    if(this.state.isActiveSubcribe){
      if(this.state.subscribeMarker.length > 0 ){
        this.setState({deleteSubscribeModal:true})
      }else{
        this.setState({
          instantSubscribeMarker:[
            {
              id:KeepToken.userID,
              coordinate: {
                latitude: e.nativeEvent.coordinate.latitude,
                longitude: e.nativeEvent.coordinate.longitude,
              }
            }
          ]
        })
      }
    }
    if(this.state.isActivePin){
      this.setState({
        instantMarker:[
          {
            id:KeepToken.userID,
            coordinate: {
              latitude: e.nativeEvent.coordinate.latitude,
              longitude: e.nativeEvent.coordinate.longitude,
            },
            title:'',
            description:''
          },
        ]
      })
    }
  }
  resetButtonBG(){
    MagnetStore.magnetPinBG = '#FFF'
    MagnetStore.magnetLocationBG = '#FFF'
    MagnetStore.magnetSubscribeBG = '#FFF'
  }
  
  updateServerTime(){
    var userRef = Firebase.database().ref().child('Users').child(KeepToken.userID)
      userRef.update({serverTime: Firebase.database.ServerValue.TIMESTAMP})
      Firebase.database().ref('/Users/' + KeepToken.userID).once('value').then(function(snapshot) {
        const serverTimeData = (snapshot.val() && snapshot.val().serverTime)
        MagnetStore.serverTime = serverTimeData
        //this.setState({serverTime: serverTimeData})
        //console.log(serverTimeData)
      }.bind(this));

     this.setIntervalTimeData = setInterval(function(){ 
      var userRef = Firebase.database().ref().child('Users').child(KeepToken.userID)
      userRef.update({serverTime: Firebase.database.ServerValue.TIMESTAMP})
      Firebase.database().ref('/Users/' + KeepToken.userID).once('value').then(function(snapshot) {
        const serverTimeData = (snapshot.val() && snapshot.val().serverTime)
        //this.setState({serverTime: serverTimeData})
        MagnetStore.serverTime = serverTimeData
        //console.log(serverTimeData)
      }.bind(this));
    }.bind(this), 50000); 
    
  }
  
  componentDidMount(){
    const currentPlaceDB = DB.objects('CurrentPlaceSchema')
    const paramsType = typeof this.props.navigation.state.params
    if(paramsType !== "undefined"){
      //this.animateTo(this.props.navigation.state.params.latitudeFromNotif,this.props.navigation.state.params.longitudeFromNotif)
      this.extractMagnetDataAndOpenModalForNotification(this.props.navigation.state.params.magnetKey)
      MagnetStore.currentRegion = {
        latitude: parseFloat( this.props.navigation.state.params.latitudeFromNotif ),
        longitude: parseFloat( this.props.navigation.state.params.longitudeFromNotif ) ,
        latitudeDelta: parseFloat( 0.07 ),
        longitudeDelta: parseFloat( 0.07 )
      }
    }else{
      if(currentPlaceDB.length > 0 ){
        //this.animateTo(currentPlaceDB[0].latitude,currentPlaceDB[0].longitude)
        MagnetStore.currentRegion = {
          latitude: parseFloat( currentPlaceDB[0].latitude ),
          longitude: parseFloat( currentPlaceDB[0].longitude ),
          latitudeDelta: parseFloat( 0.07 ),
          longitudeDelta: parseFloat( 0.07 )
        }
      }
    }
    this.listenTray()
    
    
  }
  routingToRoom(contactUserID,contactUsername,contactImage){
    this.props.navigation.navigate('room',{contactUserID:contactUserID,contactUsername:contactUsername,contactImage:contactImage});
  }
  fcmRegistiration(){
    FCM.requestPermissions();
    FCM.getFCMToken();      
  }
  
  listenTray(){
    FCM.getInitialNotification().then(notif => {
      if(typeof notif !== undefined && typeof notif !== "undefined"){
        if(notif.aps.category == "fcm.ACTION.COMMENT"){
          MagnetStore.currentRegion = {
            latitude: parseFloat( notif.magnetLatitude ),
            longitude: parseFloat( notif.magnetLongitude ),
            latitudeDelta: parseFloat( 0.07 ),
            longitudeDelta: parseFloat( 0.07 )
          }
          this.extractMagnetDataAndOpenModalForNotification(notif.magnetKey)
        }
        if(notif.aps.category == "fcm.ACTION.MAGNET"){
          MagnetStore.currentRegion = {
            latitude: parseFloat( notif.magnetLatitude ),
            longitude: parseFloat( notif.magnetLongitude ),
            latitudeDelta: parseFloat( 0.07 ),
            longitudeDelta: parseFloat( 0.07 )
          }
        }
        if(notif.aps.category == "fcm.ACTION.MESSAGE"){
          
          this.routingToRoom(notif.fromID,notif.fromName,notif.fromImage)
        }

        //Alert.alert(notif.aps.category,JSON.stringify(notif))
      }
    });
    /*FCM.getInitialNotification().then((notif)=>{
      Alert.alert('',JSON.stringify(notif))
    })*/
  }
  extractMagnetDataAndOpenModalForNotification(magnetKey){
    const magnetRef = Firebase.database().ref().child("Magnets").child(magnetKey)
    magnetRef.once('value', (snap) => {
        //Alert.alert('',JSON.stringify(child.val()))
        if( snap.val().status !== "passive" ){
          const coordinate = {
            latitude: snap.val().latitude,
            longitude: snap.val().longitude,
          }
          this.openAllMarkerCommentModal(
            snap.val().id,
            snap.val().title,
            magnetKey,
            snap.val().userID,
            snap.val().name,
            snap.val().image,
            coordinate
          )
        }else{
          this.setState({ toastMessage: "Magnet has expired..", visibleToast: true })
          setTimeout(() => { this.setState({ visibleToast: false }) }, 4000);
        }
    })

  }
  componentWillMount(){
    //console.log(moment('10/09/17 08:09:02').utcOffset(60).format('YYYY-MM-DD HH:mm'))
    //Alert.alert(KeepToken.userID.toString())
    
    this.updateServerTime()
    console.disableYellowBox = true;
    this.resetButtonBG()
    this.fetchUserAllData()
    this.listenForNotification()
    this.listenForItems(this.locationsRef)
    //this.listenForSubscribe(this.subscribeRef)
    this.fcmRegistiration()
    
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }
  _keyboardDidShow = () => {
    this.setState({isKeyboard:true})
    if(this.state.isOpenedAllMarkerCommentModal){
      this.animateMarkerModal()
    }
  }

  _keyboardDidHide = () => {
    this.setState({isKeyboard:false})
    this.removeAnimateMarkerModal()

  }
  
  
  activateLocation(){
      this.updateLocation()
  }
  activatePin(){
    if(MagnetStore.magnetPinBG == '#FFF'){
      MagnetStore.magnetPinBG = '#27ae60'
      MagnetStore.magnetLocationBG = '#FFF'
      MagnetStore.magnetSubscribeBG = '#FFF' 
      this.setState({isActivePin:true,isActiveSubcribe:false,isActiveLocation:false})
    }else{
      MagnetStore.magnetPinBG = '#FFF'
      this.setState({ isActivePin: false, instantMarker: [] })
    }
    
  }
  
  updateLocation(){
    navigator.geolocation.getCurrentPosition(
      (position) => {
        MagnetStore.currentRegion = {
          latitude: parseFloat( position.coords.latitude ),
          longitude: parseFloat( position.coords.longitude ),
          latitudeDelta: parseFloat( 0.07 ),
          longitudeDelta: parseFloat( 0.07 )
        }
        let currentPlaceDB = DB.objects('CurrentPlaceSchema')
        DB.write(() => {
          DB.delete(currentPlaceDB)
          DB.create('CurrentPlaceSchema',{
            latitude:position.coords.latitude.toString(),
            longitude:position.coords.longitude.toString()
          })
        })
      },
      (error) =>{
        //console.log(error)
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }
  createSubscribe(){
    const subscribeRef = Firebase.database().ref().child('Subscribe').child(KeepToken.userID)
    subscribeRef.push({id:KeepToken.userID,latitude:this.state.instantSubscribeMarker[0].coordinate.latitude,longitude:this.state.instantSubscribeMarker[0].coordinate.longitude})
    this.setState({createSubscribeModal:false,instantSubscribeMarker:[]})
  }
  createSubscribeModal(){
    if(this.state.instantSubscribeMarker.length > 0){
      this.setState({createSubscribeModal:true})
    }
  }
  deleteSubscribe(){
    const subscribeRef = Firebase.database().ref().child('Subscribe').child(KeepToken.userID)
    subscribeRef.remove()
    this.setState({deleteSubscribeModal:false})
  }
  deleteSubscribeModal(){
    this.setState({deleteSubscribeModal:true})
  }
  openInstantMarkerModal(){
    var markerModalWarningText = false
    if(MagnetStore.myKey !== ""){
      markerModalWarningText = true
    }
    this.setState({
      isOpenedInstantMarkerModal:true,
      markerModalWarningText: markerModalWarningText
    })
  }
  openAllMarkerCommentModal(markerID,markerTitle,markerKey,markerUserID,markerName,markerImage,markerCoordinate){
    this.setState({
      isOpenedAllMarkerCommentModal:true,
      allMarkerCommentModalArray:[{
        markerID:markerID,
        markerTitle:markerTitle,
        markerKey:markerKey,
        markerUserID:markerUserID,
        markerName:markerName,
        markerImage:markerImage,
        markerCoordinate:markerCoordinate}]
    })
    //REMOVING NOTIFICATION
    
    let commentKeys = NotificationStore.commentNotificationKey
    for(let i=0; i < commentKeys.length; i++){
      const removeNotification = Firebase.database().ref().child("Notifications").child('CommentType').child(KeepToken.userID).child(commentKeys[i].key);
      removeNotification.once('value', (snap) => {
        if(snap.numChildren() > 0 ){
          if(markerKey == snap.val().magnetKey){
              removeNotification.remove()
          }
        }
      });
    }
    

    this.listenForComments(markerKey)
  }
  listenForComments(key){
    var commentsRef = Firebase.database().ref().child("Magnets").child(key).child("comments").orderByChild("ai_key");
    commentsRef.on('value', (snap) => {
      MagnetStore.ai_key = snap.numChildren()
      var items = [];
      snap.forEach((child) => {
        items.push({
          ai_key:child.val().ai_key,
          comment: child.val().comment,
          from:child.val().from,
          fromName:child.val().fromName,
          image:child.val().image,
          _key: child.key
        });
      });
      
      /*
      let sortedItems = items.sort((a,b) => {
      if (a.ai_key > b.ai_key) {
          return -1;
        }
        if (a.ai_key < b.ai_key) {
          return 1;
        }
        return 0;
      });
      */
      this.setState({
        dataSourceComment: this.state.dataSourceComment.cloneWithRows(items)
      });
    })
  }
  selectedSearchPlace(data, details){
    MagnetStore.searchModal = false
    //this.refs.modalSearch.close()
    MagnetStore.currentRegion = {
      latitude: parseFloat( details.geometry.location.lat ),
      longitude: parseFloat( details.geometry.location.lng ),
      latitudeDelta: parseFloat( 0.07 ),
      longitudeDelta: parseFloat( 0.07 )
    }
    MagnetStore.searchedRegionName = data.description
    MagnetStore.searchPlaceZIndex = 9999
  }
  openSearchModal(){
    MagnetStore.searchPlaceZIndex = -1
    MagnetStore.searchModal = true
   // this.refs.modalSearch.open()
  }
  modalSearchClosed(){
    MagnetStore.searchPlaceZIndex = 9999
    MagnetStore.searchModal = false
   // this.refs.modalSearch.open()
  }

  reporting(){
    if(this.state.reportingType === 'magnet'){
      const { markerKey } = this.state.allMarkerCommentModalArray[0]
      const reportRef = Firebase.database().ref().child("Reports").child('Magnets').child(markerKey);
      reportRef.push({fromID: KeepToken.userID, fromName: KeepToken.username, reportingText: this.state.reportingText, markerKey: markerKey})
      this.setState({reportingModal: false, reportingType: '', reportingText: ''})
    }else{
      const { markerKey } = this.state.allMarkerCommentModalArray[0]
      const { comment, from, fromName, _key } = this.state.reportingCommentArray
      const reportRef = Firebase.database().ref().child("Reports").child('Comments').child(markerKey);
      reportRef.push({fromID: from, fromName: fromName, comment: comment, reportingText: this.state.reportingText, markerKey: markerKey})
      this.setState({reportingModal: false, reportingCommentArray: [], reportingType: '', reportingText: ''})
      
    }
    
  }
  handleEditComplete(){
    //Alert.alert('clicked to textinput')
  }
  setCommentArrayForReporting(data){
    this.setState({reportingCommentArray: data, reportingModal: true, reportingType: 'comment'})
    //Alert.alert('',JSON.stringify(this.state.reportingCommentArray))
  }
  routeToProfile(userID,userName){
    this.setState({isOpenedAllMarkerCommentModal: false})
    if(KeepToken.userID == userID){
      this.props.navigation.navigate('profilMain')
    }else{
      this.props.navigation.navigate('profilOther',{userID:userID,username:userName});
    }
  }
  animateTo(latitude, longitude) {
     this.mapRef.animateToCoordinate({ latitude: latitude, longitude: longitude})
  }
  renderComments(rowData){
    return(
      <View style={{width:width-50,borderBottomWidth:0.2,borderColor:'#A0A0A0'}}>
        <View style={{flexDirection:'row',width:width-50,padding:10}}>
          <View style={{flex:1}}>
            <TouchableOpacity onPress={() => this.routeToProfile(rowData.from,rowData.fromName) } style={{height:50,width:50}}>
              {rowData.image.length > 30 ?
                <Image style={{height:40,width:40}} source={{uri: rowData.image}} />
              :
                <Image style={{height:40,width:40}} source={Images.noImage} />
              }
            </TouchableOpacity>
          </View>
          
          <View style={{flex:5,flexDirection:'column'}}>
            <Text style={{color:'#3498db',fontWeight:'bold'}}>{rowData.fromName} </Text>
            <Text>{rowData.comment}</Text>
          </View>
          <TouchableOpacity onPress={() => this.setCommentArrayForReporting(rowData)} style={{flex:0.7,justifyContent:'flex-start',alignItems:'center'}}>
            <Image style={{height:23,width:20}} source={Images.extraGray} />
          </TouchableOpacity>
       </View>
      </View>
    )
  }
  render() {
    
    if(!this.state.isLoaded){
      return(
         <View style={{flex:1}}></View>
      )
    }else{
      return (
        <View style={styles.container}>

          





          {MagnetStore.searchModal ? 
          <ModalBox
            style={{flex:1,backgroundColor:'#fff',justifyContent:'center',alignItems:'center'}}
            isOpen={MagnetStore.searchModal}
            onClosed={this.modalSearchClosed}
            swipeToClose={true}
          >
          <GooglePlacesAutocomplete
               placeholder='Search'
               minLength={2}
               autoFocus={true}
               returnKeyType={'search'}
               listViewDisplayed='auto'
               fetchDetails={true}
               renderDescription={(row) => row.description}
               onPress={(data, details = null) => {
                 this.selectedSearchPlace(data,details)
               }}
               getDefaultValue={() => {
                 return '';
               }}
               query={{
                 key: 'AIzaSyAScjjcyAcbI8qa7sz8aiCt7tFxB91EX_M',
                 language: 'en',
               }}
               styles={{
                 container:{width:width,height:50,marginTop:20},
                 description: {
                   fontWeight: 'bold',
                 },
                 predefinedPlacesDescription: {
                   color: '#1faadb',
                 },
               }}
               currentLocation={false}
               currentLocationLabel="Current location"
               nearbyPlacesAPI='GooglePlacesSearch'
               GoogleReverseGeocodingQuery={{
               }}
               GooglePlacesSearchQuery={{
                 rankby: 'distance',
                 types: 'food',
               }}
               filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
               debounce={200}
             />
          </ModalBox>
          : 
          null
          }
          
          <Modal isVisible={this.state.isOpenedInstantMarkerModal}>
            <KeyboardAvoidingView keyboardVerticalOffset={20} behavior={"position"}>
            <View style={styles.instantMarkerModalContainer}>
              <View style={styles.instantMarkerModalTopFlex}>
                <Text style={styles.instantMarkerModalTitleText}>Title</Text>
                {MagnetStore.myKey != "" && this.state.markerModalWarningText == true ? 
                  <View>
                    <Text style={styles.instantMarkerModalWarningText}>If you create new magnet,</Text>
                    <Text style={styles.instantMarkerModalWarningText}>your previous magnet will be deleted.</Text>
                  </View>
                : null }
                <View style={styles.textInputInstantMarkerPlace}>
                  <TextInput multiline={true} onChangeText={(text) => this.setState({text:text}) }  style={styles.textInputInstantMarkerTitle} />
                </View>
              </View>
              <View style={styles.instantMarkerModalBottomFlex}>
                
                <TouchableOpacity onPress={() => this.setState({isOpenedInstantMarkerModal:false})} style={styles.instantMarkerModalCancelButtonPlace}>
                  <Text style={styles.instantMarkerModalCancelButtonText}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.createMagnet()} style={styles.instantMarkerModalCreateButtonPlace}>
                  <Text style={styles.instantMarkerModalCreateButtonText}>Create</Text>
                </TouchableOpacity>
              </View>
            </View>
            </KeyboardAvoidingView>
          </Modal>

          <Modal isVisible={this.state.deleteSubscribeModal}>
            <View style={styles.instantMarkerModalContainer}>
              <View style={styles.instantMarkerModalTopFlex}>
                <Text style={styles.instantMarkerModalTitleText}>Do you want to remove existing </Text>
                <Text style={styles.instantMarkerModalTitleText}>subscriber place ?</Text>
              </View>
              <View style={styles.instantMarkerModalBottomFlex}>
                <TouchableOpacity onPress={() => this.deleteSubscribe()} style={styles.instantMarkerModalCreateButtonPlace}>
                  <Text style={styles.instantMarkerModalCreateButtonText}>Delete</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({deleteSubscribeModal:false})} style={styles.instantMarkerModalCancelButtonPlace}>
                  <Text style={styles.instantMarkerModalCreateButtonText}>Cancel</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>

          <Modal isVisible={this.state.createSubscribeModal}>
            <View style={styles.instantMarkerModalContainer}>
              <View style={styles.instantMarkerModalTopFlex}>
              <Text style={styles.instantMarkerModalTitleText}>Do you want to subscriber </Text>
              <Text style={styles.instantMarkerModalTitleText}>this place ?</Text>
              </View>
              <View style={styles.instantMarkerModalBottomFlex}>
                <TouchableOpacity onPress={() => this.createSubscribe()} style={styles.instantMarkerModalCreateButtonPlace}>
                  <Text style={styles.instantMarkerModalCreateButtonText}>Create</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({createSubscribeModal:false})} style={styles.instantMarkerModalCancelButtonPlace}>
                  <Text style={styles.instantMarkerModalCreateButtonText}>Cancel</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>

        <Modal isVisible={this.state.isOpenedAllMarkerCommentModal}>
            <Modal style={{justifyContent:'center',alignItems:'center'}} isVisible={this.state.reportingModal}>
            <KeyboardAvoidingView keyboardVerticalOffset={20} behavior={"position"}>
              
               <View style={{height:250,width:width-50,backgroundColor:'#fff',borderRadius:6}}>
                <View style={{flex:1.2,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <View style={{flex:1}}></View>
                  <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:'#e74c3c',fontSize:16,fontWeight:'bold'}} >Reporting</Text>
                  </View>
                  <TouchableOpacity onPress={() => this.setState({reportingModal: false}) } style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                     <Image style={{height:13,width:13}} source={Images.grayCancel} />
                  </TouchableOpacity>
                  
                </View>
                <View style={{flex:3}}>
                  <TextInput multiline={true} onChangeText={(text) => this.setState({reportingText:text})} placeholder={"Please, describe the problem"} placeholderTextColor={'#A0A0A0'}  style={{paddingHorizontal:10,paddingVertical:10,marginLeft:10,borderWidth:1,borderColor:'#d0d0d0',height:120,width:width-70,fontSize:15}} />
                </View>
                <TouchableOpacity onPress={() => this.reporting()} style={{flex:1.2,flexDirection:'row',backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center',borderBottomLeftRadius:6,borderBottomRightRadius:6}}>
                    <View style={{height:35,justifyContent:'center',alignItems:'center'}}>
                      <Text style={{color:'#fff',fontSize:18}}>Report</Text>
                    </View>
                </TouchableOpacity>
               </View>
            </KeyboardAvoidingView>
               
            </Modal>
            <View style={!this.state.isKeyboard ? styles.allMarkerCommentModalContainer : styles.allMarkerCommentModalContainerWithSizes}>
            <KeyboardAvoidingView keyboardVerticalOffset={20} behavior={"padding"}>
              
              <View style={{flex:0.6,width:width-50,backgroundColor:'#f0a574',flexDirection:'row',justifyContent:'center',alignItems:'center',borderBottomWidth:0.5,borderColor:'#A0A0A0',borderTopLeftRadius:5,borderTopRightRadius:5}}>
                <TouchableOpacity onPress={() => this.setState({reportingModal:true,reportingType:'magnet'})} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Animated.Image style={{height:this.state.markerModalIconSize,width:this.state.markerModalIconSize}} source={Images.extraWhite} />
                </TouchableOpacity>
                <View style={{flex:4,justifyContent:'center',alignItems:'center'}}>
                  <Animated.Text style={{color:'#FFF',fontSize:this.state.markerModalTitleFont,fontWeight:'bold'}}>Magnet</Animated.Text>
                </View>
                <TouchableOpacity onPress={() => this.setState({isOpenedAllMarkerCommentModal:false})} style={{flex:1,height:30,justifyContent:'center',alignItems:'center'}}>
                  <Animated.Image style={{height:this.state.markerModalIconSize,width:this.state.markerModalIconSize}} source={Images.cancel} />
                </TouchableOpacity>
              </View>
              <View style={{flex:5,width:width-50,backgroundColor:'#FFF'}}>
              <View style={{flexDirection:'row',width:width-50,padding:10,borderBottomWidth:0.8,borderColor:'#A0A0A0'}}>
                <TouchableOpacity onPress={() => this.routeToProfile(this.state.allMarkerCommentModalArray[0].markerUserID,this.state.allMarkerCommentModalArray[0].markerName)} style={{height:50,width:50}}>
                 {this.state.allMarkerCommentModalArray[0].markerImage !== null && this.state.allMarkerCommentModalArray[0].markerImage !== undefined && this.state.allMarkerCommentModalArray[0].markerImage.length > 30 ?
                     <Animated.Image style={{height:this.state.markerModalProfilePhotoSize,width:this.state.markerModalProfilePhotoSize}} source={{uri: 'https://fenicoapp.com/api/web/uploads/'+this.state.allMarkerCommentModalArray[0].markerImage}} />
                    :
                     <Animated.Image style={{height:this.state.markerModalProfilePhotoSize,width:this.state.markerModalProfilePhotoSize}} source={Images.noImage} />
                  }
                </TouchableOpacity>
                <View style={{flexDirection:'column',width:width-120}}>
                  <Animated.Text style={{color:'#3498db',fontWeight:'bold',fontSize:this.state.markerModalContenFont}}>{this.state.allMarkerCommentModalArray[0].markerName}</Animated.Text>
                  <Animated.Text style={{fontSize:this.state.markerModalContenFont}}>{this.state.allMarkerCommentModalArray[0].markerTitle}</Animated.Text>
                </View>
                
             </View>
                <ListView
                  dataSource={this.state.dataSourceComment}
                  renderRow={this.renderComments.bind(this)}
                />
              </View>
              <View style={{flex: this.state.isKeyboard ? 0.7 : 0.6 ,width:width-50,backgroundColor:'#FFF',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
               <View style={{marginLeft:5,flex:5,justifyContent:'center',alignItems:'center'}}>
                 <AnimatedTI onSubmitEditing={this.handleEditComplete.bind(this)} style={{height:this.state.markerModalTextInputHeight ,width:width-120,fontSize:this.state.markerModalTextInputFont,borderRadius:12,borderWidth:1,paddingHorizontal:10,borderColor:'#E0E0E0'}} ref={(r) => {this.textInputComment = r;}} onChangeText={(text) => this.setState({text:text})} placeholder={'Write a comment'} />
               </View>
               <TouchableOpacity onPress={() => this.addComment(this.state.allMarkerCommentModalArray[0].markerKey,this.state.text,KeepToken.userID,MagnetStore.ai_key)} style={{flex:1,marginRight:5,justifyContent:'center',alignItems:'center'}}>
                 <Animated.Text style={{fontSize:this.state.markerModalSendFont,color:'#f0a574',fontWeight:'bold'}}>Send</Animated.Text>
               </TouchableOpacity>
              </View>
            </KeyboardAvoidingView>
              
            </View>
          </Modal>

          
          <TouchableOpacity onPress={() => this.openSearchModal()} style={{zIndex:MagnetStore.searchPlaceZIndex,position:'absolute',left:10,top:30,width:width-20,borderRadius:20,height:40,backgroundColor:'white',borderWidth:1,borderColor:'#D0D0D0',flexDirection:'row'}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Image style={{height: 20,width: 20}} source={Images.blackSearch} />
            </View>
            <View style={{flex:4,justifyContent:'center',alignItems:'flex-start'}}>
              <Text numberOfLines={1} style={{color:'#A0A0A0'}}>{MagnetStore.searchedRegionName ? MagnetStore.searchedRegionName : 'Search' }</Text>
            </View>


          </TouchableOpacity>
          <MapView
            ref={ref => this.mapRef = ref}
            showsUserLocation={true}
            style={styles.mapContainer}
            onPress={this.handlePress}
            region={MagnetStore.currentRegion}
            onRegionChange={() => MagnetStore.currentRegion = "" }
            
          >
          {this.state.instantMarker.map(marker => (
            <MapView.Marker
              coordinate={marker.coordinate}
              onPress={() => this.openInstantMarkerModal()}
            >
            <Image style={{height:40,width:40,resizeMode:'contain'}} source={Images.pinOrange} />

             </MapView.Marker>
           ))}
           {this.state.instantSubscribeMarker.map(marker => (
             <MapView.Marker
               coordinate={marker.coordinate}
               title={'Subscribed Place'}
               onCalloutPress={() => this.createSubscribeModal()}
             >
             <Image style={{height:40,width:40,resizeMode:'contain'}} source={Images.pinGreen} />

             </MapView.Marker>
           ))}
           {this.state.subscribeMarker.map(marker => (
             <MapView.Marker
               coordinate={marker.coordinate}
               title={'Subscribed Place'}
               onCalloutPress={() => this.deleteSubscribeModal()}
             >
             <Image style={{height:40,width:40,resizeMode:'contain'}} source={Images.pinGreen} />

             </MapView.Marker>
           ))}

          {MagnetStore.allMagnets.map(marker => (
            <MapView.Marker
              coordinate={marker.coordinate}
              title={marker.title}
              onCalloutPress={() => this.openAllMarkerCommentModal(marker.id,marker.title,marker._key,marker.userID,marker.name,marker.image,marker.coordinate)}
            >
             <Image style={{height:40,width:40,resizeMode:'contain'}} source={marker.id === KeepToken.userID ? Images.pinOrange : Images.pinRed} />
            </MapView.Marker>
           ))}

           

          </MapView>

          <TouchableOpacity onPress={() => this.activatePin()} style={[styles.pin,{backgroundColor:MagnetStore.magnetPinBG}]}>
            <Image style={styles.pinImage} source={Images.magnetPin} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.activateLocation()} style={[styles.location,{backgroundColor:MagnetStore.magnetLocationBG}]}>
            <Image style={styles.locationImage} source={Images.magnetLocation} />
          </TouchableOpacity>
          <Toast
           visible={this.state.visibleToast}
           position={height-150}
           shadow={true}
           animaton={true}
           hideOnPress={true}
          >{this.state.toastMessage}
          </Toast>
          <Footer />
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#F5FCFF',},
  mapContainer:{position:'relative',flex: 1,height:height,width:width,opacity:1},
  pin:{position:'absolute',right:15,bottom:130,height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'},
  location:{position:'absolute',right:15,bottom:80,height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'},
  subscribe:{position:'absolute',right:15,bottom:0,height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'},
  testMarkers:{position:'absolute',right:15,bottom:210,height:40,width:40,borderRadius:20,justifyContent:'center',alignItems:'center'},
  pinImage:{height:40,width:40},
  locationImage:{height:40,width:40},
  subscribeImage:{height:40,width:40},
  instantMarkerModalContainer:{borderRadius:5,height:300,width:null,backgroundColor:'white',justifyContent:'center',alignItems:'center',flexDirection:'column'},
  textInputInstantMarkerPlace:{height:120,width:width-50,marginTop:10,borderWidth:1,borderColor:'#E0E0E0',alignItems:'center'},
  textInputInstantMarkerTitle:{height:120,width:width-50,fontSize:15},
  instantMarkerModalTopFlex:{flex:3,width:width-75,justifyContent:'center',alignItems:'center'},
  instantMarkerModalBottomFlex:{flex:1,width:width-50,justifyContent:'space-between',alignItems:'center',flexDirection:'row'},
  instantMarkerModalTitleText:{fontSize:18,color:'gray',fontWeight:'bold'},
  instantMarkerModalWarningText:{fontSize:18,color:'#ef5350',fontWeight:'bold'},
  instantMarkerModalCreateButtonPlace:{height:50,width:130,backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center'},
  instantMarkerModalCreateButtonText:{color:'#FFF',fontWeight:'bold'},
  instantMarkerModalCancelButtonPlace:{height:50,width:130,backgroundColor:'#ef5350',justifyContent:'center',alignItems:'center'},
  instantMarkerModalCancelButtonText:{color:'#FFF',fontWeight:'bold'},
  allMarkerCommentModalContainer:{flex:1,flexDirection:'column',backgroundColor:'transparent',justifyContent:'center',alignItems:'center',borderTopLeftRadius:10,borderTopRightRadius:5},
  allMarkerCommentModalContainerWithSizes:{flex:1,flexDirection:'column',backgroundColor:'transparent',justifyContent:'center',alignItems:'center',borderTopLeftRadius:10,borderTopRightRadius:5},
  KeyboardAwareScrollViewStyle:{position: 'absolute',bottom: 0,left: 0,flex: 1,width: width,height: height,}
});
/*

<TouchableOpacity onPress={() => this.activateSubscribe()} style={[styles.subscribe,{backgroundColor:MagnetStore.magnetSubscribeBG}]}>
            <Image style={styles.subscribeImage} source={Images.magnetSubscribe} />
          </TouchableOpacity>

*/

/*listenForItems(locationsRef) {
     
    locationsRef.on('value', (snap) => {
      fetch('https://fenicoapp.com/api/v1/api/servertime?format=m/d/y+h:i:s')
        .then((response) => response.json())
        .then((responseJson) => {
         var items = [];
         snap.forEach((child) => {
            var currentTime = responseJson.date
            var dateMagnetTime = new Date(child.val().time);
            var dateCurrentTime = new Date(currentTime);
            //console.log(dateMagnetTime, dateCurrentTime)
            var diff = dateCurrentTime.getTime() - dateMagnetTime.getTime();
            var msec = diff;
            var hh = Math.floor(msec / 1000 / 60 / 60);
            //console.log(hh, child.val().title)
            
            if(hh < 3 & child.val().status === undefined){
              if(child.val().id == KeepToken.userID){
                MagnetStore.myKey = child.key
              }
              items.push({
                id: child.val().id,
                coordinate:{latitude:child.val().latitude,longitude:child.val().longitude},
                title:child.val().title,
                userID:child.val().userID,
                name:child.val().name,
                image:child.val().image,
                _key: child.key
              });
              
            }else{
              const magnetRef = Firebase.database().ref().child('Magnets').child(child.key.toString())
              magnetRef.update({status: 'passive'})
            }
            MagnetStore.allMagnets = items
          })
        })
        .catch((error) => {
          //console.error(error);
        });
      this.setState({
        isLoadedMagnets:true
      });
    })
} */