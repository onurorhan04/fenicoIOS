import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,View,Dimensions,TouchableOpacity,ListView,Alert,Image} from 'react-native';
import NotificationStore from '../../stores/notificationStore';
import KeepToken from '../../stores/keeptoken';
import Images from '../../images/images';
import Colours from '../../const/colours';
import MagnetStore from '../../stores/magnetStore';
import { observer } from 'mobx-react/native';
var Firebase  = require('firebase');
var { width, height } = Dimensions.get('window')
@observer
export default class NotificationMain extends Component {
  routeAndRemoveCommentNotification(rowData){
    MagnetStore.currentRegion = {
      latitude:Number(rowData.magnetLatitude),
      longitude:Number(rowData.magnetLongitude),
      latitudeDelta:0.5,
      longitudeDelta:0.5
    }
    //this.props.navigation.navigate('magnetMain',)

    this.props.navigation.dispatch({type: 'Reset', index: 0, actions: [{ type: 'Navigate', routeName:'magnetMain', params:{latitudeFromNotif:rowData.magnetLatitude,longitudeFromNotif:rowData.magnetLongitude,magnetKey: rowData.magnetKey}}]})
  }
  routeAndRemoveMessageNotification(notifType,keyID,fromName){
    var contactImage = ""
    var count = NotificationStore.notificationCount
    NotificationStore.notificationCount = count -1
    const deleteNotifToServer = Firebase.database().ref().child("Notifications").child('MessageType').child(KeepToken.userID).child(keyID)
    deleteNotifToServer.remove()

    this.routingToRoom('room',keyID,fromName,contactImage)
  }
  routingToRoom(routeName,contactUserID,contactUsername,contactImage){
    //ProfileOtherStories.searchedUserID=contactUserID
    //ProfileOtherStories.searchedUsername=contactUsername
    //ProfileOtherStories.searchedUserImage=contactImage
    this.props.navigation.navigate(routeName,{contactUserID:contactUserID,contactUsername:contactUsername,contactImage:contactImage})
  }
  renderNotifItem(rowData){
    if(rowData.notifType == "commentType"){
      return(
        <TouchableOpacity onPress={() => this.routeAndRemoveCommentNotification(rowData)} style={styles.commentTypeContainer}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center',marginLeft:5}}><Image source={Images.commentTypeNotif} style={{height:30,width:30}} /></View>
            <View style={{flex:8,justifyContent:'center',marginRight:10}}>
              <Text style={{marginLeft:5,fontWeight:'600'}} numberOfLines={2}>{rowData.commentFrom} commented something in {rowData.magnetTitle};
              </Text>
              <Text style={{marginLeft:5,color:'gray'}} numberOfLines={1}>{rowData.commentText}
              </Text>
            </View>
        </TouchableOpacity>
      )
    }else if(rowData.notifType == "messageType"){
      return(
        <TouchableOpacity onPress={() => this.routeAndRemoveMessageNotification(rowData.notifType,rowData.from,rowData.fromName)} style={styles.messageTypeContainer}>
          <View style={{flex:1,justifyContent:'center',alignItems:'center',marginLeft:5}}><Image source={Images.messageTypeNotif} style={{height:30,width:30}} /></View>
          <View style={{flex:8,justifyContent:'center',marginRight:10}}>
           <Text style={{marginLeft:5,fontWeight:'600'}} numberOfLines={1}>{rowData.fromName}
           </Text>
           <Text style={{marginLeft:5,color:'gray'}} numberOfLines={1}>{rowData.text}
           </Text>
          </View>        
        </TouchableOpacity>
      )
    }else{
      return null;
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerBlank}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.headerLeft}>
             <Image style={styles.headerLeftImage} source={Images.leftArrow} />
            </TouchableOpacity>
            <View style={styles.headerMiddle}>
              <Text style={styles.headerMiddleText}>Notification</Text>
            </View>
            <View style={styles.headerRight}>
              <Text style={styles.headerRightText}></Text>
            </View>
          </View>
        </View>
        {NotificationStore.notificationCount > 0 ?
            <ListView
              style={{marginTop:10}}
              ref="notifList"
              dataSource={NotificationStore.dataSource}
              renderRow={this.renderNotifItem.bind(this)} />
           :
            <View style={{flex:1}}>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}><Text>No Notification</Text></View>
              <View style={{flex:1}}></View>
            </View>
         }
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {flex: 1,alignItems:'center',backgroundColor: '#E0E0E0'},
  commentTypeContainer:{height:60,width:width-20,backgroundColor:'#FFF',flexDirection:'row',margin:3,justifyContent:'center',borderBottomRightRadius:15,borderTopLeftRadius:15},
  messageTypeContainer:{height:60,width:width-20,backgroundColor:'#FFF',flexDirection:'row',margin:3,justifyContent:'center',borderBottomRightRadius:15,borderTopLeftRadius:15},
  header:{height:70,width:width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerBlank:{marginTop:20,height:50,width:width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerLeft:{flex:1,justifyContent:'center',alignItems:'center'},
  headerMiddle:{flex:4,justifyContent:'center',alignItems:'center'},
  headerRight:{flex:1,justifyContent:'center',alignItems:'center'},
  headerRightText:{color:'#FFF',fontWeight:'bold'},
  headerMiddleText:{color:'#FFF',fontWeight:'bold',fontSize:18},
  headerLeftImage:{height:25,width:25},
});

