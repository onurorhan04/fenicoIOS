import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Alert,
  ListView,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Dimensions,
  Image
} from 'react-native';
import Images from '../../images/images';
var windowSize = Dimensions.get('window');
var Firebase  = require('firebase');
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import UserData from '../../stores/userData';
import KeepToken from '../../stores/keeptoken';
import MessageStore from '../../stores/messageStore';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Colours from '../../const/colours'
import moment from 'moment'
import InvertibleScrollView from 'react-native-invertible-scroll-view';
export default class Room extends Component {
  constructor(props){
    super(props);
    this.state={
      userUID:KeepToken.userID,
      oppositeUserUID:this.props.navigation.state.params.contactUserID,
      oppositeUserName:this.props.navigation.state.params.contactUsername,
      oppositeUserImage:this.props.navigation.state.params.contactImage,
      oppositeImage:"",
      speed:10,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      text:'',
      difference:0,
      height:50,
      shouldScrollToBottom:true,
      isLoadedMessages: false
    }
    this.contentHeight = 0,
    this.scrollViewHeight = 0,
    this.itemsRef = this.getRef().child('Messages');
    this.contactRef = this.getRef().child('Contact');
  }
  getRef() {
    return Firebase.database().ref();
  }
  goToUserProfil(){
    
    this.props.navigation.navigate('profilOther',{userID:this.state.oppositeUserUID,username:this.state.oppositeUserName})
  }
  setViewedAllMessage(){
    let childNumberGenerate = this.generateChildNumber();
    const idRef = Firebase.database().ref().child('Messages').child(childNumberGenerate);
    console.log('', MessageStore.chatData.length)
    for(let i = 0; i< MessageStore.chatData.length; i++){
      
      if(MessageStore.chatData[i].from != KeepToken.userID){
          const messageRef = idRef.child(MessageStore.chatData[i]._key)
          messageRef.update({isViewed:true})
      }
    }
  }
  closeMessageListener(){
    const childNumberGenerate = this.generateChildNumber();
    const messagesRef = this.getRef().child('Messages').child(childNumberGenerate)
    messagesRef.off('value')
  }
  listenForItems(itemsRef) {
    var childNumberGenerate = this.generateChildNumber();
    const messagesRef = itemsRef.child(childNumberGenerate).limitToLast(2000);

    messagesRef.on('value', (snap) => {
      
      var items = [];
      snap.forEach((child) => {
        
        items.push({
          from: child.val().from,
          text: child.val().text,
          messageTime: child.val().messageTime,
          isViewed: child.val().isViewed,
          dateTime: child.val().dateTime,
          _key: child.key
        });
        if(items.length == snap.numChildren()){
            setTimeout(function(){ this.setState({ isLoadedMessages: true }) }.bind(this), 3000);
            
        }
      });
      items.reverse()
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(items)
      });
      //this.refs.list.scrollToEnd({animated:false})
      MessageStore.chatData = items
      this.setViewedAllMessage()
    })
  }
  getContactList(){
    MessageStore.userGotInContactList = false
    //MessageStore.contactList = [];
    var contactList = Firebase.database().ref("ContactList/"+KeepToken.userID);
    contactList.on('value', (snap) => {
      var items = [];
      snap.forEach((child) => {
        items.push(child.val().oppositeUserUID);
        if(this.state.oppositeUserUID == child.val().oppositeUserUID){
          MessageStore.userGotInContactList = true
        }
      });
    })
  }
  generateChildNumber(){
    var integerUserUID = parseInt(this.state.userUID);
    var integerOppositeUserUID = parseInt(this.state.oppositeUserUID);
    if(integerUserUID > integerOppositeUserUID){
       var differenceNumber = integerUserUID - integerOppositeUserUID;
    }else{
       var differenceNumber = integerOppositeUserUID - integerUserUID;
    }
    return ((integerUserUID + integerOppositeUserUID) + (integerUserUID*integerOppositeUserUID))*differenceNumber;
  }
  listenForContactItems(contactRef) {
    MessageStore.contactList = []
    var dateNow = new Date();
    const contact = contactRef.child(KeepToken.userID)
    contact.on('value', (snap) => {
      var items = [];
      snap.forEach((child) => {
        items.push({
          contactUserID: child.val().contactUserID,
          contactUsername: child.val().contactUsername,
          _key: child.key
        });
        MessageStore.gotNewContact = true
        MessageStore.contactList.push({contactMessage:child.val().contactMessage,contactUsername:child.val().contactUsername,contactUserID:child.val().contactUserID,_key: child.key})
      });
    })
  }
  pushText(contactRef){
    if(this.state.text != ""){
      var dateNow = new Date();
      const pushContactRef = Firebase.database().ref().child('Contact').child(KeepToken.userID).child(this.state.oppositeUserUID);
      pushContactRef.remove()
      pushContactRef.push({contactUserID:this.state.oppositeUserUID,contactUsername:this.state.oppositeUserName,contactMessage:this.state.text,contactImage:this.state.oppositeImage,dateTime:dateNow.getTime().toString()});
      if(!MessageStore.userGotInContactList){
        const pushContactListRef = Firebase.database().ref().child('ContactList').child(KeepToken.userID);
        pushContactListRef.push({oppositeUserUID:this.state.oppositeUserUID})
        const pushContactListForOppositeRef = Firebase.database().ref().child('ContactList').child(this.state.oppositeUserUID);
        pushContactListForOppositeRef.push({oppositeUserUID:KeepToken.userID})
      }
      var myImage = "https://fenicoapp.com/api/web/uploads/"+UserData.data.profile.image;
      const pushContactForOppositeRef = Firebase.database().ref().child('Contact').child(this.state.oppositeUserUID).child(KeepToken.userID);
      pushContactForOppositeRef.remove()
      pushContactForOppositeRef.push({contactUserID:KeepToken.userID,contactUsername:KeepToken.username,contactMessage:this.state.text,contactImage:myImage,dateTime:dateNow.getTime().toString()});
      const pushMessagesRef = Firebase.database().ref().child('Messages')
      var childNumberGenerate = this.generateChildNumber();
      const idRef = pushMessagesRef.child(childNumberGenerate);

      idRef.push({isViewed:false,fromImage:myImage,toImage:this.state.oppositeUserImage,text:this.state.text,text:this.state.text,fromName:UserData.data.profile.name,from:KeepToken.userID,to:this.state.oppositeUserUID,dateTime:dateNow.toString()});
      
      this._textInput.setNativeProps({text: ''});
      this.setState({ text: '' })
      this.listenForContactItems(this.contactRef);
    }
  }
  getOppositeUserData(){
    fetch("https://fenicoapp.com/api/v1/user-profile/view?id="+ this.state.oppositeUserUID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
        this.setState({
          oppositeImage:"https://fenicoapp.com/api/web/uploads/"+responseData.items[0].image
        })
    });
  }
  componentDidMount(){
    this.setViewedAllMessage()
    this.deleteFromNotification()
  }
  
  componentWillMount(){
    console.disableYellowBox = true;
    this.getOppositeUserData()
    this.getContactList()
    this.listenForItems(this.itemsRef);
  }
  componentWillUnmount() {
    this.closeMessageListener()
  }
  deleteFromNotification(){
    const deleteNotifToServer = Firebase.database().ref().child("Notifications").child('MessageType').child(KeepToken.userID).child(this.state.oppositeUserUID)
    deleteNotifToServer.remove()
    const deleteNotifCount = Firebase.database().ref().child("Contact").child(KeepToken.userID).child(this.state.oppositeUserUID)
    deleteNotifCount.update({notifCount:0})
  }
  showContacts(){
    class ContactTestSchema {}
    ContactTestSchema.schema = {
      name: 'ContactTest',
      properties: {
        oppositeUserUID:  'string',
      }
    };
    var realm = new Realm({schema: [ContactTestSchema,],schemaVersion:2});
    var allContactData = realm.objects('ContactTest')
    //Alert.alert(allContactData[0].oppositeUserUID.toString(),JSON.stringify(allContactData))
  }
  renderItem(item) {
    if(item.from == KeepToken.userID){
      return (
        <View style={{flexDirection:'row',marginBottom:5}}>
          <View style={{flex:1}}>
          </View>
          <View style={{alignItems: 'flex-end',backgroundColor:'#C5E1A5',borderRadius:5,marginTop:5,maxWidth:300,minWidth:70}}>
            <View style={{margin:10,alignItems:'center',justifyContent:'center',marginLeft:10,marginRight:10}}>
              <Text style={{color:'black'}}>
                {item.text}
              </Text>
            </View>
            <View style={{marginLeft:6,marginRight:6,marginBottom:7,flexDirection:'row'}}>
              <Image source={item.isViewed == true ? Images.seen : Images.unseen } style={{height:14,width:14,marginRight:4,marginLeft:4}} />
              <Text style={styles.messageDate}>{moment(item.dateTime).format('LT')}</Text>
            </View>
          </View>
        </View>
      );
    }else{
      return (
        <View style={{flexDirection:'row',marginBottom:5}}>
          <View style={{alignItems: 'flex-start',backgroundColor:'#fff',borderRadius:5,marginTop:5,maxWidth:300,minWidth:70}}>
            <View style={{margin:7,alignItems:'center',justifyContent:'center',marginLeft:10,marginRight:10}}>
              <Text style={{color:'black'}}>
                {item.text}
              </Text>
            </View>
            <View style={{marginLeft:4,marginRight:4,flexDirection:'row'}}>
              <Text style={styles.messageDateOpposite}>{moment(item.dateTime).format('LT')}</Text>
            </View>
          </View>
          <View style={{flex:1}}>
          </View>
        </View>
      );
    }
  }
  render() {
    
    return (
      <View style={styles.container}>
        <View style={{height:70,flexDirection:'row',backgroundColor:Colours.lightSalmon,justifyContent:'center',alignItems:'center',width:windowSize.width}}>
         <View style={{height:60,marginTop:20,flexDirection:'row',justifyContent:'center',alignItems:'center',width:windowSize.width}}>
            <View style={{flex:2,flexDirection:'row'}}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={{height:40,width:35,justifyContent:'center',alignItems:'center'}}>
                <Image source={Images.leftArrow} style={{height:22,width:22}}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.goToUserProfil() }>
                {this.state.oppositeUserImage.length > 60 ?
                  <Image style={{height:40,width:40,borderRadius:20}} source={{uri:this.state.oppositeUserImage}}/>
                :
                  <Image style={{height:40,width:40,borderRadius:20}} source={Images.noImage}/>
                }
              </TouchableOpacity>
            </View>
            <View style={{flex:6,marginLeft:10,marginTop:10,justifyContent:'center'}}>
              <Text style={{color:'white',fontSize:17,fontWeight:'bold'}}>{this.state.oppositeUserName}
              </Text>
              <Text style={{color:'white',fontSize:13}}>
              </Text>
            </View>
            <View style={{flex:1,backgroundColor:'blue'}}>
            </View>
         </View>
        </View>
        
        <View style={{flex:1}}>
            <Image source={Images.roomBG} style={styles.roomBG} />
            <ListView
              ref="list"
              renderScrollComponent={props => <InvertibleScrollView {...props} inverted />}
              dataSource={this.state.dataSource}
              renderRow={this.renderItem.bind(this)}
              enableEmptySections={true}
              style={styles.listview}
              />

             <View style={{flexDirection:'row',height:50,justifyContent:'center',alignItems:'center'}}>
               <View style={{flex:9}}>
                 <TextInput
                   ref={(r) => {this._textInput = r;}}
                   onChangeText={(text) => this.setState({text:text})}
                   style={styles.textInput}
                   underlineColorAndroid='white'
                   placeholder="Type a message"
                 />
               </View>
               <View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
                 <TouchableOpacity style={styles.sendView} onPress={() => this.pushText(this.contactRef)}>
                   <View style={styles.sendButton}>
                     <Image source={Images.send} style={{height:25,width:25,resizeMode:'stretch'}} />
                   </View>
                 </TouchableOpacity>
               </View>
               </View>
               <KeyboardSpacer/>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{flex: 1,backgroundColor: 'white'},
  sendView:{height:40,width:60,justifyContent:'center',alignItems:'center'},
  sendButton:{height:40,width:40,borderRadius:20,backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center'},
  sendText:{fontWeight:'bold',color:'#B0B0B0'},
  textInput:{height:40,width:windowSize.width-70,marginLeft:5,paddingHorizontal:10,backgroundColor:'white',borderWidth:1,borderColor:'#E0E0E0',borderRadius:5},
  senderContent:{height:50,flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#F8F8F8'},
  messageDate:{fontSize:11,color:'black'},
  messageDateOpposite:{fontSize:11,color:'black'},
  roomBG:{position:'absolute',left:0,top:0,width:windowSize.width,height:windowSize.height,resizeMode:'stretch'},
  listview:{margin:15}
  
  

});
