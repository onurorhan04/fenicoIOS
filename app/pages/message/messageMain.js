

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ListView,
  Alert,
  Dimensions,
  Image,
  FlatList
} from 'react-native';
import Modal from 'react-native-modal'
import Images from '../../images/images';
var windowSize = Dimensions.get('window');
var Firebase  = require('firebase');
import KeepToken from '../../stores/keeptoken';
import MessageStore from '../../stores/messageStore';
import NotificationStore from '../../stores/notificationStore';
import Colours from '../../const/colours'
import Footer from '../../components/footer'
import { observer } from 'mobx-react/native';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

@observer
export default class MessageMain extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      isOpenedNewMessageModal:false,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      dataSourceFellows: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    }
    this.messageRef = this.getRef().child('Messages');
    this.contactRef = this.getRef().child('Contact');
  }
  getUserInfo(userID){
    return fetch("https://fenicoapp.com/api/v1/user-profile/view-for-orhan-pasha?id=" + userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      return {image: responseData.items[0].image, name: responseData.items[0].name} 
    })
    
    
  }
  getRef() {
    return Firebase.database().ref();
  }
  routing(routeName,contactUserID,contactUsername,contactImage){
    this.props.navigation.navigate(routeName,{contactUserID:contactUserID,contactUsername:contactUsername,contactImage:contactImage});
  }
  componentWillMount(){
    MessageStore.contactList = []
    this.getContactList()
    this.getFellows()
  }
  generateChildNumber(contactID){
    var integerUserUID = parseInt(KeepToken.userID);
    var integerOppositeUserUID = parseInt(contactID);
    if(integerUserUID > integerOppositeUserUID){
       var differenceNumber = integerUserUID - integerOppositeUserUID;
    }else{
       var differenceNumber = integerOppositeUserUID - integerUserUID;
    }
    return ((integerUserUID + integerOppositeUserUID) + (integerUserUID*integerOppositeUserUID))*differenceNumber;
  }
  componentWillUnmount(){
    console.log('will un mount')
  }
  getContactList(){
    var ref = Firebase.database().ref("Contact/"+KeepToken.userID);
    ref.on('value', (snapshot) => {
        var items = [];
        snapshot.forEach((childSnapshot) => {
        // key will be "ada" the first time and "alan" the second time
        var key = childSnapshot.key;
        //console.log(childSnapshot.val().notifCount)
        var notifCount = 0
        if(childSnapshot.val().notifCount !== undefined){
          notifCount = childSnapshot.val().notifCount.notifCount
        }

        
        // childData will be the actual contents of the child
        childSnapshot.forEach((childChildSnapshot) => {
          var childChildData =  childChildSnapshot.val()
          console.log('key', childChildSnapshot.key)
          //item.key == childChildData.contactUserID
          //Alert.alert('',JSON.stringify(NotificationStore.messageNotifData))
        
          if(childChildSnapshot.key !== 'notifCount'){
            items.push({
                contactMessage: childChildData.contactMessage,
                contactUserID: childChildData.contactUserID,
                contactUsername:childChildData.contactUsername, 
                contactImage:childChildData.contactImage,
                dateTime:childChildData.dateTime,
                messageNotifCount: notifCount
              });
            
            
            
            MessageStore.contactList = items
          }
        })

      });
      items.sort(function(a,b){
        return b.dateTime - a.dateTime
      })
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(items)
      });
     
    });
  }


  /*
  contactMessage: childChildData.contactMessage,
                    contactUserID: childChildData.contactUserID,
                    contactUsername:childChildData.contactUsername, 
                    contactImage:childChildData.contactImage,
                    dateTime:childChildData.dateTime,
                    messageNotifCount: notifCount
  */
  getFellows(){
    fetch("https://fenicoapp.com/api/v1/user-follower/index?id=" + KeepToken.userID, {
      method: "GET",
      headers: {
        'Authorization': 'Bearer ' + KeepToken.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
       this.setState({dataSourceFellows:this.state.dataSourceFellows.cloneWithRows(responseData.items)})
    })
  }
  goToRoomWithModal(followerID,followerName,followerImage){
    this.setState({isOpenedNewMessageModal:false})
    this.routing('room',followerID,followerName,followerImage)
  }
  renderFellows(rowData){
    
    var imageView = ""
    if(rowData.image == null || rowData.image == "" || rowData.image.length < 40){
       imageView = <Image style={{height:40,width:40,borderRadius:20}} source={Images.noImage} />
    }else{
       imageView = <Image style={{height:40,width:40,borderRadius:20}} source={{uri: rowData.image}} />
    }
    return(
      <TouchableOpacity onPress={() => this.goToRoomWithModal(rowData.follower,rowData.name,rowData.image)} style={{flexDirection:'row',height:50,justifyContent:'center',alignItems:'center'}}>
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          {imageView}
        </View>
        <View style={{flex:5,justifyContent:'center'}}>
          <Text style={{marginBottom:5}}>{rowData.name} {rowData.userID} </Text>
          
        </View>
      </TouchableOpacity>
    )
  }
  renderContact(rowData){
    //Alert.alert(rowData.messageNotifCount.toString())
    return(
      <TouchableOpacity onPress={() => this.routing('room',rowData.contactUserID,rowData.contactUsername,rowData.contactImage)} style={{marginTop:10,flex:1,flexDirection:'row'}}>
        
        <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
          <View style={{height:56,width:56,borderRadius:28,justifyContent:'center',alignItems:'center'}}>
            {rowData.contactImage.length > 50 ?
              <Image  source={{uri:rowData.contactImage}} style={{height:56,width:56,borderRadius:28}}/>
              :
              <Image  source={Images.noImage} style={{height:56,width:56,borderRadius:28}}/>
            }
          </View>
        </View>
        <View style={{flex:4,justifyContent:'center',alignItems:'center',borderBottomWidth:1,borderColor:'#D0D0D0'}}>
            <View style={{marginLeft:20,width:windowSize.width-150,margin:15}}>
                <Text numberOfLines={1} style={{fontSize:17,color:'#5A5C5E',fontWeight:'500'}}>{rowData.contactUsername}</Text>
                <Text numberOfLines={1} style={{marginTop:5,color:'#A0A0A0'}}>{rowData.contactMessage} </Text>
            </View>
        </View>
        <View style={{flex:1,justifyContent:'center',alignItems:'center',borderBottomWidth:1,borderColor:'#D0D0D0'}}>
          {rowData.messageNotifCount > 0 ? 
            <View style={{height:24,width:24,borderRadius:12,backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center'}}>
              <Text style={{color:'#fff'}}>{rowData.messageNotifCount}</Text>
            </View>
          : 
           null
          }
        </View>
        <View style={{flex:0.2}}></View>
        
          
        
        
      </TouchableOpacity>
    )
  }
  render() {
    //Alert.alert('',JSON.stringify( MessageStore.contactList))
    return (
      <View style={styles.container}>
        <Modal isVisible={this.state.isOpenedNewMessageModal}>
          <View style={styles.newMessageModalContainer}>
            <View style={styles.newMessageModalModalTopFlex}>
              <View style={{flex:2}}></View>
              <View style={{flex:5,justifyContent:'center',alignItems:'center'}}>
               <Text style={styles.newMessageModalTitleText}>Fellows</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({isOpenedNewMessageModal:false})} style={{flex:2,height:50,justifyContent:'center',alignItems:'center'}}>
                <Image source={Images.exit} style={{marginLeft:15,height:15,width:15}} />
              </TouchableOpacity>
            </View>
            <View style={styles.newMessageModalFellows}>
              {this.state.dataSourceFellows.getRowCount() === 0 ? <View style={styles.noFellowsView}><Text style={styles.noFellowsText}>No Fellows</Text></View> :
                <ListView
                  style={{marginTop:10}}
                  enableEmptySections={true}
                  dataSource={this.state.dataSourceFellows}
                  renderRow={this.renderFellows.bind(this)}
                />
              }
              
            </View>
          </View>
        </Modal>
        <View style={styles.header}>
          <View style={styles.headerBlank}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={styles.headerLeft}>
             <Image style={styles.headerLeftImage} source={Images.leftArrow} />
            </TouchableOpacity>
            <View style={styles.headerMiddle}>
              <Text style={styles.headerMiddleText}>Messages</Text>
            </View>
            <View onPress={() => this.save()} style={styles.headerRight}>
              <Text style={styles.headerRightText}></Text>
            </View>
          </View>
        </View>
        <ListView
          style={{marginTop:10}}
          removeClippedSubviews={false}
          dataSource={this.state.dataSource}
          renderRow={this.renderContact.bind(this)}
        />
        
        <TouchableOpacity onPress={() => this.setState({isOpenedNewMessageModal:true})} style={styles.newMessageView}>
          <Image style={styles.addContact} source={Images.addContact} />
        </TouchableOpacity>
        <View style={styles.blankSpace}></View>
        <Footer />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{flex:1,width:windowSize.width,backgroundColor:'#FFF'},
  blankSpace:{height:50},
  headerBlank:{marginTop:20,height:50,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  header:{height:70,width:windowSize.width,backgroundColor:Colours.lightSalmon,flexDirection:'row'},
  headerLeft:{flex:1,justifyContent:'center',alignItems:'center'},
  headerMiddle:{flex:4,justifyContent:'center',alignItems:'center'},
  headerRight:{flex:1,justifyContent:'center',alignItems:'center'},
  headerRightText:{color:'#FFF',fontWeight:'bold'},
  headerMiddleText:{color:'#FFF',fontWeight:'bold',fontSize:18},
  headerLeftImage:{height:25,width:25},
  newMessageView:{position:'absolute',justifyContent:'center',alignItems:'center',right:25,bottom:75,height:50,width:50,borderRadius:25,backgroundColor:'#27ae60'},
  newMessageModalContainer:{borderRadius:5,height:300,width:windowSize.width-40,backgroundColor:'white',justifyContent:'center',alignItems:'center',flexDirection:'column'},
  newMessageModalModalTopFlex:{flex:1,width:windowSize.width-40,flexDirection:'row',backgroundColor:'#2ecc71',justifyContent:'center',alignItems:'center'},
  newMessageModalTitleText:{color:'#fff',fontWeight:'bold',fontSize:18},
  newMessageModalFellows:{flex:5,width:windowSize.width-40},
  addContact:{height:25,width:25,resizeMode:'stretch'},
  noFellowsView:{flex:1,justifyContent:'center',alignItems:'center'},
  noFellowsText:{fontWeight:'600',fontSize:17}
});
