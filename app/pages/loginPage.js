import React, { Component,PropTypes } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
  TextInput,
  Dimensions,
  Image
} from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob'
var {FBLogin, FBLoginManager} = require('react-native-facebook-login');
const windowSize = Dimensions.get('window');
var base64 = require('base-64');
import KeepToken from '../stores/keeptoken';
import ContractsStore from '../stores/contractsStore';
import MagnetStore from '../stores/magnetStore';
import Button from 'apsl-react-native-button';
import DB from '../realmDB/databases'
import TextField from 'react-native-md-textinput';
import { observer } from 'mobx-react/native';
import Colours from '../const/colours';
import FCM from 'react-native-fcm';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import CheckBox from '../components/checkbox';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Images from '../images/images';

//onuro2oyla
//53c1eadonur


@observer
export default class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state={
      username:'',
      password:'',
      loadingOverlay:false,
      invalid: false,
      error: false,
      errorMessage: '',
    }
  }
  routing(routeName){
    this.props.navigation.navigate(routeName)
  }
  saveFBLogin(fbtoken,password,email){
    const deleteObjects = DB.objects('FaceBookLoginSchema')
    DB.write(() => {
      DB.delete(deleteObjects)
      const login = DB.create('FaceBookLoginSchema',
        {
          username: password,
          password: password,
          fbtoken: fbtoken,
          email: email,
        }
      )
    })
    const objects = DB.objects('FaceBookLoginSchema')
  }
  saveSPLogin(username,password){
    const deleteObjects = DB.objects('SPLoginSchema')
    DB.write(() => {
      DB.delete(deleteObjects)    
      const login = DB.create('SPLoginSchema',
        {
          username: username,
          password: password,
        }
      )
    })
  }
  
  checkAppIntro(){
    const appIntroData = DB.objects('AppIntro');
    if(appIntroData.length == 0){
      this.props.navigation.navigate('appIntro',{from: 'loginPage'})
    }else{
      this.routing("magnetMain")
    }
    //return appIntroData.length
    /*if(appIntroData.length == 0){
      return 0
    }*/
  }
  updatePasswordAndImage(userID,password,token,userFacebookID){
    let formDataPassword = []
    formDataPassword.push(encodeURIComponent('password') + "=" + encodeURIComponent(password))
    const body = formDataPassword.join('&')
    fetch('https://fenicoapp.com/api/v1/user/update?id=' + userID, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Authorization': 'Bearer' + token
      },
      body
    }).then((resp) => {
        this.setState({loadingOverlay: false})
        this.checkAppIntro()
    })
  }
  userLogin(username,password){
    if(ContractsStore.contractInfo){
      if(username == "" || password == ""){
        //Alert.alert('Error','Fill in empty spaces!')
        this.setState({error: true, errorMessage: 'Please fill in the blanks!'})
      }else{
        this.setState({loadingOverlay: true})
        var params = {
            username: base64.encode(username),
            password: base64.encode(password)
        };
        var formData = new FormData();
        for (var k in params) {
            formData.append(k, params[k]);
        }

        fetch("https://fenicoapp.com/api/v1/api/access-token", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data'
          },
          body: formData
        })
        .then((response) => response.json())
        .then((responseData) => {
          if(responseData.status == true){
            this.storeReducer()
            KeepToken.userID=responseData.user_id
            KeepToken.token=responseData.access_token
            KeepToken.username = username
            KeepToken.password = password
            this.subscribeToFirebase(responseData.user_id)
            this.saveSPLogin(username,password)
            this.setState({loadingOverlay: false})
            this.routing("magnetMain")
          }else{
            this.setState({loadingOverlay: false, error: true, errorMessage: 'Invalid username or password'})
            //Alert.alert('Error',responseData.message.toString())
          }
        }).catch((err) => {
            this.setState({loadingOverlay: false, error: true, errorMessage: 'Invalid username or password'})
            //Alert.alert('Error','Invalid username or password')
            
          
        })
      }
    } else { this.setState({ loadingOverlay: false, error: true, errorMessage: 'Please accept the terms of use and privacy policy!'}) }
    
  }
  userFBLogin(data){
   
    this.setState({loadingOverlay: true})
    
    
    fetch('https://fenicoapp.com/api/v1/search/search?q=istanbul21312321', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer fbtoken' + data.credentials.token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.storeReducer()
      this.getUserData(responseData.user_id,responseData.access_token)
      this.saveFBLogin(data.credentials.token,data.credentials.userId,'')
      this.subscribeToFirebase(responseData.user_id)
      KeepToken.userID = responseData.user_id
      KeepToken.token = responseData.access_token
      KeepToken.username = data.credentials.userId
      KeepToken.password = data.credentials.userId
      this.updatePasswordAndImage(KeepToken.userID,KeepToken.password,KeepToken.token,data.credentials.userId)
      
    }).catch((err) => {
      console.log('fb err:')
      console.log(err)
      this.setState({loadingOverlay: false, error: true, errorMessage: 'A problem has occured'})
    })
    
  }
  subscribeToFirebase(userID){
    const firebaseSubscriber = DB.objects('FirebaseSubscriber')
    for(let i=0; i<firebaseSubscriber.length; i++){
      const topic = '/topics/' + firebaseSubscriber[i].userID
      FCM.unsubscribeFromTopic(topic);
    }
    DB.write(() => {
      DB.delete(firebaseSubscriber)    
      const fs = DB.create('FirebaseSubscriber',
        {
          userID: userID.toString()
        }
      )
    })
    const newTopic = '/topics/' + userID
    FCM.subscribeToTopic(newTopic);

  }
  getUserData(userID,token){
    fetch('https://fenicoapp.com/api/v1/user-profile/view?id=' + userID, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
      KeepToken.username = responseData.items[0].username     
    })
  }
  routeForgotPassword(){
    this.props.navigation.navigate('forgotPasswordPage')
  }
  routeSignup(){
    this.props.navigation.navigate('signupPage')
  }
  componentWillMount(){
    ContractsStore.contractInfo = false
  }
  storeReducer(){
    MagnetStore.myKey = ""
  }
  
  render() {
    return (
      
      <View style={styles.container}>
        <Image style={{position:'absolute',left:0,top:0,width:windowSize.width,height:windowSize.height}} source={Images.loginBG2} />
        <View style={{flex:0.2,backgroundColor:'transparent',marginTop:20,justifyContent:'center',alignItems:'center'}}>
          <Text style={{color:'#FFF',fontWeight:'bold',fontSize:28}}>Fenico</Text>
        </View>
        <KeyboardAwareScrollView style={{flex:1}} showsVerticalScrollIndicator={false} contentContainerStyle={{justifyContent:'center',alignItems:'center'}} >
        
        <View style={{flex:1}}>
          <View style={{flex:1,width:300,padding:5,borderRadius:10}}>
            <TextField 
              label={'Username'} 
              highlightColor={'#FFF'}
              textFocusColor={'#FFF'}
              labelColor={'#FFF'}
              textColor={'#FFF'}
              textBlurColor={'#FFF'}
              value={this.state.username}
              onChangeText={(username) => this.setState({username:username})} 
           />
          </View>
          <View style={{flex:1,width:300,padding:5,borderRadius:10,marginTop:10}}>
            
            <TextField 
              label={'Password'} 
              highlightColor={'#FFF'}
              textFocusColor={'#FFF'}
              labelColor={'#FFF'}
              textColor={'#FFF'}
              textBlurColor={'#FFF'}
              value={this.state.password}
              secureTextEntry={true}
              onChangeText={(password) => this.setState({password:password})}
          />
          </View>
        </View>
        <View style={{flex:1,flexDirection:'row',alignItems:'center',marginTop:10}}>
            <CheckBox
            leftTextStyle={{backgroundColor:'transparent'}}
            rightTextStyle={{backgroundColor:'transparent'}}
              style={{backgroundColor:'transparent'}}
              label=''
              checked={ContractsStore.contractInfo}
              onChange={(checked) => ContractsStore.contractInfo = !ContractsStore.contractInfo }
            />
            <TouchableOpacity style={{backgroundColor:'transparent'}} onPress={() => this.props.navigation.navigate('contracts')}>
              <Text style={{color:'#fff',fontWeight:'bold',marginLeft:5}}>Terms of Use and Privacy Policy</Text>
            </TouchableOpacity>
            
        </View>
        {this.state.error && !ContractsStore.contractInfo ? 
          <View style={{height:20,backgroundColor:'transparent'}}>
              <Text style={{color:'#E74C3C',fontWeight:'bold'}}>{this.state.errorMessage}</Text>
          </View>
        :null}
        {this.state.error && ContractsStore.contractInfo ? 
          <View style={{height:20,backgroundColor:'transparent'}}>
              <Text style={{color:'#E74C3C',fontWeight:'bold'}}>{this.state.errorMessage}</Text>
          </View>
        : null }
        <View style={{flex:0.1}}>
          {this.state.invalid ? <Text style={{color:'red',fontWeight:'600'}}>Invalid username or password</Text> : null }
        </View>
        <View style={{flex:1,marginTop:30,justifyContent:'center',alignItems:'center'}}>
          
            <TouchableOpacity onPress={() => this.userLogin(this.state.username,this.state.password)} activeOpacity={0.6} style={{height:50,width:windowSize.width-50,backgroundColor:Colours.lightSalmon,borderRadius:25,justifyContent:'center',alignItems:'center'}} >
                <Text style={{color:'#fff',fontWeight:'bold'}}>Login</Text>
            </TouchableOpacity>
            
            <View>

              <FBLogin 
                style={{marginTop:20}}
                permissions={["email"]}
                buttonView={<FBLoginView />}
                ref={(fbLogin) => { this.fbLogin = fbLogin}}
                loginBehavior={FBLoginManager.LoginBehaviors.Native}
                onLogin={(data) => this.userFBLogin(data)}
              />
              {!ContractsStore.contractInfo ? 
                <TouchableOpacity onPress={() => this.setState({error: true,errorMessage: 'Please accept the terms of use and privacy policy!'})} activeOpacity={1} style={{marginTop:20,position:'absolute',zIndex:9999,width:200,height:40,backgroundColor:'transparent'}}>
                </TouchableOpacity>
              : null}
            </View>
            
              
          <View style={{flex:1,marginTop:20,justifyContent:'center',alignContent:'center',flexDirection:'row'}}>
            <TouchableOpacity style={{backgroundColor:'transparent'}} onPress={() => this.routeForgotPassword()}>
              <Text style={{fontSize:11,color:'#fff'}}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.routeSignup()} style={{marginLeft:20,backgroundColor:'transparent'}}>
              <Text style={{fontSize:11,color:'#fff'}}>Don't have an account? Create One!</Text>
              
            </TouchableOpacity>
          </View>
          
        </View>
        </KeyboardAwareScrollView>
        
         <OrientationLoadingOverlay
          visible={this.state.loadingOverlay}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="Logging in. Please wait.."
          />
       
      </View>
      
    );
  }
}

class FBLoginView extends Component {
  static contextTypes = {
    isLoggedIn: React.PropTypes.bool,
    login: React.PropTypes.func,
    logout: React.PropTypes.func,
    props: React.PropTypes.object
	};

  constructor(props) {
      super(props);
    }
    render(){
        return (
            <TouchableOpacity
              style={{flexDirection:'row',marginTop:10,color:'#FFFFFF',backgroundColor: '#375694',borderRadius:30,borderWidth:0,width:400}}
              textStyle={{fontSize: 18}}
              onPress={() => {
                  if(!this.context.isLoggedIn){
                    this.context.login()
                  }else{
                    this.context.logout()
                  }
                  
                }}>
                <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                  <Image style={{height:25,width:25}} source={Images.plus}/>
                </View>
                <View style={{flex:3,justifyContent:'center'}}>
                  <Text style={{marginLeft:20,color:'#FFFFFF'}}>Sign in with Facebook</Text>
                </View>
            </TouchableOpacity>
      )
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: '#fff',

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
