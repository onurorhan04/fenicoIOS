import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Alert
} from 'react-native';
import Images from '../../images/images';
import { observer } from 'mobx-react/native';
import KeepToken from '../../stores/keeptoken';
var { width, height } = Dimensions.get('window')

@observer
export default class FollowInfo extends Component {
    constructor(props){
        super(props)
        this.state = {
            filtre:this.props.navigation.state.params.filtre,
            userID:this.props.navigation.state.params.userID,
            data:[],
        }
    }
    getFollowerData(){
        fetch("https://fenicoapp.com/api/v1/user-follower/follower?id="+ this.state.userID, {
            method: "GET",
            headers: {
            'Authorization': 'Bearer ' + KeepToken.token
            }
        })
        .then((response) => response.json())
        .then((responseData) => {
            this.setState({data: responseData.items})
        })
    }
    getFollowingData(){
        fetch("https://fenicoapp.com/api/v1/user-follower/index?id=" + this.state.userID, {
            method: "GET",
            headers: {
                'Authorization': 'Bearer ' + KeepToken.token
            }
        })
        .then((response) => response.json())
        .then((responseData) => {
            this.setState({data: responseData.items})
            //Alert.alert('',JSON.stringify(responseData.items))
        })
    }
    componentDidMount(){
        if(this.state.filtre == "Followers"){
            this.getFollowerData()
        }else{
            this.getFollowingData()
        }
    }
    goToUser(userID, followerID, userName){
        if(this.state.filtre == "Followers"){
            //userID
            if(userID == KeepToken.userID){
                this.props.navigation.navigate('profilMain')
            }else{
                this.props.navigation.navigate('profilOther',{userID:userID,username:userName})
            }
        }else{
            //followerID
            if(followerID == KeepToken.userID){
                this.props.navigation.navigate('profilMain')
            }else{
                this.props.navigation.navigate('profilOther',{userID:followerID,username:userName})
            }
        }
    }
    renderRow(item){
        return(
            <TouchableOpacity onPress={() => this.goToUser(item.item.user,item.item.follower,item.item.name)} style={{flexDirection:'row',padding:10,borderBottomWidth:1,borderColor:'#E0E0E0',height:60}}>
                {item.item.image.length > 0 ? 
                    <Image source={{uri: item.item.image}} style={{height:40,width:40,borderRadius:20}} />
                : 
                    <Image source={Images.noImage} style={{height:40,width:40,borderRadius:20}} />
                }
                <View style={{marginLeft:10,justifyContent:'center',alignItems:'center'}}>
                  <Text>{item.item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    render() {
        return (
        <View style={styles.container}>
            <View style={{height:10}}></View>
            
            <View style={{flex:1,flexDirection:'row',borderBottomWidth:1,borderColor:'#D0D0D0'}}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Image style={{height:24,width:24}} source={Images.back} />
                </TouchableOpacity>
                <View style={{flex:6,justifyContent:'center'}}>
                    
                    <Text style={{fontSize:18}}>{this.state.filtre}</Text>
                </View>
            </View>
            <View style={{flex:9}}>
                <FlatList
                    style={{flex:1}}
                    data={this.state.data}
                    renderItem={this.renderRow.bind(this)}
                />
            </View>
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   
    backgroundColor: '#FFF',
  },
});

