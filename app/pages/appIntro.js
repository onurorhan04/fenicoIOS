import React, { Component } from 'react';
import { AppRegistry, Alert,View,Image,Text,StyleSheet,TouchableOpacity } from 'react-native';
import Intro from 'react-native-app-intro';
import Images from '../images/images'
import DB from '../realmDB/databases'
import Colours from '../const/colours';
export default class AppIntro extends Component {
  constructor(props){
    super(props)
    this.state = {
      from: this.props.navigation.state.params.from
    }
  }
  checkAppIntro(){
    const appIntroStatus = DB.objects('AppIntro')
    DB.write(() => {
      const status = DB.create('AppIntro',
        {
          username: 'true',
        }
      )
    })
  }
  onSkipBtnHandle = (index) => {
    this.checkAppIntro()
    if(this.state.from == "signupPage"){
      this.props.navigation.dispatch({type: 'Reset', index: 0, actions: [{ type: 'Navigate', routeName:'loginPage'}]})
    }else{
      this.props.navigation.dispatch({type: 'Reset', index: 0, actions: [{ type: 'Navigate', routeName:'magnetMain'}]})
    }
  }
  doneBtnHandle = () => {
    this.checkAppIntro()
    if(this.state.from == "signupPage"){
      this.props.navigation.dispatch({type: 'Reset', index: 0, actions: [{ type: 'Navigate', routeName:'loginPage'}]})
    }else{
      this.props.navigation.dispatch({type: 'Reset', index: 0, actions: [{ type: 'Navigate', routeName:'magnetMain'}]})
    }
  }
  nextBtnHandle = (index) => {
    //console.log(index);
  }
  onSlideChangeHandle = (index, total) => {
    //console.log(index, total);
  }
  componentWillMount(){
    //console.disableYellowBox = true;
  }
  render() {
    const pageArray = [{
      title: 'Page 1',
      description: 'Description 1',
      img: Images.initMap,
      imgStyle: {
        height: 400 * 1,
        width: 109 * 2.5,
      },
      backgroundColor: '#fa931d',
      fontColor: '#fff',
      level: 10,
    }];
    return (
      <View style={{backgroundColor:Colours.appIntro}} >

      <Intro
        onNextBtnClick={this.nextBtnHandle}
        onDoneBtnClick={this.doneBtnHandle}
        onSkipBtnClick={this.onSkipBtnHandle}
        onSlideChange={this.onSlideChangeHandle}>
        <View style={styles.slide}>
          <Text style={styles.text}></Text>
          <Image level={10} source={Images.appIntro1} style={{height:400,width:300,resizeMode:'contain',marginTop:5}} />
          <View level={15} style={{justifyContent:'center',alignItems:'center',marginTop:15}}>
            <Text style={styles.text}>When you first open the app, </Text>
            <Text style={styles.text}>' MAGNET ' will pull you to the map</Text>
              <Text style={styles.text}></Text>
          </View>
          <View style={{height:100}}></View>
        </View>

        <View style={styles.slide}>
          <Text style={styles.text}></Text>
          <Image level={10} source={Images.appIntro2} style={{height:400,width:300,resizeMode:'contain',marginTop:5}} />
          <View level={15} style={{justifyContent:'center',alignItems:'center',marginTop:15}}>
            <Text style={styles.text}>Click ' LOCATION ' button to show</Text>
            <Text style={styles.text}>your location</Text>
            <Text style={styles.text}></Text>
            
          </View>
          <View style={{height:100}}></View>
        </View>
        <View style={styles.slide}>
          <Text style={styles.text}></Text>
          <Image level={10} source={Images.appIntro3} style={{height:350,width:300,resizeMode:'contain',marginTop:5}} />
          <View level={15} style={{justifyContent:'center',alignItems:'center',marginTop:5}}>
            <Text style={styles.text15}>If you want to create subscriber place</Text>
            <Text style={styles.text}>Creating ' MAGNET ' you can leave</Text>
            <Text style={styles.text}>comment/opinion/question</Text>
            <Text style={styles.text}>about certain place</Text>
          </View>
          <View style={{height:100}}></View>
        </View>
        <View style={styles.slide}>
          <Text style={styles.text}></Text>
          <Image level={10} source={Images.appIntro4} style={{height:350,width:300,resizeMode:'contain',marginTop:5}} />
          <View level={15} style={{justifyContent:'center',alignItems:'center',marginTop:5}}>
            <Text style={styles.text}>Click on the place you want to leave </Text>
            <Text style={styles.text}>comment/opinion/question,</Text>
            <Text style={styles.text}>then touch orange magnet icon </Text>
            <Text style={styles.text}>to open a text box</Text>
            
            
          </View>
          <View style={{height:100}}></View>
        </View>
        



        
      </Intro>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#9DD6EB',
    backgroundColor:'black',
    padding: 15,
    marginTop:50,
    marginBottom:10,
    backgroundColor: Colours.appIntro
  },
  text: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  text14: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'bold',
  },
  text15: {
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
  },
  
});
