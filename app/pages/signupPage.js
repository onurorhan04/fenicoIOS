import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  Dimensions,
  Alert,
  ActivityIndicator,
  ScrollView
} from 'react-native';
import TextField from 'react-native-md-textinput';
import Colours from '../const/colours';
import ContractsStore from '../stores/contractsStore';
var windowSize = Dimensions.get('window');
import Button from 'apsl-react-native-button';
import Images from '../images/images';
import Modal from 'react-native-animated-modal';
import CheckBox from '../components/checkbox/blackIndex';
var base64 = require('base-64');
import { observer } from 'mobx-react/native';
import DB from '../realmDB/databases'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
@observer
export default class SignupPage extends Component {
  constructor(props){
    super(props);
    this.state={
      nameandlastname: "",
      username: "",
      email: "",
      password: "",
      passwordAgain: "",
      error_message: "",
      error_code: "",
      visibleModal: null,
      contractsModal: false,
      
    }
  }
  /*componentWillMount(){
    const appIntroData = DB.objects('AppIntro');
    DB.write(() => {
      DB.delete(appIntroData);
    })
    const appIntroDataAf = DB.objects('AppIntro');
    
    Alert.alert('',appIntroDataAf.length.toString())
    //Alert.alert('',this.checkAppIntro().toString())
  }*/
  checkAppIntro(){
    const appIntroData = DB.objects('AppIntro');
    if(appIntroData.length == 0){
      this.props.navigation.navigate('appIntro',{from: 'signupPage'})
    }else{
      this.props.navigation.navigate('loginPage')
    }
    //return appIntroData.length
    /*if(appIntroData.length == 0){
      return 0
    }*/
  }

  signupUser(){
    
    var formData = new FormData();

    if(this.state.nameandlastname && this.state.username && this.state.email && this.state.password){
      if(this.state.password == this.state.passwordAgain){
        if(ContractsStore.contractInfo){
            this.setState({
            visibleModal:1
          })
          formData.append('Users[name]', this.state.nameandlastname.toString());
          formData.append('Users[username]', this.state.username.toString());
          formData.append('Users[email]',this.state.email.toString());
          formData.append('Users[password]', this.state.password.toString());
          fetch("https://fenicoapp.com/api/v1/user/signup", {
            method: "POST",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data'
            },
            body: formData
          })
          .then((response) => response.json())
          .then((responseData) => {
            if(responseData.success){
              this.setState({
                error_message: "Account has successfully created",
                error_code:0,
                visibleModal:null
              })
              //this.props.navigation.goBack(null)
              this.checkAppIntro()
            }else{
              if(responseData.error_code == 111){
                //mail adresi kayıtlı
                this.setState({
                  error_message: "Mail address is registered !",
                  error_code:1,
                  visibleModal:null
                })
              }else if(responseData.error_code == 112){
                //username kayitli
                this.setState({
                  error_message: "Username is registered !",
                  error_code:1,
                  visibleModal:null
                })
              }
            }
          }).catch((err) => {
            this.setState({
              error_message: "A problem has occured.Try Again!",
              error_code:0,
              visibleModal:null
            })
          })
          
        }else{
          this.setState({
            error_message: "Accept Terms of Use and Privacy Policy",
            error_code:1,
            visibleModal:null
          })
        }
        
      }else{
        this.setState({
          error_message: "Passwords are not equal!",
          error_code:1,
          visibleModal:null
        })
      }
    }else{
      this.setState({
        error_message: "Fill in empty spaces!",
        error_code:1,
        visibleModal:null

      })
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Modal isVisible={this.state.visibleModal === 1}
          backdropColor = '#000000'
          backdropOpacity = {0.55}
          animationIn={'bounceIn'}
          animationOut={'bounceOut'}
          style={{justifyContent:'center',alignItems:'center'}}
          >
          <View style={{flexDirection:'row',backgroundColor:'#FFF',width:150,height:60,borderRadius:5,borderWidth:1,borderColor:'#',justifyContent:'center',alignItems:'center'}}>
            <ActivityIndicator
                 animating={this.state.animating}
                 style={[styles.centering, {height: 50}]}
                 size="small"
               />
             <Text>Please wait...
            </Text>
          </View>
        </Modal>
        <Modal isVisible={this.state.contractsModal}
          style={{justifyContent:'center',alignItems:'center'}}
          backdropColor = '#000000'
          backdropOpacity = {0.5}
          animationIn={'bounceIn'}
          animationOut={'bounceOut'}
          >
          <View style={{flexDirection:'row',backgroundColor:'#FFF',width:windowSize.width-50,height:windowSize.height-100,justifyContent:'center',alignItems:'center'}}>
            
          </View>
        </Modal>
        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={{position:'absolute',left:8,top:25,height:60,width:60,justifyContent:'center',alignItems:'center'}}>
          <Image source={Images.back} style={{height:30,width:30}} />
        </TouchableOpacity>
        <View style={{flex:0.2,justifyContent:'center',alignItems:'center'}}>
          <View style={{flex:5,alignItems:'center'}}>
            <View style={{marginTop:10}}>
              <Image style={{height:40,width:40}} source={Images.magnet}/>
              <Text style={{color:Colours.lightSalmon,fontSize:24,marginBottom:10}}>
                fenico
              </Text>
            </View>

            
          </View>
          <View style={{flex:1}}>
          </View>
        </View>
        <KeyboardAwareScrollView style={{flex:1}} contentContainerStyle={{justifyContent:'center',alignItems:'center'}} enableAutoAutomaticScroll={true} showsVerticalScrollIndicator={false} >
        
        <View style={{flex:1,justifyContent:'space-between',alignItems:'center'}}>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:5,justifyContent:'center',alignItems:'center'}}>
              <TextField
                value={this.state.nameandlastname}
                onChangeText={(nameandlastname) => this.setState({nameandlastname:nameandlastname})}
                label={"Name and Lastname"}
                style={{height:30,width:windowSize.width-50}}/>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{justifyContent:'center',alignItems:'center'}}>
              <TextField
                value={this.state.username}
                onChangeText={(username) => this.setState({username:username})}
                label={"Username"}
                style={{height:30,width:windowSize.width-50}}/>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:5,justifyContent:'center',alignItems:'center'}}>
              <TextField
                value={this.state.email}
                onChangeText={(email) => this.setState({email:email})}
                label={"E-mail adress"}
                style={{height:30,width:windowSize.width-50}}/>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>

            <View style={{flex:5,justifyContent:'center',alignItems:'center'}}>
              <TextField
                value={this.state.password}
                secureTextEntry={true}
                onChangeText={(password) => this.setState({password:password})}
                label={"Password"}
                style={{height:30,width:windowSize.width-50}}/>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:5,justifyContent:'center',alignItems:'center'}}>
              <TextField
                value={this.state.passwordAgain}
                secureTextEntry={true}
                onChangeText={(passwordAgain) => this.setState({passwordAgain:passwordAgain})}
                label={"Again Password"}
                style={{height:30,width:windowSize.width-50}}/>
            </View>
          </View>
          <View style={{flex:0.8,flexDirection:'row',alignItems:'center'}}>
            <CheckBox
              label=''
              checked={ContractsStore.contractInfo}
              onChange={(checked) => ContractsStore.contractInfo = !ContractsStore.contractInfo }
            />
            <TouchableOpacity onPress={() => this.props.navigation.navigate('contracts')}>
              <Text style={{color:'gray',marginLeft:5}}>Terms of Use and Privacy Policy</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{flex:0.5,justifyContent:'center',alignItems:'center',marginBottom:10}}>

          <View>
            <Text style={{color: this.state.error_code == 0 ? '#2ECC71' : '#E74C3C'}}>{this.state.error_message}
            </Text>
          </View>
          <Button
            style={{color:'#FFFFFF',backgroundColor: Colours.lightSalmon,borderRadius:30,borderWidth:0,width:300}}
            textStyle={{fontSize: 18}}

            onPress={() => this.signupUser()}>

          <Text style={{color:'#FFFFFF'}}>Sign up</Text>
          </Button>
         
          <View style={{flexDirection:'row',marginTop:0}}>
            <Text style={{fontSize:12}}>Already have an account
            </Text>
            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
              <Text style={{fontSize:12,color:Colours.lightSalmon}}> Log in!
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        </KeyboardAwareScrollView>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  centering: {
     alignItems: 'center',
     justifyContent: 'center',
     padding: 8,
   },
});