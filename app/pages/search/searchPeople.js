import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableOpacity,
  Image,
  Dimensions
} from 'react-native';
import Images from '../../images/images';
import SearchStore from '../../stores/searchStore';
import { observer } from 'mobx-react/native';
import { withNavigation } from 'react-navigation';
import KeepToken from '../../stores/keeptoken';
import Footer from '../../components/footer'
var { width, height } = Dimensions.get('window')
@withNavigation
@observer
export default class SearchPeople extends Component {
  routing(routeName,searchedUserID,searchedUsername){
    if(KeepToken.userID == searchedUserID){
      this.props.navigation.navigate('profilMain')
    }else{
      this.props.navigation.navigate(routeName,{userID:searchedUserID,username:searchedUsername})
    }

    //this.props.navigator.push(router.internalRouter.getRoute(routeName,{searchedUserID:searchedUserID,searchedUsername:searchedUsername}));
  }
  renderSearchPeople(rowData){
    if(rowData.type != "user"){return(null)}
    if(rowData.type == "user"){
      return (
        <TouchableOpacity onPress={() => this.routing("profilOther",rowData.id,rowData.name)} style={styles.finding} >
            <View style={styles.photoContainer}>
              {rowData.image.length > 60 ?
                  <Image style={styles.photoStyle} source={{uri: rowData.image}} />
                 :
                  <Image style={styles.photoStyle} source={Images.noImage} />
                }
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.name}>{rowData.name}</Text>
              <Text style={styles.tag}></Text>
            </View>
        </TouchableOpacity>
      )
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <ListView
          dataSource={SearchStore.dataSource}
          renderRow={this.renderSearchPeople.bind(this)}
          enableEmptySections={true}
          initialListSize={1}
        />
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#fff'},
  photoStyle:{height:50,width:50,borderRadius:25},
  finding:{flexDirection:'row',height:50,width:width-20,margin:10},
  photoContainer:{flex:1,justifyContent:'center',alignItems:'center'},
  textContainer:{flex:3,borderBottomWidth:1,borderColor:'#E0E0E0',flexDirection:'column'},
  name:{color:'#A0A0A0'},
  tag:{color:'#C0C0C0'}
});
