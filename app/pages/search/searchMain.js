import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import ScrollableTabView from '../../components/scrollableTabView'
//import ScrollableTabView from 'react-native-scrollable-tab-view';
import SearchPeople from './searchPeople'
import SearchStory from './searchStory'
import SearchTitle from './searchTitle'

import KeepToken from '../../stores/keeptoken';
import SearchStore from '../../stores/searchStore';
import { observer } from 'mobx-react/native';
import { withNavigation } from 'react-navigation'
import Footer from '../../components/footer'
import Images from '../../images/images';


var { width, height } = Dimensions.get('window')
@withNavigation
@observer
export default class SearchMain extends Component {

  instantSearch(keyword){
    if(keyword.length > 0){
      fetch("https://fenicoapp.com/api/v1/search/search?q="+keyword, {
        method: "GET",
        headers: {
          'Authorization': 'Bearer ' + KeepToken.token
        }
      })
      .then((response) => response.json())
      .then((responseData) => {
        if(responseData.items){
          console.log(responseData)
          SearchStore.searchedData = responseData.items
        }
      }).catch((error) => {
          //console.log(error);
      });
    }else{
      SearchStore.searchedData = []
    }

  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.searchBarPlace}>
          <View style={{flex:0.2}}></View>
          <View style={{flex:1,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Image style={{height:24,width:24}} source={Images.back} />
            </TouchableOpacity>
            <View style={{flex:7}}>
              <TextInput onChangeText={(searchKeyWord) => this.instantSearch(searchKeyWord)} style={styles.textInput} placeholder={"Search something"} />
            </View>
          </View>
        </View>
        <ScrollableTabView
          style={{flex:9}}
          tabBarBackgroundColor={"FFF"}
        >
          <SearchPeople tabLabel="People" />
          <SearchTitle tabLabel="Title" />
          <SearchStory tabLabel="Location" />
          

        </ScrollableTabView>
        <View style={styles.blankSpace}></View>
        <Footer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#fff'},
  searchBarPlace:{flex:1.5,justifyContent:'center',alignItems:'center'},
  textInput:{height:40,width:width-60,borderWidth:1,borderColor:'#E0E0E0',borderRadius:20,paddingHorizontal:10},
  blankSpace:{height:50}
});
