import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native';
import SearchStore from '../../stores/searchStore';
import { observer } from 'mobx-react/native';
import { withNavigation } from 'react-navigation'
var { width, height } = Dimensions.get('window')
@withNavigation
@observer
export default class SearchStory extends Component {
  routing(routeName,storyID){
      this.props.navigation.navigate(routeName,{storyID:storyID})
  }
  cleanStoryTitle(title){
    var fullTitle = title
    var splicedTitle = fullTitle.slice(0,30)
    var inTag = false
    var cleanedTitle = ""
    for( let i = 0; i < splicedTitle.length; i++ ){
      if(!inTag){
        if(splicedTitle.substr(i,1) == "<" && !inTag){
          inTag = true
        }else{
          cleanedTitle += splicedTitle.substr(i,1)
          inTag = false
        }

      }else{
        if(splicedTitle.substr(i,1) == ">"){
          inTag = false
        }
      }
    }
    return cleanedTitle
  }
  renderSearchStory(rowData){
    if(rowData.type != "story"){return(null)}
    if(rowData.type == "story"){
      return (
        <TouchableOpacity onPress={() => this.routing('showStory',rowData.id)} style={styles.finding} >
            <View style={styles.photoContainer}>
              <Image style={styles.photoStyle} source={{uri: rowData.image}} />
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.name}>{this.cleanStoryTitle(rowData.name)}...</Text>
              <Text style={styles.tag}>{rowData.like_count} {rowData.like_count > 1 ? "likes" : "like" }</Text>
            </View>
        </TouchableOpacity>
      )
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <ListView
          dataSource={SearchStore.dataSource}
          renderRow={this.renderSearchStory.bind(this)}
          enableEmptySections={true}
          initialListSize={1}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#fff'},
  photoStyle:{height:50,width:50,borderRadius:25},
  finding:{flexDirection:'row',height:50,width:width-20,margin:10},
  photoContainer:{flex:1,justifyContent:'center',alignItems:'center'},
  textContainer:{flex:3,borderBottomWidth:1,borderColor:'#E0E0E0',flexDirection:'column'},
  name:{color:'#A0A0A0'},
  tag:{color:'#C0C0C0'}
});
