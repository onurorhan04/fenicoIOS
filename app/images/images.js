const images = {
  footerProfile: require('./footer/profile.png'),
  footerSearch: require('./footer/search.png'),
  footerSettings: require('./footer/settings.png'),
  footerMessage: require('./footer/message.png'),
  footerMagnet: require('./footer/magnetHome.png'),
  footerNotification: require('./footer/notification.png'),

  activeFooterProfile: require('./footer/activeProfile.png'),
  activeFooterSearch: require('./footer/activeSearch.png'),
  activeFooterSettings: require('./footer/activeSettings.png'),
  activeFooterMessage: require('./footer/activeMessage.png'),
  activeFooterMagnet: require('./footer/activeMagnetHome.png'),
  activeFooterNotification: require('./footer/activeNotification.png'),

  roomBG: require('./roomBG.jpg'),    
  backgroundOfProfilImage: require('./backgroundOfProfilImage.png'),
  exampleProfilPhoto: require('./exampleProfilPhoto.jpg'),

  usa: require('./usa.png'),
  england: require('./england.jpg'),
  brazil: require('./brazil.png'),
  
  addToWishList: require('./addToWishList.png'),
  extraGray: require('./extraGray.png'),
  extraWhite: require('./extraWhite.png'),
  back: require('./back.png'),
  undo: require('./undo.png'),
  signupUser: require('./signupUser.png'),
  whitePlus: require('./whitePlus.png'),
  plus: require('./plus.png'),
  noImage: require('./noImage.png'),
  heartbordered: require('./heartbordered.png'),
  heartfilled: require('./heartfilled.png'),
  passivestar: require('./passivestar.png'),
  activestar: require('./activestar.png'),
  searchPlace: require('./searchPlace.png'),
  passivecomment: require('./passivecomment.png'),
  activecomment: require('./activecomment.png'),
  trash: require('./trash.png'),
  list: require('./list.png'),
  addContact: require('./addContact.png'),
  logout: require('./logout.png'),
  camera: require('./camera.png'),
  save: require('./save.png'),
  leftArrow: require('./leftArrow.png'),
  calendar: require('./calendar.png'),
  exit: require('./exit.png'),
  seen: require('./seen.png'),
  unseen: require('./unseen.png'),
  messageTypeNotif: require('./messageTypeNotif.png'),
  commentTypeNotif: require('./commentTypeNotif.png'),
  send: require('./send.png'),
  story: require('./story.png'),
  grayList: require('./grayList.png'),
  earth: require('./earth.png'),
  whiteBasePlus: require('./whiteBasePlus.png'),
  
  
  
  
  
  
  
  appIntro1:	require('./appintro/1.png'),
  appIntro2:	require('./appintro/2.png'),  
  appIntro3:	require('./appintro/3.png'),
	appIntro4:	require('./appintro/4.png'),
	appIntro5:	require('./appintro/5.png'),
	appIntro6:	require('./appintro/6.png'),
	//appIntro7:	require('./appintro/7.png'),
	//appIntro8:	require('./appintro/8.png'),
	//appIntro9:	require('./appintro/9.png'),

  notification: require('./profilMain/notification.png'),
  setting: require('./profilMain/setting.png'),
  message: require('./profilMain/message.png'),
  messageTest: require('./profilMain/messageTest.png'),
  location: require('./profilMain/location.png'),
  plusOrange: require('./profilMain/plus.png'),
  more: require('./profilMain/more.png'),
  locationPassive: require('./profilMain/locationPassive.png'),
  messagePassive: require('./profilMain/messagePassive.png'),
  addUser: require('./profilMain/addUser.png'),
  fellows: require('./profilMain/fellows.png'),
  

  grayLocation: require('./profileAddStory/grayLocation.png'),
  grayLocation2: require('./profileAddStory/grayLocation2.png'),
  noPhoto: require('./profileAddStory/noPhoto.png'),
  location: require('./profilMain/location.png'),
  //magnet
  pinRed: require('./magnet/p1.png'),
  pinGreen: require('./magnet/p2.png'),
  pinOrange: require('./magnet/p3.png'),
  magnetPin: require('./magnet/pin.png'),
  magnetSubscribe: require('./magnet/subscribe.png'),
  magnetLocation: require('./magnet/location.png'),

  cancel: require('./magnet/cancel.png'),
  blackCancel: require('./magnet/blackCancel.png'),
  grayCancel: require('./magnet/grayCancel.png'),
  blackSearch: require('./magnet/blackSearch.png'),

  storyImage1: require('./tester/storyImage1.jpeg'),
  storyImage2: require('./tester/storyImage2.jpeg'),
  storyImage3: require('./tester/storyImage3.jpeg'),

  ss1: require('./splashscreen/splashscreenbg.png'),

  loginBG1: require('./login/bg1.jpeg'),
  loginBG2: require('./login/bg2.jpg'),
  loginBG3: require('./login/bg3.jpg'),
  

  


  logo: require('./logo.png'),


  //TESTERS

  emma: require('./testers/Emma.jpg'),
  paolo: require('./testers/Paolo.jpg'),
  
  
  
}

export default images;
