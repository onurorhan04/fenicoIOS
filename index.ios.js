import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Alert,
  AppState
} from 'react-native';
import KeepToken from './app/stores/keeptoken';
import SplashScreen from './app/components/splashscreen'
import DB from './app/realmDB/databases'
import {Routes} from './app/router';
import LoginPage from './app/pages/loginPage';
import SignupPage from './app/pages/signupPage'
import MagnetMain from './app/pages/magnet/magnetMain';
import { StackNavigator } from 'react-navigation';
import { Provider } from 'mobx-react';
import Stores from './app/stores'
var base64 = require('base-64');
var {height, width} = Dimensions.get('window');
import { observer } from 'mobx-react/native';
import * as Firebase from 'firebase';
const config = {
  apiKey: "AIzaSyCd2-Qr46XKCiEAn_-JNPB2cCTTPQz29nc",
  authDomain: "fenicochatfb.firebaseapp.com",
  databaseURL: "https://fenicochatfb.firebaseio.com",
  storageBucket: "fenicochatfb.appspot.com"
};
Firebase.initializeApp(config);

@observer
export default class fenicoMac extends Component {
  constructor(props){
    super(props)
    this.state = {
      isChecked: false,
      routeStatus:"",
      passSplashScreen:false,
      appState:''
    }
  }
  checker(){
    const spLoginInfo = DB.objects('SPLoginSchema')
    const fbLoginInfo = DB.objects('FaceBookLoginSchema')
    if(spLoginInfo.length > 0 ){
      return "SP"
    }else if(fbLoginInfo.length > 0 ){
      return "FB"
    }
  }
  setOnline(){
    const onlineUserRef = Firebase.database().ref().child('Log').child('Online').child(KeepToken.userID)
    onlineUserRef.update({
      userID:KeepToken.userID,
      username:KeepToken.username
    })
  }
  setUserPlatform(){
    const onlineUserRef = Firebase.database().ref().child('Log').child('IOS').child(KeepToken.userID)
    onlineUserRef.update({
      userID:KeepToken.userID,
      username:KeepToken.username
    })
  }

  userSPLogin(username,password){
    var params = {
      username: base64.encode(username),
      password: base64.encode(password)
    }
    var formData = new FormData()
    for(var k in params) {
      formData.append(k,params[k])
    }
    fetch("https://fenicoapp.com/api/v1/api/access-token", {
      method: 'POST',
      headers: {
        'Acccept': 'application/json',
        'Content-type': 'multipart/form-data'
      },
      body:formData
    })
    .then((response) => response.json())
    .then((responseData) => {
      KeepToken.userID = responseData.user_id
      KeepToken.token = responseData.access_token
      KeepToken.username = username
      KeepToken.password = password
      this.setState({isChecked:true,routeStatus:"magnet"})
      this.setOnline()
      this.setUserPlatform()
    })
  }
  userFBLogin(fbtoken,password){
      fetch('https://fenicoapp.com/api/v1/search/search?q=istanbul21312321',{
        method: 'GET',
        headers: {
          'Authorization': 'Bearer fbtoken' + fbtoken
        }
      })
      .then((response) => response.json())
      .then((responseData) => {
        //Alert.alert('',JSON.stringify(responseData))
        //console.log(responseData)
        //console.log(responseData)
        KeepToken.userID = responseData.user_id
        KeepToken.token = responseData.access_token
        KeepToken.fbtoken = fbtoken
        KeepToken.username = password
        KeepToken.password = password
        this.getUserData(responseData.user_id,responseData.access_token)
        this.setOnline()
        this.setUserPlatform()
      }).catch((err) => {
        //Alert.alert('',JSON.stringify(err) )
      })
    
  }
  getUserData(userID,token){
    fetch('https://fenicoapp.com/api/v1/user-profile/view?id=' + userID, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + token
      }
    })
    .then((response) => response.json())
    .then((responseData) => {
        KeepToken.username = responseData.items[0].username
        //console.log('KEEPTOKEN USERNAME: ' + KeepToken.username)
        
          this.setState({isChecked:true,routeStatus:"magnet"})
          //this.timeOutForSplashScreen()
        
    })
  }
  checkAppIntro(){
    const appIntroData = DB.objects('AppIntro');
    if(appIntroData.length == 0){
      return 0
    }
  }
  timeOutForSplashScreen(){
    setTimeout(function(){ this.setState({ passSplashScreen:true }) }.bind(this), 4000);
  }
  componentDidMount(){
    
    AppState.addEventListener('change', this._handleAppStateChange);
  }
  componentWillUnmount(){
    AppState.removeEventListener('change', this._handleAppStateChange);

  }
  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      const onlineUserRef = Firebase.database().ref().child('Log').child('Online').child(KeepToken.userID)

      onlineUserRef.update({
        userID:KeepToken.userID,
        username:KeepToken.username
      })

    }else{
      if(KeepToken.userID !== ""){

        const onlineUserRef = Firebase.database().ref().child('Log').child('Online').child(KeepToken.userID)
        onlineUserRef.remove()
      }
    }
    this.setState({appState: nextAppState});
  }
  componentWillMount(){
    KeepToken.userID = ""
    console.disableYellowBox = true;
    if(this.checker()){
      
      if(this.checker() == "SP"){
        const spLoginInfo = DB.objects('SPLoginSchema')
        this.userSPLogin(spLoginInfo[0].username,spLoginInfo[0].password)
      }else{
        const fbLoginInfo = DB.objects('FaceBookLoginSchema')
        //Alert.alert(fbLoginInfo[0].fbtoken.toString(),JSON.stringify(fbLoginInfo))
        //console.log(fbLoginInfo.length)
        this.userFBLogin(fbLoginInfo[0].fbtoken,fbLoginInfo[0].password)
      }
    }else{
        this.setState({isChecked:true,routeStatus:'login'})
        //this.timeOutForSplashScreen()
    }
  }
  render() {
    
    return (
      <View style={{flex:1}}>
        {this.state.isChecked && this.state.routeStatus == "login" ? 
        <Provider {...Stores}>
        <AppNavigatorLoginPage />
        </Provider> : null }
        {this.state.isChecked && this.state.routeStatus == "magnet" ? 
        <Provider {...Stores}>
        <AppNavigatorMagnetMain /> 
        </Provider> : null }
      </View>
    );
  }
}
const AppNavigatorLoginPage = StackNavigator({
  ...Routes,
}, {
  initialRouteName: 'loginPage',
  headerMode: 'none',
});
const AppNavigatorMagnetMain = StackNavigator({
  ...Routes,
}, {
  initialRouteName: 'magnetMain',
  headerMode: 'none',
});
const AppNavigatorAppIntro = StackNavigator({
  ...Routes,
}, {
  initialRouteName: 'appIntro',
  headerMode: 'none',
});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('fenicoMac', () => fenicoMac);
