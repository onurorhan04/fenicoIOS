const functions = require('firebase-functions');
const admin = require('firebase-admin');
var Firebase  = require('firebase');

admin.initializeApp(functions.config().firebase);
const config = {
    apiKey: "AIzaSyCd2-Qr46XKCiEAn_-JNPB2cCTTPQz29nc",
    authDomain: "fenicochatfb.firebaseapp.com",
    databaseURL: "https://fenicochatfb.firebaseio.com",
    storageBucket: "fenicochatfb.appspot.com"
  };
  Firebase.initializeApp(config);
exports.sendNewMessageNotification = functions.database.ref('/Messages/{uid}/{ouid}').onWrite(event => {
        if (!event.data.previous.exists()) {
          const {uid, ouid} = event.params;
          const pushNotifToServer = Firebase.database().ref().child("Notifications").child('MessageType').child(event.data.val().to.toString()).child(event.data.val().from.toString())
          //pushNotifToServer.remove()
          pushNotifToServer.push({
            dateTime:event.data.val().dateTime,
            from:event.data.val().from,
            text:event.data.val().text,
            to:event.data.val().to,
            fromName:event.data.val().fromName,
            notifType:'messageType'
          });
        }
                
        
        const toTopicName = event.data.val().to
        const payload = {
            data:{
              fromID:event.data.val().from.toString(),
              fromName:event.data.val().fromName.toString(),
              fromImage:event.data.val().fromImage.toString(),
              toImage:event.data.val().toImage.toString()
            },
            notification: {
                title: event.data.val().fromName.toString(),
                body: event.data.val().text.toString(),
                color:'#FFFFFF',
                icon:'ic_stat_ic_notification',
                large_icon:'ic_stat_ic_notification',
                click_action:'fcm.ACTION.MESSAGE'
            }
        };

        return admin.messaging()
                    .sendToTopic('/topics/'+toTopicName.toString(), payload);
});
exports.sendNewMagnetNotification = functions.database.ref('/Magnets/{key}').onWrite(event => {
  //icon: magnetImage.toString(),

  // todo if status is active !!!!
  const {key} = event.params;
  if (!event.data.previous.exists()) {
    console.log('-----')
    console.log(event.data.val().userID)
    console.log(event.data)
    console.log('*****')

    if(event.data.val().userID !== null){
      var subscribeRef = Firebase.database().ref().child("Subscribe")

      subscribeRef.once('value', (snap) => {
        snap.forEach((child) => {
          child.forEach((data) => {

            //magnetID = event.data.val().userID
            //magnetImage = event.data.val().image
            var magnetLatitude = event.data.val().latitude
            var magnetLongitude = event.data.val().longitude
            var magnetName = event.data.val().name
            var magnetTime = event.data.val().time
            var magnetTitle = event.data.val().title
            var magnetType = event.data.val().type
            var magnetUserID = event.data.val().userID
            var longitudeDiff = magnetLongitude - data.val().longitude
            var latitudeDiff = magnetLatitude - data.val().latitude
            if(data.val().id != event.data.val().userID ){
              if(longitudeDiff > -1 && longitudeDiff < 1){
                if(latitudeDiff > -1 && latitudeDiff < 1){
                  console.log(magnetTitle.toString())
                  console.log('toTopicName')
                  console.log(data.val().id)

                  const toTopicName = data.val().id.toString()
                  const payload = {
                      data:{
                        magnetID : event.data.val().id.toString(),
                        magnetImage : event.data.val().image.toString(),
                        magnetLatitude : event.data.val().latitude.toString(),
                        magnetLongitude : event.data.val().longitude.toString(),
                        magnetName : event.data.val().name.toString(),
                        magnetTime : event.data.val().time.toString(),
                        magnetTitle : event.data.val().title.toString(),
                        magnetType : event.data.val().type.toString(),
                        magnetUserID : event.data.val().userID.toString()
                      },
                      notification: {
                          title: 'Someone nearby pinned a magnet',
                          body: magnetName.toString(),
                          icon:'ic_launcher',
                          color:'#FFFFFF',
                          click_action: 'fcm.ACTION.MAGNET'
                      }
                  }; 
                    
                  //5qt6x4zu

                  admin.messaging()
                            .sendToTopic('/topics/'+toTopicName.toString(), payload);
                }
              }
            }


          })
        })
      })

    }
  
  }
  
  

});

exports.sendNewCommentNotification = functions.database.ref('/Magnets/{magnetKey}/comments/{commentKey}').onWrite(event => {
        // will use event.data.val() instead of child.val() for some of them
        // not might be problem in loop

        //yazdıgı commentten sonraki commentler notif olarak gönderilsin belki moment gerekir
        console.log(event)
        console.log(event.data)
        console.log(event.data.val().comment)
        const {magnetKey, commentKey} = event.params;
        var commentedRef = Firebase.database().ref().child("Commented")
        commentedRef.once('value', (snap) => {
          snap.forEach((child) => {
            if(child.val().magnetKey == magnetKey){
              const toCommentTopicName = child.val().userID.toString()
              //------------------------------------------------------------------//
              if(toCommentTopicName != event.data.val().from){
                var currentDate = new Date();
                const pushNotifToServer = Firebase.database().ref().child("Notifications").child('CommentType').child(toCommentTopicName.toString())
                pushNotifToServer.push({
                  magnetKey: child.val().magnetKey.toString(),
                  magnetTitle: child.val().magnetTitle.toString(),
                  magnetLatitude: child.val().coordinate.latitude.toString(),
                  magnetLongitude: child.val().coordinate.longitude.toString(),
                  commentFromID: event.data.val().from.toString(),
                  commentFrom: event.data.val().fromName.toString(),
                  commentText: event.data.val().comment.toString(), // have to be event.data.val()
                  date: currentDate.toString(),
                  notifType:'commentType'
                });
                //------------------------------------------------------------------//
                const payload = {
                    data:{
                      magnetKey: child.val().magnetKey.toString(),
                      magnetLatitude: child.val().coordinate.latitude.toString(),
                      magnetLongitude: child.val().coordinate.longitude.toString()
                    },
                    notification: {
                        title: 'A new comment',
                        body: 'from ' + event.data.val().fromName.toString(),
                        color:'#FFFFFF',
                        icon:'ic_launcher',
                        large_icon:'ic_launcher',
                        click_action:'fcm.ACTION.COMMENT'
                    }
                };
                admin.messaging()
                          .sendToTopic('/topics/'+toCommentTopicName.toString(), payload);
              }

            }
          })
        })
});
exports.handleMagnets = functions.database.ref('/Magnets/{magnetKey}').onWrite(event => {
        const {magnetKey} = event.params;
        if(event.data.val().status === "passive"){
          const magnetRef = Firebase.database().ref().child('Magnets').child(magnetKey.toString())
          //COMMENTED
          const commentedRef = Firebase.database().ref().child("Commented")
          commentedRef.once('value', (snap) => {
            snap.forEach((child) => {
              if(child.val().magnetKey == magnetKey){
                const removeCommentedRef = Firebase.database().ref().child('Commented').child(child.key.toString())
                removeCommentedRef.remove()
              }
            })
          })
          //NOTIFICATIONS
          const notificationRef = Firebase.database().ref().child("Notifications").child('CommentType');
          notificationRef.once('value', (snap) => {
            snap.forEach((child) => {
              child.forEach((data) => {
                if(data.val().magnetKey == magnetKey){
                  const removeNotificationRef = Firebase.database().ref().child("Notifications").child('CommentType').child(child.key.toString()).child(data.key.toString());
                  removeNotificationRef.remove()
                }
              })
            })
          })

        }
});

exports.exampleRemoteIOSNotif = functions.database.ref('/exampleRemoteNotif/{uid}/').onWrite(event => {
        const { uid } = event.params;
         
        
        const topicName = event.data.val().topicName.toString()
        console.log(event.data.val().text.toString())
        const payload = {
            data:{
              text: event.data.val().text.toString(),
              topicName: event.data.val().topicName.toString()
            },
            notification: {
                title: 'title',
                body: event.data.val().text.toString(),
                subtitle: 'subtitle'
            }
            
        };

        return admin.messaging()
                    .sendToTopic('/topics/' + topicName.toString(), payload);
});


